//
//  Enums.swift
//  Needs
//
//  Created by webcastle on 11/02/22.
//

import Foundation

enum ViewType: Int{
    case product
    case wishlist
    case monthlyBucket
    case cart
    case order
    case returnPdt
    case checkOut
}

enum HeaderType: Int {
    case newArrival
    case offerZone
    case dealOfTheWeek
}

enum PageType: Int {
    case category = 0
    case newArrival
    case offerZone
    case dealOfTheWeek
    case home    
}

enum ProfileCellType: Int {
    case wishlist
    case address
    case invite
    case faq
    case about
    case terms
    case privacy
    case contact
    case logout
}

enum PopUpType: Int {
    case logout
    case cart
    case login
    case fav
    case location
    case delete
    case outOfStock
    case cartOutOfStock
}
