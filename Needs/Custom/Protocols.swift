//
//  Protocol.swift
//  Needs
//
//  Created by webcastle on 11/02/22.
//

import Foundation


protocol NotifyDelegate : AnyObject {
    func didSelect(withInfo info:[String:Any]?)
}

protocol CVCDelegate : AnyObject {
    func didSelect(withInfo info:[String:Any]?)
}
