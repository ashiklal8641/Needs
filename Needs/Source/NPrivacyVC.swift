//
//  NPrivacyVC.swift
//  Needs
//
//  Created by Admin on 17/02/22.
//

import UIKit
import Alamofire

class NPrivacyVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    var isFromMenu = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.lblTitle.text = isFromMenu ? "Privacy Policy" : "Return Policy"
        self.lblSubTitle.text = isFromMenu ? "Our Privacy Policy" : "Our Return Policy"
        if isFromMenu {
            self.getPrivacyDetails()
        }else {
            self.getReturnPolicyDetails()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NPrivacyVC {
    
    private func getPrivacyDetails(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID
        ]
        
        NMainViewModel.getPrivacyPolicy(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.lblDesc.attributedText = (data.policy ?? "").htmlToAttributedString 
                    }
                } else {
                   
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    private func getReturnPolicyDetails(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID
        ]
        
        NMainViewModel.getReturnPolicy(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.lblDesc.attributedText = (data.policy ?? "").htmlToAttributedString
                    }
                } else {
                   
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}
