//
//  NLoginVC.swift
//  Needs
//
//  Created by webcastle on 09/02/22.
//

import UIKit
import Alamofire

class NLoginVC: UIViewController {

    @IBOutlet weak var lblSkip: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var tfPhone: UITextField!
    
    lazy var attributes:[NSAttributedString.Key:Any] = {
        return [.font:UIFont.robotoFont(ofSize: 14, weight: .medium),.underlineStyle:NSUnderlineStyle.single.rawValue]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.lblSkip.attributedText = NSAttributedString(string: "Skip", attributes: self.attributes)
        self.initializeView()
    }
    

    
    
    // MARK: - Button action
    
    @IBAction func tappedContinue(_ sender: UIButton) {
        self.loginUser()
//        let vc = Storyboards.Login.viewController(controller: NOtpVerifyVC.self)
//        self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)
    }
    
    @IBAction func tappedSkip(_ sender: UIButton) {
        self.skipLogin()
//        let vc = Storyboards.Login.viewController(controller: NLocationVC.self)
//        self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)
    }

    func initializeView() {
        guard let text = lblDesc.attributedText?.string else {
            return
        }
        
        let range = text.range(of: NSLocalizedString("One Time Password", comment: "one time password"))!
        
        let attrs = [NSAttributedString.Key.font : UIFont.robotoFont(ofSize: 14, weight: .medium),NSAttributedString.Key.foregroundColor: UIColor.init(red: 29/255.0, green: 29/255.0, blue: 29/255.0, alpha: 1.0)]
        let attrStr = NSMutableAttributedString(string: text)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 10
        attrStr.addAttribute(.paragraphStyle,
                                              value: paragraphStyle,
                                              range: NSRange(location: 0, length: text.count))
        attrStr.addAttributes(attrs, range: NSRange(range, in: text))
        
        self.lblDesc.attributedText = attrStr
        self.lblSkip.isHidden = SessionHandler.isGuest ? true : false
    }
    
    private func validateFields() throws -> String {
        if !BaseValidator.isNotEmpty(string: self.tfPhone.text) {
            throw ValidationError("Please Enter Your Mobile Number")
        } else {
            //            if AppData.environment == .development {
            if tfPhone.text?.trimWhiteSpace.count != 10 {
                throw ValidationError("Please Enter Valid Number")
            } else {
                
            }
            //            } else {
            //                if tfPhone.text?.trimWhiteSpace.count != 8 {
            //                    throw ValidationError("Please Enter Valid Number")
            //                } else {
            //
            //                }
            //            }
        }
        return self.tfPhone.text?.trimWhiteSpace.replacingOccurrences(of: " ", with: "") ?? ""
    }
    
}

extension NLoginVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
   
            let maxLength = 10
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        
    }
}

extension NLoginVC {
    
    private func loginUser(){
        
        do {
            let number = try validateFields()
            let parameter:Parameters = [
                "country_code":"+91",
                "phone_number": number,
                "device_token": SessionHandler.deviceToken,
                "device_type": SessionHandler.deviceType,
                "pre_user_id": SessionHandler.preUserID,
                "session_id": SessionHandler.bearer_token ?? ""
            ]
            NLoginViewModel.login(parameters: parameter) { result in
                switch result {
                case .success(let response):
                    if response.errorcode == 0 {
                        if let loginData = response.data {
                            let vc = Storyboards.Login.viewController(controller: NOtpVerifyVC.self)
                            vc.phone = number
                            vc.otp = loginData.otp ?? 0000
                            self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)
                            
                        }
                    } else {
                        Utility.displayBanner(message: response.message ?? "", style: .danger)
                    }
                case .failure(let error):
                    DebugLogger.error(error.localizedDescription)
                }
            }
        } catch (let error){
            Utility.displayBanner(message: (error as! ValidationError).message , style: .danger)
        }
        
    }
    
    private func skipLogin(){
        
        let parameter:Parameters = [
            "device_token": SessionHandler.deviceToken,
            "device_type": SessionHandler.deviceType,
            "session_id": SessionHandler.bearer_token ?? ""
        ]
        
        NLoginViewModel.guestLogin(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let loginData = response.data {
                        SessionHandler.preUserID = loginData.user_id ?? -1
                        SessionHandler.bearer_token = loginData.session_id ?? ""
                        SessionHandler.isGuest = true
//                        Utility.setDashboardViewController()
                        let vc = Storyboards.Login.viewController(controller: NLocationVC.self)
                        self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}
