//
//  NRegisterVC.swift
//  Needs
//
//  Created by webcastle on 09/02/22.
//

import UIKit
import Alamofire

class NRegisterVC: ParentController {

    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTnC: UILabel!
    @IBOutlet weak var lblPrivacy: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    var userID = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblTnC.isUserInteractionEnabled = true
        lblTnC.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel(_:))))
        lblPrivacy.isUserInteractionEnabled = true
        lblPrivacy.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnPrivacyLabel(_:))))
        self.initializeView()
    }
    

    // MARK: - Button action
    
    @IBAction func tappedContinue(_ sender: UIButton) {
        if isValid() {
            self.registerUser()
        }
//        let vc = Storyboards.Login.viewController(controller: NLocationVC.self)
//        self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)
    }

    func initializeView() {
        guard let text = lblDesc.attributedText?.string else {
            return
        }
        guard let str = lblTnC.attributedText?.string else {
            return
        }
        guard let strPrivacy = lblPrivacy.attributedText?.string else {
            return
        }
        let range = text.range(of: NSLocalizedString("Needs App", comment: "needs app"))!
        let resendRange = str.range(of: NSLocalizedString("Terms of Services", comment: "terms"))!
        let privacyRange = strPrivacy.range(of: NSLocalizedString("Privacy Policy", comment: "privacy"))!
        let attrs = [NSAttributedString.Key.font : UIFont.robotoFont(ofSize: 12, weight: .medium),NSAttributedString.Key.foregroundColor: UIColor.init(red: 29/255.0, green: 29/255.0, blue: 29/255.0, alpha: 1.0)]
        let attr = [NSAttributedString.Key.font : UIFont.robotoFont(ofSize: 12, weight: .regular),NSAttributedString.Key.foregroundColor: UIColor.init(red: 1/255.0, green: 131/255.0, blue: 83/255.0, alpha: 1.0),NSAttributedString.Key.underlineStyle:NSUnderlineStyle.single.rawValue] as [NSAttributedString.Key : Any]
        let attrStr = NSMutableAttributedString(string: text)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 10
        attrStr.addAttribute(.paragraphStyle,
                                              value: paragraphStyle,
                                              range: NSRange(location: 0, length: text.count))
        let attrbtStr = NSMutableAttributedString(string: str)
        let attStr = NSMutableAttributedString(string: strPrivacy)
        attrStr.addAttributes(attrs, range: NSRange(range, in: text))
        attrbtStr.addAttributes(attr, range: NSRange(resendRange, in: str))
        attStr.addAttributes(attr, range: NSRange(privacyRange, in: strPrivacy))
        self.lblDesc.attributedText = attrStr
        self.lblTnC.attributedText = attrbtStr
        self.lblPrivacy.attributedText = attStr
    }
    
    @objc func handleTapOnLabel(_ recognizer: UITapGestureRecognizer) {
        guard let text = lblTnC.attributedText?.string else {
            return
        }
        
        if let range = text.range(of: NSLocalizedString("Terms of Services", comment: "terms")),
           recognizer.didTapAttributedTextInLabel(label: lblTnC, inRange: NSRange(range, in: text)) {
           // BrowserClient.openSafariController(path: AppData.tnc)
            let vc  = Storyboards.New.viewController(controller: NTandCVC.self)
            self.pushVC = vc        }
    }
    
    @objc func handleTapOnPrivacyLabel(_ recognizer: UITapGestureRecognizer) {
        guard let text = lblPrivacy.attributedText?.string else {
            return
        }
        
        if let range = text.range(of: NSLocalizedString("Privacy Policy", comment: "privacy")),
           recognizer.didTapAttributedTextInLabel(label: lblPrivacy, inRange: NSRange(range, in: text)) {
           // BrowserClient.openSafariController(path: AppData.tnc)
            let vc  = Storyboards.New.viewController(controller: NPrivacyVC.self)
            self.pushVC = vc
        }
    }
    
    private func isValid() -> Bool {
        if !BaseValidator.isNotEmpty(string: self.txtName.text) {
            Utility.displayBanner(message: "The name field is required", style: .danger)
            return false
        }else if !BaseValidator.isNotEmpty(string: self.txtEmail.text) {
            Utility.displayBanner(message: "The email id field is required", style: .danger)
            return false
        }else if !BaseValidator.isValid(email: self.txtEmail.text) {
            Utility.displayBanner(message: "Enter valid email id", style: .danger)
            return false
        }
        return true
    }
}

extension NRegisterVC {
    
    func registerUser() {
        
        let parameter:Parameters = [
            "email": self.txtEmail.text?.trimWhiteSpace ?? "",
            "name": self.txtName.text?.trimWhiteSpace ?? "",
            "user_id": userID
        ]
       
        NLoginViewModel.registerUser(parameters: parameter){ [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        SessionHandler.uID = data.user_id ?? -1
                        LocalPersistant.save(value: data.name ?? "", key: .user_name)
                        LocalPersistant.save(value: data.email ?? "", key: .email)
//                        LocalPersistant.save(value: self.txtEmail.text?.trimWhiteSpace, key: .phone_number)
                        if SessionHandler.isGuest && SessionHandler.isGuestCheckOut {
                            SessionHandler.isGuestCheckOut = false
                            SessionHandler.isGuest = false
//                            let vc = Storyboards.Cart.viewController(controller: JCartVC.self)
//                            vc.isFromLogin = true
//                            self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)
                        }else {
                            SessionHandler.isGuest = false
//                            Utility.setDashboardViewController()
                            let vc = Storyboards.Login.viewController(controller: NLocationVC.self)
                            self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)
                        }
                    }
                }
            case .failure(let error):
                Utility.displayBanner(message: error.localizedDescription, style: .danger)
            }
        }
       
    }
}
