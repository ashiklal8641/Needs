//
//  NOtpVerifyVC.swift
//  Needs
//
//  Created by webcastle on 09/02/22.
//

import UIKit
import Alamofire

class NOtpVerifyVC: UIViewController {

    
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblResendOtp: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet var txtFourth: UITextField!
    @IBOutlet var txtThird: UITextField!
    @IBOutlet var txtSecond: UITextField!
    @IBOutlet var txtFirst: UITextField!
    
    
    var seconds = 30
    var otp = 0000
    var phone = ""
    var otpTimer:OTPTimer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initializeView()
        self.showToast(message: "\(otp)", font:.systemFont(ofSize: 15))
        self.initilizeTimer()
//        self.txtFirst.becomeFirstResponder()
        self.lblResendOtp.isHidden = true
        lblResendOtp.isUserInteractionEnabled = true
        lblResendOtp.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel(_:))))
    }
    

    // MARK: - Button action
    
    @IBAction func tappedContinue(_ sender: UIButton) {
//        let vc = Storyboards.Login.viewController(controller: NRegisterVC.self)
//        self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)
        self.verifyOTP()
    }
    
    @IBAction func didChangedTextfield(_ sender: UITextField) {
        let text = sender.text
        if  text?.count == 1 {
            switch sender{
            case txtFirst:
                txtSecond.becomeFirstResponder()
            case txtSecond:
                txtThird.becomeFirstResponder()
            case txtThird:
                txtFourth.becomeFirstResponder()
            case txtFourth:
                txtFourth.resignFirstResponder()
            default:
                break
            }
        }else if  text?.count ?? 0 >= 1 {
            switch sender{
            case txtFirst:
                let str = text?.dropFirst()
                txtFirst.text = String(str ?? "")
                txtSecond.becomeFirstResponder()
            case txtSecond:
                let str = text?.dropFirst()
                txtSecond.text = String(str ?? "")
                txtThird.becomeFirstResponder()
            case txtThird:
                let str = text?.dropFirst()
                txtThird.text = String(str ?? "")
                txtFourth.becomeFirstResponder()
            case txtFourth:
                let str = text?.dropFirst()
                txtFourth.text = String(str ?? "")
                txtFourth.resignFirstResponder()
            default:
                break
            }
        }
        if  text?.count == 0 {
            switch sender{
            case txtFirst:
                txtFirst.becomeFirstResponder()
            case txtSecond:
                txtFirst.becomeFirstResponder()
            case txtThird:
                txtSecond.becomeFirstResponder()
            case txtFourth:
                txtThird.becomeFirstResponder()
            default:
                break
            }
        }
    }
    
    @objc func handleTapOnLabel(_ recognizer: UITapGestureRecognizer) {
        guard let text = lblResendOtp.attributedText?.string else {
            return
        }
        
        if let range = text.range(of: NSLocalizedString("Don't receive an OTP ? Resend Otp", comment: "resend otp")),
           recognizer.didTapAttributedTextInLabel(label: lblResendOtp, inRange: NSRange(range, in: text)) {
           // BrowserClient.openSafariController(path: AppData.tnc)
            self.resendOtp()
        }
    }

    func initializeView() {
        self.lblDesc.text = "Please enter the One Time Password send \nto your number \(phone)"
        guard let text = lblDesc.attributedText?.string else {
            return
        }
        guard let str = lblResendOtp.attributedText?.string else {
            return
        }
        let range = text.range(of: NSLocalizedString("One Time Password", comment: "one time password"))!
        let resendRange = str.range(of: NSLocalizedString("Resend Otp", comment: "otp"))!
        let attrs = [NSAttributedString.Key.font : UIFont.robotoFont(ofSize: 13, weight: .medium),NSAttributedString.Key.foregroundColor: UIColor.init(red: 29/255.0, green: 29/255.0, blue: 29/255.0, alpha: 1.0)]
        let attr = [NSAttributedString.Key.font : UIFont.robotoFont(ofSize: 13, weight: .medium),NSAttributedString.Key.foregroundColor: UIColor.init(red: 1/255.0, green: 131/255.0, blue: 83/255.0, alpha: 1.0),NSAttributedString.Key.underlineStyle:NSUnderlineStyle.single.rawValue] as [NSAttributedString.Key : Any]
        let attrStr = NSMutableAttributedString(string: text)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 10
        attrStr.addAttribute(.paragraphStyle,
                                              value: paragraphStyle,
                                              range: NSRange(location: 0, length: text.count))
        let attrbtStr = NSMutableAttributedString(string: str)
        attrStr.addAttributes(attrs, range: NSRange(range, in: text))
        attrStr.addAttributes(attrs, range: NSRange(location: (text.count - phone.count), length: phone.count))
        attrbtStr.addAttributes(attr, range: NSRange(resendRange, in: str))
        self.lblDesc.attributedText = attrStr
        self.lblResendOtp.attributedText = attrbtStr
    }
    
    func validate() {
        if txtFirst.text == "" || (txtSecond.text == "") || (txtThird.text == "") || (txtFourth.text == "") {
//            BannerNotification.successBanner(message: "Enter a valid otp")
            return
        }
    }
    
    private func resetTimer() {
        self.clearOTPFields()
        self.otpTimer?.start()
    }
    
    fileprivate func stopTimer(){
        if self.otpTimer != nil {
            self.otpTimer?.stop()
            self.otpTimer = nil
        }
        
    }
    
    private func clearOTPFields() {
        self.txtFirst.text = nil
        self.txtSecond.text = nil
        self.txtThird.text = nil
        self.txtFourth.text = nil
    }
    
    private func initilizeTimer() {
        self.otpTimer = OTPTimer(seconds: seconds, callback: { [weak self] (value,isEnded) in
            guard let self = self else {return}
            self.lblTimer.text = "Expire in \(value)"
            self.lblResendOtp.isHidden = !isEnded
            self.lblTimer.isHidden = isEnded
            if isEnded {
                self.clearOTPFields()
            }
        })
        self.otpTimer?.start()
    }
    
}

extension NOtpVerifyVC {
    
    func verifyOTP(){
        if let one = self.txtFirst.text,
           let two = txtSecond.text,
           let three = txtThird.text,
           let four = txtFourth.text {
            let otp = (one+two+three+four).trimWhiteSpace
            if otp.count == 4 {
                let parameter:Parameters = [
                    "otp":otp,
                    "country_code":"+" + "91",
                    "phone_number": self.phone
                ]
                NLoginViewModel.verifyOtp(parameters: parameter) { [weak self] result in
                    guard let self = self else {return}
                    switch result {
                    case .success(let response):
                        if response.errorcode == 0 {
                            if let verifyOtpData = response.data {
                                let userID = verifyOtpData.user_id ?? -1
                                if verifyOtpData.isExisting == true {
//                                    SessionHandler.bearer_token = verifyOtpData.token
                                    SessionHandler.uID = userID
                                    LocalPersistant.save(value: verifyOtpData.name ?? "", key: .user_name)
                                    LocalPersistant.save(value: verifyOtpData.email ?? "", key: .email)
                                    LocalPersistant.save(value: verifyOtpData.phone ?? "", key: .phone_number)
                                    
                                    if SessionHandler.isGuest && SessionHandler.isGuestCheckOut{
                                        SessionHandler.isGuestCheckOut = false
                                        SessionHandler.isGuest = false
                                        if let window = Utility.keyWindow,let controller = window.rootViewController as? UINavigationController {
                                            let vc = Storyboards.Main.viewController(controller:NCustomTabbar.self)
                                            vc.selectedIndex = 2
                                            controller.setViewControllers([vc], animated: true)
                                        }
                                    }else {
                                        SessionHandler.isGuest = false
                                        Utility.setDashboardViewController()
//                                        let vc = Storyboards.Login.viewController(controller: NLocationVC.self)
//                                        self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)
                                    }
                                } else {
                                    let vc: NRegisterVC = Storyboards.Login.viewController(controller: NRegisterVC.self)
                                    vc.userID = userID
                                    self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)
                                }
                            }
                        }else {
                            Utility.displayBanner(message: response.message ?? "",style:.danger)
                        }
                    case .failure(_):
                        break
                    }
                }
            } else {
                Utility.displayBanner(message: "Invalid OTP",style:.danger)
            }
        }
    }
    
    private func resendOtp(){
        
            let parameter:Parameters = [
                "country_code":"+91",
                "mobile": self.phone
            ]
        
            NLoginViewModel.resendOtp(parameters: parameter) { result in
                switch result {
                case .success(let response):
                    if response.errorcode == 0 {
                        if let loginData = response.data {
                            self.showToast(message: "\(loginData.otp ?? 0000)", font:.systemFont(ofSize: 15))
                            self.resetTimer()
                        }
                    } else {
                        Utility.displayBanner(message: response.message ?? "", style: .danger)
                    }
                case .failure(let error):
                    DebugLogger.error(error.localizedDescription)
                }
            }
        
        
    }
}
