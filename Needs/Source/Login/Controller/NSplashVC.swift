//
//  NSplashVC.swift
//  Needs
//
//  Created by webcastle on 09/02/22.
//

import UIKit

class NSplashVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initializeView()
    }
    
    //---------------------------------------------------------------------------------
    // MARK: - UI UPDATIONS
    //---------------------------------------------------------------------------------
    
    private func initializeView(){
        self.perform(#selector(navigate), with: nil, afterDelay: 1)
    }


    @objc private func navigate() {
        self.navigateToDashBoard()
//        let vc = Storyboards.Login.viewController(controller: NLoginVC.self)
//        self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)
    }
    
    private func reDirect(){
        if SessionHandler.isLoggedIn {
            Utility.setDashboardViewController()
        } else {
            if SessionHandler.isGuest {
                Utility.setDashboardViewController()
            }else {
                let vc = Storyboards.Login.viewController(controller: NLoginVC.self)
                self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)
            }
        }
    }
    
    private func navigateToDashBoard() {
        if SessionHandler.bearer_token == nil || SessionHandler.bearer_token == "" {
            NLoginViewModel.createToken{ [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let response):
                    if response.errorcode == 0 {
                        if let data = response.data {
                            SessionHandler.bearer_token = data
                            self.reDirect()
                        }
                    }
                case .failure(let error):
                    Utility.displayBanner(message: error.localizedDescription, style: .danger)
                }
            }
        } else {
            self.reDirect()
        }
    }
}
