//
//  NChangeLocationVC.swift
//  Needs
//
//  Created by webcastle on 09/02/22.
//

import UIKit
import Alamofire
import GooglePlaces
import GoogleMaps

class NChangeLocationVC: ParentController {

    @IBOutlet weak var tblView: UITableView!
    
    weak var delegate: NotifyDelegate?  
    var selectedIndex = 0
    var arrAddress = [AddressM]()
    var locationManager = CLLocationManager()
    var lat = 0.0
    var long = 0.0
    var loc = ""
    var city = ""
    var place : GMSPlace?
    var currentLocation: CLLocation?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        locationManager.delegate = self
        
        self.getAddressList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initializeLocation()
        
    }

    
    //---------------------------------------------------------------------------------
    // MARK: - Custom functions
    //---------------------------------------------------------------------------------

    private func configureLocationServices(){
        
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            locationManager.distanceFilter = 50
        }
    }
    
    func initializeLocation() {
        if long == 0.0, lat == 0.0{
            self.configureLocationServices()
        }else {
            locateWithLong(lon: self.long, lat: self.lat)
        }
    }
    
    func locateWithLong(lon: Double, lat: Double) {
        
        DispatchQueue.main.async {
            
            let latDouble = lat
            let lonDouble = lon
//            self.mapView.clear()
            let camera = GMSCameraPosition.camera(withLatitude: latDouble , longitude: lonDouble , zoom: 19.5)
//            self.mapView.camera = camera
            self.locationManager.stopUpdatingHeading()
        }
    }
    
    //---------------------------------------------------------------------------------
    // MARK: - Button Actions
    //---------------------------------------------------------------------------------

    @IBAction func tappedCurrentLocation(_ sender: UIButton) {
//        self.lat = 0
//        self.long = 0
//        self.loc = ""
//        self.city = ""
//        self.initializeLocation()
        let vc = Storyboards.Login.viewController(controller: NLocationVC.self)
        vc.isFromAddress = false
        self.pushVC = vc
    }
    
    @IBAction func tappedSearchLocation(_ sender: UIButton) {
        
    }
}

extension NChangeLocationVC:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }else {
            return arrAddress.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sec = indexPath.section
        let row = indexPath.row
        if sec == 0 {
            if row == 0 {
                let  cell = tableView.dequeueReusableCell(withIdentifier: "locationheaderTVC") as! locationheaderTVC
                cell.lblTitle.text = "Recent Searches"
                return cell
            }else {
                let  cell = tableView.dequeueReusableCell(withIdentifier: "RecentlistTVC") as! RecentlistTVC
                return cell
            }
        }
        else {
            if row == 0 {
            let  cell = tableView.dequeueReusableCell(withIdentifier: "locationheaderTVC") as! locationheaderTVC
                cell.lblTitle.text = "Saved Address"
            return cell
            }else {
                let  cell = tableView.dequeueReusableCell(withIdentifier: "locationlistTVC")as! locationlistTVC
                cell.configureCell(model: self.arrAddress[row - 1])
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sec = indexPath.section
        let row = indexPath.row
        if sec == 1 {
            if row != 0 {
                self.confirmLocation(index: row - 1)
            }
        }
    }
    
}

extension NChangeLocationVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Place name: \(place.name!)")
        print("Place ID: \(place.placeID!)")
        print("Place attributions: \(place.attributions)!")
        print("Place lattitude: \(place.coordinate.latitude)")
        print("Place longitude: \(place.coordinate.longitude)")
        print("Place addr: \(place.formattedAddress ?? "")")
        print("Place addr: \(place.addressComponents!)")
        locateWithLong(lon: place.coordinate.longitude ?? 0.0, lat: place.coordinate.latitude ?? 0.0)
        self.place = place
//        self.lblAddress.text = "\(place.formattedAddress!)"
//        self.lblLoc.text = "\(place.addressComponents)"
        self.lat = place.coordinate.latitude
        self.long = place.coordinate.longitude
//        self.loc = place.formattedAddress ?? ""

//        reverseGeocodeCoordinate(place.coordinate)
        self.dismiss(animated: false, completion: {
            
        })
    }
      

    func dismissView() {
        self.dismiss(animated: false, completion: {
//        self.delegate?.didTappedLocation(self.place ?? GMSPlace())
        })
    }
       
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
}

extension NChangeLocationVC: CLLocationManagerDelegate {
 
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
            print("Location: \(location)")

        locateWithLong(lon: (locationManager.location?.coordinate.longitude ?? 0)!, lat: (locationManager.location?.coordinate.latitude ?? 0)!)
        
        print("current location didupdate :\(locationManager.location?.coordinate.latitude ?? 0) \(locationManager.location?.coordinate.longitude ?? 0)")
        self.lat = locationManager.location?.coordinate.latitude ?? 0
        self.long = locationManager.location?.coordinate.longitude ?? 0
        let userLocation :CLLocation = locations[0] as CLLocation
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverse geocode")
            }
            let placemark = (placemarks ?? [CLPlacemark]()) as [CLPlacemark]
            if placemark.count > 0 {
                let placemark = placemarks![0]
                print(placemark.locality!)
                print(placemark.administrativeArea!)
                print(placemark.country!)
       
                var addressString : String = ""
                                    if placemark.subLocality != nil {
                                        addressString = addressString + placemark.subLocality! + ", "
                                    }
       //                             if address.thoroughfare != nil {
       //                                 addressString = addressString + address.thoroughfare! + ", "
       //                             }
                                    if placemark.locality != nil {
                                        addressString = addressString + placemark.locality! + ", "
                                    }
                                    if placemark.administrativeArea != nil {
                                        addressString = addressString + placemark.administrativeArea! + " "
                                    }
                                    if placemark.postalCode != nil {
                                        addressString = addressString + placemark.postalCode! + ", "
                                    }
                                   if placemark.country != nil {
                                           addressString = addressString + placemark.country! + " "
                                   }
                
                var locString : String = ""
                                        if placemark.subLocality != nil {
                                            locString = locString + placemark.subLocality!
                                        } else if placemark.locality != nil {
                                            locString = locString + placemark.locality!
                                        }else if placemark.administrativeArea != nil {
                                            locString = locString + placemark.administrativeArea!
                                        }else if placemark.country != nil {
                                            locString = locString + placemark.country!
                                        }
                var cityString: String = ""
                                        if placemark.subLocality != nil {
                                            cityString = locString + placemark.subLocality!
                                        } else if placemark.locality != nil {
                                            cityString = locString + placemark.locality!
                                        }
                self.loc = addressString
//                self.lblAddress.text = self.loc
//                self.lblLoc.text = "\(placemark.thoroughfare!),\(placemark.subAdministrativeArea!)"
                self.city = cityString
            }
        }
        locationManager.stopUpdatingLocation()

    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        let geocoder = GMSGeocoder()
        
     geocoder.reverseGeocodeCoordinate(coordinate) { [self] response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
         
            let country = address.country ?? ""
            let postalCode = address.postalCode ?? ""
            let administrativeArea =  address.administrativeArea ?? ""
            let locality = address.locality ?? ""
            let subLocality = address.subLocality ?? ""
            let thoroughfare = address.thoroughfare ?? ""
        
         var addressString : String = ""
                             if address.subLocality != nil {
                                 addressString = addressString + address.subLocality! + ", "
                             }
//                             if address.thoroughfare != nil {
//                                 addressString = addressString + address.thoroughfare! + ", "
//                             }
                             if address.locality != nil {
                                 addressString = addressString + address.locality! + ", "
                             }
                             if address.administrativeArea != nil {
                                 addressString = addressString + address.administrativeArea! + " "
                             }
                             if address.postalCode != nil {
                                 addressString = addressString + address.postalCode! + ", "
                             }
                            if address.country != nil {
                                addressString = addressString + address.country! + " "
                            }

        var locString : String = ""
                                if address.subLocality != nil {
                                    locString = locString + address.subLocality!
                                } else if address.locality != nil {
                                    locString = locString + address.locality!
                                }else if address.administrativeArea != nil {
                                    locString = locString + address.administrativeArea!
                                }else if address.country != nil {
                                    locString = locString + address.country!
                                }

        var cityString: String = ""
                                if address.subLocality != nil {
                                    cityString = locString + address.subLocality!
                                } else if address.locality != nil {
                                    cityString = locString + address.locality!
                                }
        
         print("address",address)
         print("coordinate",coordinate)
         print("country",country)
         print("postalCode",postalCode)
         print("administrativeArea",administrativeArea)
         print("locality",locality)
         print("subLocality",subLocality)
         print("thoroughfare",thoroughfare)
         print("address",addressString)

            
            self.lat = address.coordinate.latitude
            self.long = address.coordinate.longitude
            self.loc = addressString
//            self.lblAddress.text = self.loc
//            self.lblLoc.text = locString
            self.city = cityString
   
        }
    }
}

extension NChangeLocationVC {
    
    func getAddressList(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "session_id": SessionHandler.bearer_token ?? ""
        ]
        
        NCartViewModel.getAddressList(parameters: parameter) { result in
               switch result {
               case .success(let response):
                   if response.errorcode == 0 {
                       if let data = response.data {
                        self.arrAddress = [AddressM]()
                        self.arrAddress = data
                        self.tblView.reloadData()
                       }
                   } else {
                       Utility.displayBanner(message: response.message ?? "", style: .danger)
                   }
               case .failure(let error):
                   DebugLogger.error(error.localizedDescription)
               }
           }
       
   }
    
    private func confirmLocation(index: Int){
        
        let parameter:Parameters = [
            "latitude": self.arrAddress[index].lat,
            "longitude": self.arrAddress[index].long,
            "location": self.arrAddress[index].address ?? "",
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID
        ]
        
        NLoginViewModel.confirmLocation(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let loginData = response.data {
                        self.delegate?.didSelect(withInfo: ["type": AppData.changeAddress])
                        self.navigateBackward()
                    }
                } else {
                    
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
}
