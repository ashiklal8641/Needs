//
//  NLocationVC.swift
//  Needs
//
//  Created by webcastle on 09/02/22.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire

class NLocationVC: ParentController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblLoc: UILabel!
    @IBOutlet weak var viewChange: UIView!
    @IBOutlet weak var imgMarker: UIImageView!
    @IBOutlet weak var viewSnippet: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    weak var delegate: NotifyDelegate?
    
    var london: GMSMarker?
    var londonView: UIImageView?
    var lat = 0.0
    var long = 0.0
    var locationManager = CLLocationManager()
    var loc = ""
    var city = ""
    var place : GMSPlace?
    var currentLocation: CLLocation?
    var placesClient: GMSPlacesClient!
    var preciseLocationZoomLevel: Float = 15.0
    var approximateLocationZoomLevel: Float = 10.0
    var isFromAddress = false
    var isFromMap = false
    var addressModel = AddressM()
    var isFromDelivery = false
    var showRadius = false
    var radius = 0.0
    var marker = GMSMarker()
    var originLat = 0.0
    var originLong = 0.0
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mapView.delegate = self
        locationManager.delegate = self
        self.viewChange.isHidden = !isFromAddress
        self.imgMarker.isHidden = showRadius
        self.originLat = 9.98434058525753
        self.originLong = 76.70942634344101
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if showRadius {
            locateWithLong(lon: self.originLong, lat: self.originLat)
        }else {
            if long == 0.0, lat == 0.0{
                self.configureLocationServices()
            }
            else    {
                locateWithLong(lon: self.long, lat: self.lat)
            }
        }
    }
    
    
    private func configureLocationServices(){
        
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            locationManager.distanceFilter = 50
        }
    }
    

    func locateWithLong(lon: Double, lat: Double) {
        
        DispatchQueue.main.async {
            
            let latDouble = lat
            let lonDouble = lon
            self.mapView.clear()
            
            //            let position = CLLocationCoordinate2D(latitude: latDouble ?? 20.0, longitude: lonDouble ?? 10.0)
            //            let marker = GMSMarker(position: position)
            let camera = GMSCameraPosition.camera(withLatitude: latDouble , longitude: lonDouble , zoom: 19.5)
            self.mapView.camera = camera
            
            if self.showRadius {
                
                let house = UIImage(named: "loc_marker")!
                let markerView = UIImageView(image: house)
                //        markerView.tintColor = .red
                self.londonView = markerView
                
                let position = CLLocationCoordinate2D(latitude: self.originLat, longitude: self.originLong)
                let marker = GMSMarker(position: position)
                //        marker.title = "London"
                marker.iconView = markerView
                marker.tracksViewChanges = true
                marker.map = self.mapView
                marker.isDraggable = true
                self.london = marker
                
                let circleCenter = CLLocationCoordinate2D(latitude: self.originLat, longitude: self.originLong)
                
                let circle = GMSCircle(position: circleCenter, radius: self.radius)
                circle.fillColor = UIColor(red: 1.0/255.0, green: 131.0/255.0, blue: 83.0/255.0, alpha: 0.5)
                circle.strokeColor = UIColor(red: 1.0/255.0, green: 131.0/255.0, blue: 83.0/255.0, alpha: 1.0)
                circle.strokeWidth = 0.5
                circle.map = self.mapView
                
            }
            self.locationManager.stopUpdatingHeading()
        }
    }
    

    @objc private func navigate() {
        let vc = Storyboards.New.viewController(controller: NLogoutpopupVC.self)
        vc.delegate = self
        vc.page = .location
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false, completion: nil)
    }

    // MARK: - Button action
    
    @IBAction func tappedChange(_ sender: UIButton) {
        let vc = Storyboards.Login.viewController(controller: NChangeLocationVC.self)
        self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)        
    }
    
    @IBAction func tappedSearchLoc(_ sender: UIButton) {

        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }
    
    @IBAction func tappedContinue(_ sender: UIButton) {
//        let vc = Storyboards.Main.viewController(controller: NCustomTabbar.self)
//        self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)
        if showRadius {
            self.delegate?.didSelect(withInfo: ["type": AppData.location, "address": self.addressModel,"lat":  self.lat,"long":  self.long,"loc":  self.loc, "city": self.city ])
            self.navigateBackward()
        }else {
            if isFromAddress {
                if isFromMap {
                    self.delegate?.didSelect(withInfo: ["type": AppData.changeAddress, "address": self.addressModel,"lat":  self.lat,"long":  self.long,"loc":  self.loc, "city": self.city ])
                    self.navigateBackward()
                }else {
                    let vc = Storyboards.Cart.viewController(controller: NAddNewAddressVC.self)
                    vc.lat = self.lat
                    vc.long = self.long
                    vc.loc = self.loc
                    vc.city = self.city
                    vc.delegate = self
                    vc.addressModel = self.addressModel
                    self.pushVC = vc
                }
            }else {
    //            Utility.setDashboardViewController()
                self.confirmLocation()
            }
        }
         
//        self.confirmLocation()
        
    }

    
}

extension NLocationVC: NotifyDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
             if let type = dic["type"] as? String {
                switch type {
                case AppData.cntnue:
                    let acController = GMSAutocompleteViewController()
                    acController.delegate = self
                    present(acController, animated: true, completion: nil)
                    break
                
                default:
                    break
                }
            }
        }
    }
    
    
}

extension NLocationVC: GMSMapViewDelegate,CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
            print("Location: \(location)")

//        if #available(iOS 14.0, *) {
//            let zoomLevel = locationManager.accuracyAuthorization == .fullAccuracy ? preciseLocationZoomLevel : approximateLocationZoomLevel
//        } else {
//            // Fallback on earlier versions
//        }
//            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
//                                                  longitude: location.coordinate.longitude,
//                                                  zoom: 14.5)
//
//        lat = location.coordinate.latitude
//        long = location.coordinate.longitude
//            if mapView.isHidden {
//              mapView.isHidden = false
//              mapView.camera = camera
//            } else {
//              mapView.animate(to: camera)
//            }
        
        locateWithLong(lon: (locationManager.location?.coordinate.longitude ?? 0)!, lat: (locationManager.location?.coordinate.latitude ?? 0)!)
        
        print("current location didupdate :\(locationManager.location?.coordinate.latitude ?? 0) \(locationManager.location?.coordinate.longitude ?? 0)")
        self.lat = locationManager.location?.coordinate.latitude ?? 0
        self.long = locationManager.location?.coordinate.longitude ?? 0
        let userLocation :CLLocation = locations[0] as CLLocation
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverse geocode")
            }
            let placemark = (placemarks ?? [CLPlacemark]()) as [CLPlacemark]
            if placemark.count > 0 {
                let placemark = placemarks![0]
                print(placemark.locality!)
                print(placemark.administrativeArea!)
                print(placemark.country!)
       
                var addressString : String = ""
                                    if placemark.subLocality != nil {
                                        addressString = addressString + placemark.subLocality! + ", "
                                    }
       //                             if address.thoroughfare != nil {
       //                                 addressString = addressString + address.thoroughfare! + ", "
       //                             }
                                    if placemark.locality != nil {
                                        addressString = addressString + placemark.locality! + ", "
                                    }
                                    if placemark.administrativeArea != nil {
                                        addressString = addressString + placemark.administrativeArea! + " "
                                    }
                                    if placemark.postalCode != nil {
                                        addressString = addressString + placemark.postalCode! + ", "
                                    }
                                   if placemark.country != nil {
                                           addressString = addressString + placemark.country! + " "
                                   }
                
                var locString : String = ""
                                        if placemark.subLocality != nil {
                                            locString = locString + placemark.subLocality!
                                        } else if placemark.locality != nil {
                                            locString = locString + placemark.locality!
                                        }else if placemark.administrativeArea != nil {
                                            locString = locString + placemark.administrativeArea!
                                        }else if placemark.country != nil {
                                            locString = locString + placemark.country!
                                        }
                self.city = ""
                var cityString: String = ""
                                        if placemark.subLocality != nil {
                                            cityString = locString + placemark.subLocality!
                                        } else if placemark.locality != nil {
                                            cityString = locString + placemark.locality!
                                        }
                self.loc = addressString
                self.lblAddress.text = self.loc
                self.lblLoc.text = "\(placemark.thoroughfare!),\(placemark.subAdministrativeArea!)"
                self.city = cityString
            }
        }
        locationManager.stopUpdatingLocation()

    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error: \(error)")
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        if showRadius {
            let from = CLLocation(latitude: self.originLat, longitude: self.originLong)
            let to = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            let distance = from.distance(from: to)
            if radius < distance {
                self.lblTitle.text = "Order can't be deliver here"
                self.lblDesc.text = "Place pin accurately inside the circle"
            }else {
                let house = UIImage(named: "pin")
                let markerView = UIImageView(image: house)
                marker.iconView = UIImageView()
                let position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
                self.reverseGeocodeCoordinate(coordinate)
                marker = GMSMarker(position: position)
                marker.iconView = markerView
                marker.map = mapView
                self.lblTitle.text = "Order will be delivered here"
                self.lblDesc.text = "Place pin accurately on the map"
            }
        }
        print("coordintes:", coordinate)
    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        let geocoder = GMSGeocoder()
        
     geocoder.reverseGeocodeCoordinate(coordinate) { [self] response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
         
            let country = address.country ?? ""
            let postalCode = address.postalCode ?? ""
            let administrativeArea =  address.administrativeArea ?? ""
            let locality = address.locality ?? ""
            let subLocality = address.subLocality ?? ""
            let thoroughfare = address.thoroughfare ?? ""
        
         var addressString : String = ""
                             if address.subLocality != nil {
                                 addressString = addressString + address.subLocality! + ", "
                             }
//                             if address.thoroughfare != nil {
//                                 addressString = addressString + address.thoroughfare! + ", "
//                             }
                             if address.locality != nil {
                                 addressString = addressString + address.locality! + ", "
                             }
                             if address.administrativeArea != nil {
                                 addressString = addressString + address.administrativeArea! + " "
                             }
                             if address.postalCode != nil {
                                 addressString = addressString + address.postalCode! + ", "
                             }
                            if address.country != nil {
                                addressString = addressString + address.country! + " "
                            }

        var locString : String = ""
                                if address.subLocality != nil {
                                    locString = locString + address.subLocality!
                                } else if address.locality != nil {
                                    locString = locString + address.locality!
                                }else if address.administrativeArea != nil {
                                    locString = locString + address.administrativeArea!
                                }else if address.country != nil {
                                    locString = locString + address.country!
                                }
        self.city = ""
        var cityString: String = ""
                                if address.subLocality != nil {
                                    cityString = locString + address.subLocality!  + ", "
                                } else if address.locality != nil {
                                    cityString = locString + address.locality!
                                }
        
         print("address",address)
         print("coordinate",coordinate)
         print("country",country)
         print("postalCode",postalCode)
         print("administrativeArea",administrativeArea)
         print("locality",locality)
         print("subLocality",subLocality)
         print("thoroughfare",thoroughfare)
         print("address",addressString)

            
            self.lat = address.coordinate.latitude
            self.long = address.coordinate.longitude
            self.loc = addressString
            self.lblAddress.text = self.loc
            self.lblLoc.text = locString
            self.city = cityString
   
        }
    }
    
//    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
//
//        if !isMarkerWithinScreen(marker: marker, mapView) {
//            let camera = GMSCameraPosition(target: marker.position, zoom: mapView.camera.zoom)
//            self.mapView.animate(to: camera)
//        }
//
//    }
//
//    func isMarkerWithinScreen(marker: GMSMarker, _ mapView: GMSMapView) -> Bool {
//        let region = self.mapView.projection.visibleRegion()
//        let bounds = GMSCoordinateBounds(region: region)
//        return bounds.contains(marker.position)
//    }
    
}

extension NLocationVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Place name: \(place.name!)")
        print("Place ID: \(place.placeID!)")
        print("Place attributions: \(place.attributions)!")
        print("Place lattitude: \(place.coordinate.latitude)")
        print("Place longitude: \(place.coordinate.longitude)")
        print("Place addr: \(place.formattedAddress ?? "")")
        print("Place addr: \(place.addressComponents!)")
        locateWithLong(lon: place.coordinate.longitude ?? 0.0, lat: place.coordinate.latitude ?? 0.0)
        self.place = place
//        self.lblAddress.text = "\(place.formattedAddress!)"
//        self.lblLoc.text = "\(place.addressComponents)"
        self.lat = place.coordinate.latitude
        self.long = place.coordinate.longitude
//        self.loc = place.formattedAddress ?? ""

        reverseGeocodeCoordinate(place.coordinate)
        self.dismiss(animated: false, completion: {
            
        })
    }
      

    func dismissView() {
        self.dismiss(animated: false, completion: {
//        self.delegate?.didTappedLocation(self.place ?? GMSPlace())
        })
    }
       
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
}

extension NLocationVC {
    
    private func confirmLocation(){
        
        let parameter:Parameters = [
            "latitude": self.lat,
            "longitude": self.long,
            "location": self.loc,
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "city": self.city
        ]
        
        NLoginViewModel.confirmLocation(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        Utility.setDashboardViewController()
                    }
                } else {
                    self.perform(#selector(self.navigate), with: nil)
//                    self.perform(#selector(self.navigate), with: nil, afterDelay: 0.4)
//                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}

