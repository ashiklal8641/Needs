//
//  NLoginViewModel.swift
//  Needs
//
//  Created by webcastle on 04/03/22.
//

import UIKit
import Alamofire

class NLoginViewModel: NSObject {
    
    static func createToken(completion:@escaping Completion<String>){
         APIClient.performRequest(route:  APIConfiguration(request: (nil,.token)),
                                  loader: true,
                                  completion: completion)
     }
    
    static func login(parameters:Parameters,completion:@escaping Completion<LoginModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.signin)),
                                 loader: true,
                                 completion: completion)
    }

    static func verifyOtp(parameters:Parameters,completion:@escaping Completion<RegisterModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.verifyOtp)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func resendOtp(parameters:Parameters,completion:@escaping Completion<RegisterModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.resendOtp)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func confirmLocation(parameters:Parameters,completion:@escaping Completion<AddressModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.confirmLocation)),
                                 loader: true,
                                 completion: completion)
    }    

    static func guestLogin(parameters:Parameters,completion:@escaping Completion<LoginModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.skipLogin)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func registerUser(parameters:Parameters,completion:@escaping Completion<RegisterModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.register)),
                                 loader: true,
                                 completion: completion)
    }
    
}
