//
//  LoginModel.swift
//  Needs
//
//  Created by webcastle on 04/03/22.
//

import Foundation

struct LoginModel : Codable {
    var email : String?
    var isExisting : Bool?
    var isVendor : Bool?
    var name : String?
    var otp : Int?
    var phone_number : String?
    var preuser_id : Int?
    var user_id : Int?
    var session_id: String?
    var token: String?
    var status: Int?
}

struct RegisterModel : Codable{
    var user_id : Int?
    var isExisting : Bool?
    var otp : Int?
    var countryCode : String?
    var phone : String?
    var token : String?
    var steps_finished: Int?
    var user_type: Int?
    var session_id: String?
    var email:String?
    var name:String?
}

struct AddressModel : Codable{
    var available : Bool?
    var address : String?
    var id : Int?
    var type : String?
    var name: String?
    var home: String?
    var landmark: String?
    var pincode: String?
    var mobile: String?
    var lat: String?
    var long: String?
}
