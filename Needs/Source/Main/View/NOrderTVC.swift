//
//  NOrderTVC.swift
//  Needs
//
//  Created by webcastle on 13/02/22.
//

import UIKit

class NOrderTVC: UITableViewCell {

    @IBOutlet weak var imgPdt: UIImageView!
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(model: OrderM) {
        self.imgPdt.kf.setImage(with: URL(string:model.image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        self.lblOrderId.text = "Order ID: #\(model.order_no ?? "0")"
        self.lbldate.text = model.date
        self.lblItems.text = "\(model.items ?? 0) Items"
        self.lblStatus.text = model.status
        let strAmount = "Paid: ₹ \(model.amount ?? 0)"
        let range = strAmount.range(of: NSLocalizedString("Paid:", comment: "paid"))!
        let attrs = [NSAttributedString.Key.font : UIFont.robotoFont(ofSize: 12, weight: .medium),NSAttributedString.Key.foregroundColor: UIColor.init(red: 19/255.0, green: 19/255.0, blue: 19/255.0, alpha: 1.0)]
        let attrStr = NSMutableAttributedString(string: strAmount)
        attrStr.addAttributes(attrs, range: NSRange(range, in: strAmount))
        self.lblPrice.attributedText = attrStr
    }

}

