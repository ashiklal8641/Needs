//
//  NHeaderTVC.swift
//  Needs
//
//  Created by webcastle on 10/02/22.
//

import UIKit



class NHeaderTVC: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    var type: HeaderType = .newArrival {
        didSet {
            switch type {
            case .newArrival:
                self.lblTitle.text = "New Arrivals"
            case .offerZone:
                self.lblTitle.text = "Offer Zone"
            case .dealOfTheWeek:
                self.lblTitle.text = "Deal Of The Week"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
