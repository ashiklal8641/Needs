//
//  NProductTVC.swift
//  Needs
//
//  Created by webcastle on 09/02/22.
//

import UIKit




class NProductTVC: UITableViewCell {
    
    @IBOutlet weak var viewOfferPdt: UIView!
    @IBOutlet weak var viewOffer: UIView!
    @IBOutlet weak var viewDelete: UIView!
    @IBOutlet weak var viewFav: UIView!
    @IBOutlet weak var viewCartDelete: UIView!
    @IBOutlet weak var viewAddCart: UIView!
    @IBOutlet weak var viewCart: UIView!
    @IBOutlet weak var viewReturn: UIView!
    @IBOutlet weak var viewQty: UIView!
    @IBOutlet weak var viewConfig: UIView!
    @IBOutlet weak var viewCustomize: UIView!
    @IBOutlet weak var lblPdtName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblOriginalPrice: UILabel!
    @IBOutlet weak var lblOfferPdt: UILabel!
    @IBOutlet weak var lblOffer: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblPdtQty: UILabel!
    @IBOutlet weak var txtQty: UITextField!
    @IBOutlet weak var imgPdt: UIImageView!
    @IBOutlet weak var imgFav: UIImageView!

    weak var delegate: NotifyDelegate?
    var iPath: IndexPath?
    var varId = -1
    
    var type: ViewType = .product {
        didSet {
            switch type {
            case .product:
                self.viewDelete.isHidden = true
                self.viewOffer.isHidden = true
                self.viewCartDelete.isHidden = true
                self.viewReturn.isHidden = true
                self.viewQty.isHidden = true
                break
            case .wishlist:
                self.viewFav.isHidden = true
                self.viewOfferPdt.isHidden = true
                self.viewCart.isHidden = true
                self.viewAddCart.isHidden = true
                self.viewConfig.isHidden = true
                self.viewCartDelete.isHidden = true
                self.viewReturn.isHidden = true
                self.viewQty.isHidden = true
                self.viewCustomize.isHidden = true
                break
            case .monthlyBucket:
                self.viewFav.isHidden = true
                self.viewOfferPdt.isHidden = true
                self.viewConfig.isHidden = true
                self.viewDelete.isHidden = true
                self.viewReturn.isHidden = true
                self.viewQty.isHidden = true
                self.viewCustomize.isHidden = true
            case .cart:
                self.viewFav.isHidden = true
                self.viewOfferPdt.isHidden = true
                self.viewConfig.isHidden = true
                self.viewDelete.isHidden = true
                self.viewOffer.isHidden = true
                self.viewAddCart.isHidden = true
                self.viewReturn.isHidden = true
                self.viewQty.isHidden = true
                self.viewCustomize.isHidden = true
            case .order:
                self.viewFav.isHidden = true
                self.viewOfferPdt.isHidden = true
                self.viewConfig.isHidden = true
                self.viewDelete.isHidden = true
                self.viewOffer.isHidden = true
                self.viewAddCart.isHidden = true
                self.viewCartDelete.isHidden = true
                self.viewCart.isHidden = true
                self.viewCustomize.isHidden = true
            case .returnPdt:
                self.viewFav.isHidden = true
                self.viewOfferPdt.isHidden = true
                self.viewConfig.isHidden = true
                self.viewDelete.isHidden = true
                self.viewOffer.isHidden = true
                self.viewAddCart.isHidden = true
                self.viewCartDelete.isHidden = true
                self.viewCart.isHidden = true
                self.viewReturn.isHidden = true
                self.viewCustomize.isHidden = true
            case .checkOut:
                self.viewFav.isHidden = true
                self.viewOfferPdt.isHidden = true
                self.viewConfig.isHidden = true
                self.viewDelete.isHidden = true
                self.viewOffer.isHidden = true
                self.viewAddCart.isHidden = true
                self.viewCartDelete.isHidden = true
                self.viewCart.isHidden = true
                self.viewReturn.isHidden = true
                self.viewQty.isHidden = true
                self.viewCustomize.isHidden = true
                break
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(model: ProductM) {
        
        self.imgPdt.kf.setImage(with: URL(string:model.image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        self.imgFav.image = model.is_wishlist ?? false ? UIImage(named: "fav") : UIImage(named: "notFav")
        self.lblPdtName.text = model.name
        let offPercent = model.percent_off?.cleanValue
        self.lblOffer.text = "\(offPercent ?? "0")%"
        self.viewOffer.isHidden = offPercent != "0" ? false : true
        if offPercent != "0" {
            self.lblOriginalPrice.attributedText = "₹ \(model.selling_price ?? "0")".updateStrikeThroughFont(.lightGray)
            self.lblPrice.text = "₹ \(model.price ?? "0")"
        }else {
            self.lblOriginalPrice.text = " "
            self.lblPrice.text = "₹ \(model.selling_price ?? "0")"
        }
        self.lblOfferPdt.text = "\(offPercent ?? "0")%"
        self.viewConfig.isHidden = model.variants?.count ?? 0 > 0 ? false : true
        self.viewCustomize.isHidden = model.variants?.count ?? 0 > 0 ? false : true
        if model.variants?.count ?? 0 > 0 {
            self.lblQty.text = model.variants?.first?.option_values?.first?.theme_value
        }
        self.viewCart.isHidden = model.cart_quantity ?? 0 > 0 ? false : true
        self.viewAddCart.isHidden = model.cart_quantity ?? 0 > 0 ? true : false
        self.txtQty.text = "\(model.cart_quantity ?? 1)"
        if type == .order {
            self.lblPdtQty.text = "\(model.quantity ?? 0)"
        }
        
    }
    
    
    
    @IBAction func tappedReturn(_ sender: UIButton){
        self.delegate?.didSelect(withInfo: ["type": AppData.returnItem, "index": iPath ?? IndexPath(row: 0, section: 0)])
    }
    
    @IBAction func tappedVariant(_ sender: UIButton){
        self.delegate?.didSelect(withInfo: ["type": AppData.product, "index": iPath ?? IndexPath(row: 0, section: 0)])
    }
    
    @IBAction func tappedDelete(_ sender: UIButton){
        self.delegate?.didSelect(withInfo: ["type": AppData.delete, "index": iPath ?? IndexPath(row: 0, section: 0)])
    }
    
    @IBAction func tappedAddCart(_ sender: UIButton){
        self.delegate?.didSelect(withInfo: ["type": AppData.addCart, "index": iPath ?? IndexPath(row: 0, section: 0)])
    }
    
    @IBAction func tappedPlus(_ sender: UIButton){
        self.delegate?.didSelect(withInfo: ["type": AppData.add, "index": iPath ?? IndexPath(row: 0, section: 0)])
    }
    
    @IBAction func tappedMinus(_ sender: UIButton){
        self.delegate?.didSelect(withInfo: ["type": AppData.minus, "index": iPath ?? IndexPath(row: 0, section: 0)])
    }
    
    @IBAction func tappedFav(_ sender: UIButton){
        self.delegate?.didSelect(withInfo: ["type": AppData.favourite, "index": iPath ?? IndexPath(row: 0, section: 0)])
    }
}
