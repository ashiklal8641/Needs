//
//  NProductCVC.swift
//  Needs
//
//  Created by webcastle on 10/02/22.
//

import UIKit

class NProductCVC: UICollectionViewCell {

    @IBOutlet weak var lblPdtName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblSellingPrice: UILabel!
    @IBOutlet weak var lblOffer: UILabel!
    @IBOutlet weak var viewOffer: UIView!
    @IBOutlet weak var imgFav: UIImageView!
    @IBOutlet weak var imgPdt: UIImageView!
    @IBOutlet weak var viewCart: UIView!
    @IBOutlet weak var viewAddCart: UIView!
    
    weak var delegate: CVCDelegate?
    var iPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(model: ProductM) {
        self.imgPdt.kf.setImage(with: URL(string:model.image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        self.imgFav.image = model.is_wishlist ?? false ? UIImage(named: "fav") : UIImage(named: "notFav")
        self.lblPdtName.text = model.name
        let offPercent = model.percent_off?.cleanValue
        self.viewOffer.isHidden = offPercent != "0" ? false : true
        if offPercent != "0" {
            self.lblSellingPrice.attributedText = "₹ \(model.selling_price ?? "0")".updateStrikeThroughFont(.lightGray)
            self.lblPrice.text = "₹ \(model.price ?? "0")"
        }else {
            self.lblSellingPrice.text = " "
            self.lblPrice.text = "₹ \(model.selling_price ?? "0")"
        }
        self.lblOffer.text = "\(offPercent ?? "0")%"
        
//        self.viewCart.isHidden = !(model.is_cart ?? false)
//        self.viewAddCart.isHidden = model.is_cart ?? false
    }
    
    @IBAction func tappedFav(_ sender: UIButton) {
        self.delegate?.didSelect(withInfo: ["type": AppData.favourite, "index": iPath ?? IndexPath(row: 0, section: 0)])
    }
    
    @IBAction func tappedAddCart(_ sender: UIButton) {
        self.delegate?.didSelect(withInfo: ["type": AppData.addCart, "index": iPath ?? IndexPath(row: 0, section: 0)])
    }
    
    @IBAction func tappedMinus(_ sender: UIButton) {
        self.delegate?.didSelect(withInfo: ["type": AppData.minus, "index": iPath ?? IndexPath(row: 0, section: 0)])
    }
    
    @IBAction func tappedAdd(_ sender: UIButton) {
        self.delegate?.didSelect(withInfo: ["type": AppData.add, "index": iPath ?? IndexPath(row: 0, section: 0)])
    }
}
