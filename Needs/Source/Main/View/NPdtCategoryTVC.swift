//
//  NPdtCategoryTVC.swift
//  Needs
//
//  Created by webcastle on 10/02/22.
//

import UIKit

class NPdtCategoryTVC: UITableViewCell {

    @IBOutlet weak var pdtCategoryCV: UICollectionView!
    @IBOutlet weak var pdtCVHeightConstraint: NSLayoutConstraint!
    
    let arrCat:[CategoryItem] = [CategoryItem(title: "Vegetables", img: "1-vegetables"),
                 CategoryItem(title: "Fruits", img: "fruits"),
                 CategoryItem(title: "Meat", img: "meat"),
                 CategoryItem(title: "Grocery", img: "groceries"),
                 CategoryItem(title: "Fish", img: "Fish"),
                 CategoryItem(title: "Fruits", img: "fruits"),
                 CategoryItem(title: "Meat", img: "meat"),
                 CategoryItem(title: "Grocery", img: "groceries"),
                 CategoryItem(title: "Vegetables", img: "1-vegetables")]
    
    weak var delegate: NotifyDelegate?
    var arrCategory: [CategoryM]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension NPdtCategoryTVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrCategory?.count ?? 0
//        arrCat.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(NPdtCategoryCVC.self, for: indexPath)
        cell.imgCat.kf.setImage(with: URL(string:self.arrCategory?[indexPath.item].image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        cell.lblCatName.text = self.arrCategory?[indexPath.item].title
//        cell.imgCat.image = UIImage(named: arrCat[indexPath.row].img ?? "")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let width = CGFloat(90.0)
//        let height = width+20.0
        let width = collectionView.frame.size.width/3.8
        let height = width+20.0
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelect(withInfo: ["type": AppData.category, "index": indexPath])
    }
}
