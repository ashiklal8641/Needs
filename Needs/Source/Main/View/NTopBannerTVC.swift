//
//  NTopBannerTVC.swift
//  Needs
//
//  Created by webcastle on 10/02/22.
//

import UIKit

class NTopBannerTVC: UITableViewCell {

    @IBOutlet weak var topBannerCV: UICollectionView!
    @IBOutlet weak var cvHeightConstraint: NSLayoutConstraint!
    
    weak var delegate: NotifyDelegate?
    let arrBanner:[CategoryItem] = [CategoryItem(title: "Monthly Bucket", img: "basket",desc: "Add products to buy monthly"),
                                    CategoryItem(title: "More Needs", img: "more_needs", desc: "Purchase above ₹ 2500 & make a delivery free")]
    var moreNeeds = MoreNeedsM()
//    {
//        didSet {
//            self.topBannerCV.reloadData()
//            self.cvHeightConstraint.constant = self.topBannerCV.collectionViewLayout.collectionViewContentSize.height
//        }
//    }
    var monthlyBkt = MonthlyBucketM()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}

extension NTopBannerTVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(NTopBannerCVC.self, for: indexPath)
        if indexPath.item == 0 {
            cell.configureCell(model: monthlyBkt)
        }else {
            cell.configureCellWithModel(model: moreNeeds)
        }
     
//        cell.configureCellModel(model: arrBanner[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width - 10)/2
        let height:CGFloat = 200
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelect(withInfo: ["type": AppData.topBanner, "index": indexPath])
    }
}


//extension NTopBannerTVC: NotifyDelegate {
//
//    func didSelect(withInfo info: [String : Any]?) {
//        guard let info = info else { return }
//        print("info",info)
//        if let dic = info as? [String: Any] {
//            if let path = dic["indexPath"] as? IndexPath {
//                if let type = dic["type"] as? String {
//                    switch type {
//                    case AppData.topBanner:
//                        self.delegate?.didSelect(withInfo: ["type": AppData.topBanner, "index": path])
//                        break
//                    default:
//                        break
//                    }
//
//                }
//            }else if let type = dic["type"] as? String {
//                switch type {
//                case AppData.favourite:
//
//                    break
//                default:
//                    break
//                }
//            }
//        }
//    }
//
//
//}
