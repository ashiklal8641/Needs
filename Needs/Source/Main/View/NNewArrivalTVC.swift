//
//  NNewArrivalTVC.swift
//  Needs
//
//  Created by webcastle on 10/02/22.
//

import UIKit


class NNewArrivalTVC: UITableViewCell {

    @IBOutlet weak var pdtCV: UICollectionView!
    @IBOutlet weak var cvHeightConstraint: NSLayoutConstraint!
    
    weak var delegate: NotifyDelegate?
    var index: IndexPath?
    var arrNewArrivals: [ProductM]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.registerCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //---------------------------------------------------------------------------------
    // MARK: - UI UPDATIONS
    //---------------------------------------------------------------------------------
    
    private func registerCell(){
        self.pdtCV.register(NProductCVC.self)
    }

}

extension NNewArrivalTVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrNewArrivals?.count ?? 0
//        7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(NProductCVC.self, for: indexPath)
//        cell.lblSellingPrice.attributedText = "₹ 150".updateStrikeThroughFont(.lightGray)
        cell.configureCell(model: arrNewArrivals?[indexPath.item] ?? ProductM())
        cell.delegate = self
        cell.iPath = indexPath
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width/2
        let height = collectionView.frame.size.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelect(withInfo: ["type": AppData.product, "index": indexPath, "sec": self.index])
    }
}

class NDealOfTheDayTVC: UITableViewCell {

    @IBOutlet weak var pdtCV: UICollectionView!
    @IBOutlet weak var cvHeightConstraint: NSLayoutConstraint!
    weak var delegate: NotifyDelegate?
    
    var index: IndexPath?
    
    var arrProducts: [ProductM]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.registerCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //---------------------------------------------------------------------------------
    // MARK: - UI UPDATIONS
    //---------------------------------------------------------------------------------
    
    private func registerCell(){
        self.pdtCV.register(NProductCVC.self)
    }

}

extension NDealOfTheDayTVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrProducts?.count ?? 0 > 3 {
            return 4
        }else {
            return arrProducts?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(NProductCVC.self, for: indexPath)
//        cell.lblSellingPrice.attributedText = "₹ 150".updateStrikeThroughFont(.lightGray)
        cell.configureCell(model: arrProducts?[indexPath.item] ?? ProductM())
        cell.delegate = self
        cell.iPath = indexPath
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width/2
        let height = CGFloat(200.0)
        return CGSize(width: width, height: width * 1.08)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelect(withInfo: ["type": AppData.product, "index": indexPath, "sec": self.index])
    }
}

extension NDealOfTheDayTVC: CVCDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
            if let path = dic["index"] as? IndexPath {
                if let type = dic["type"] as? String {
                    switch type {
                    case AppData.favourite:
                        self.delegate?.didSelect(withInfo: ["type": AppData.favourite, "index": path, "sec": index])
                        break
                    case AppData.addCart:
                        self.delegate?.didSelect(withInfo: ["type": AppData.addCart, "index": path, "sec": index])
                        break
                    case AppData.add:
                        self.delegate?.didSelect(withInfo: ["type": AppData.add, "index": path, "sec": index])
                        break
                    case AppData.minus:
                        self.delegate?.didSelect(withInfo: ["type": AppData.minus, "index": path, "sec": index])
                        break
                    default:
                        break
                    }
                    
                }
            }
        }
    }
}

extension NNewArrivalTVC: CVCDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
            if let path = dic["index"] as? IndexPath {
                if let type = dic["type"] as? String {
                    switch type {
                    case AppData.favourite:
                        self.delegate?.didSelect(withInfo: ["type": AppData.favourite, "index": path, "sec": index])
                        break
                    case AppData.addCart:
                        self.delegate?.didSelect(withInfo: ["type": AppData.addCart, "index": path, "sec": index])
                        break
                    case AppData.add:
                        self.delegate?.didSelect(withInfo: ["type": AppData.add, "index": path, "sec": index])
                        break
                    case AppData.minus:
                        self.delegate?.didSelect(withInfo: ["type": AppData.minus, "index": path, "sec": index])
                        break
                    default:
                        break
                    }
                    
                }
            }
        }
    }
}
