//
//  NLoyaltyTVC.swift
//  Needs
//
//  Created by webcastle on 14/02/22.
//

import UIKit

class NLoyaltyTVC: UITableViewCell {

    @IBOutlet weak var viewTick: UIView!
    @IBOutlet weak var lblDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class NLoyaltyBannerTVC: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class NSplDescTVC: UITableViewCell {

    @IBOutlet weak var txtDesc: GrowingTextView!
    
    weak var delegate: NotifyDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func didTappedTextView(_ sender: UITextView) {
        self.delegate?.didSelect(withInfo: ["type": AppData.desc, "desc": sender.text ?? ""])
    }
    
}

class NOfferBannerTVC: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension NLoyaltyHistoryTVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(NLoyaltyHistoryTVC.self, for: indexPath)
       
        return cell
    }
    
    
}
