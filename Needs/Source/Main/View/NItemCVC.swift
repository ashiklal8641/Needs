//
//  NItemCVC.swift
//  Needs
//
//  Created by webcastle on 10/02/22.
//

import UIKit

class NItemCVC: UICollectionViewCell {
 
    @IBOutlet weak var lblCatName: UILabel!
    @IBOutlet weak var imgCat: UIImageView!
    
    func configureCell(model: CategoryM){
        self.lblCatName.text = model.title
        self.imgCat.kf.setImage(with: URL(string:model.image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    func configureCellWithModel(model: CategoryItem){
        self.lblCatName.text = model.title
        self.imgCat.image = UIImage(named: model.img ?? "")
    }
}

class NMainBannerCVC: UICollectionViewCell {
     
    @IBOutlet weak var imgBanner: UIImageView!
}

class NTopBannerCVC: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var imgBanner: UIImageView!
    
    func configureCell(model: MonthlyBucketM){
        self.lblTitle.text = model.title
        self.lblDesc.text = model.description
        self.imgBanner.kf.setImage(with: URL(string:model.image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    func configureCellModel(model: CategoryItem){
        self.lblTitle.text = model.title
        self.lblDesc.text = model.desc
        self.imgBanner.image = UIImage(named: model.img ?? "")
    }
    
    func configureCellWithModel(model: MoreNeedsM){
        self.lblTitle.text = model.title
        self.lblDesc.text = model.description
        self.imgBanner.kf.setImage(with: URL(string:model.image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
//        self.imgBanner.image = UIImage(named: model.img ?? "")
    }
}

class NPdtCategoryCVC: UICollectionViewCell {
    
    @IBOutlet weak var lblCatName: UILabel!
    @IBOutlet weak var imgCat: UIImageView!
    
}
