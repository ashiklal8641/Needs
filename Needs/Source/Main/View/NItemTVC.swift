//
//  NItemTVC.swift
//  Needs
//
//  Created by webcastle on 10/02/22.
//

import UIKit

class NItemTVC: UITableViewCell {

    @IBOutlet weak var itemCV: UICollectionView!
    @IBOutlet weak var itemCVHeightConstraint: NSLayoutConstraint!
    
    let arrCat:[CategoryItem] = [CategoryItem(title: "Vegetables", img: "1-vegetables"),
                 CategoryItem(title: "Fruits", img: "fruits"),
                 CategoryItem(title: "Meat", img: "meat"),
                 CategoryItem(title: "Grocery", img: "groceries"),
                 CategoryItem(title: "Fish", img: "Fish"),
                 CategoryItem(title: "Fruits", img: "fruits"),
                 CategoryItem(title: "Meat", img: "meat"),
                 CategoryItem(title: "Grocery", img: "groceries"),
                 CategoryItem(title: "Vegetables", img: "1-vegetables")]
    
    weak var delegate: NotifyDelegate?
    var arrCategory: [CategoryM]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension NItemTVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrCategory?.count ?? 0
//        arrCat.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(NItemCVC.self, for: indexPath)
        cell.configureCell(model: arrCategory?[indexPath.item] ?? CategoryM())
//        cell.configureCellWithModel(model: arrCat[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width/3
        let height = width
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelect(withInfo: ["type": AppData.category, "index": indexPath])
    }
    
}
