//
//  NAddAddressTVC.swift
//  Needs
//
//  Created by webcastle on 14/02/22.
//

import UIKit

class NAddAddressTVC: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class NSavedAddressTVC: UITableViewCell {

    @IBOutlet weak var viewChange: BaseView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    
    weak var delegate: NotifyDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func tappedChangeBtn(_ sender: UIButton) {
        self.delegate?.didSelect(withInfo: ["type": AppData.changeAddress])
    }
}
