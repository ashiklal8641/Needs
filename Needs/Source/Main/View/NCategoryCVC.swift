//
//  NCategoryCVC.swift
//  Needs
//
//  Created by webcastle on 10/02/22.
//

import UIKit

class NCategoryCVC: UICollectionViewCell {
    
    @IBOutlet weak var categorylbl: UILabel!
    @IBOutlet weak var bGview: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bGview.layer.cornerRadius = bGview.frame.height/2
    }
    
}
