//
//  NAccountTVC.swift
//  Needs
//
//  Created by webcastle on 11/02/22.
//

import UIKit

class NAccountTVC: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgMenu: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(model: ProfileMenuItem) {
        self.lblTitle.text = model.title
        self.imgMenu.image = UIImage(named: model.img ?? "")
    }
}
