//
//  NMainBannerTVC.swift
//  Needs
//
//  Created by webcastle on 10/02/22.
//

import UIKit
import FSPagerView

class NMainBannerTVC: UITableViewCell {

    @IBOutlet weak var mainBannerCV: UICollectionView!
    @IBOutlet weak var mainCVHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pageControll : FSPageControl!{
        didSet {
            self.pageControll.setPath(UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: 14, height: 5),byRoundingCorners: [.topLeft, .topRight,.bottomRight,.bottomLeft], cornerRadii: CGSize(width:14, height: 5)), for: .normal)
            self.pageControll.setPath(UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: 14, height: 5),byRoundingCorners: [.topLeft, .topRight,.bottomRight,.bottomLeft], cornerRadii: CGSize(width:14, height: 5)), for: .selected)
            
            self.pageControll.interitemSpacing = 0
            self.pageControll.itemSpacing = 18
           
            self.pageControll.contentHorizontalAlignment = .center
            self.pageControll.backgroundColor = UIColor.clear
            self.pageControll.setFillColor(#colorLiteral(red: 0.003921568627, green: 0.5137254902, blue: 0.3254901961, alpha: 1), for: .selected)
            self.pageControll.setFillColor(#colorLiteral(red: 0.8901960784, green: 0.8901960784, blue: 0.8901960784, alpha: 1), for: .normal)
            self.pageControll.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)

        }}
    
    weak var delegate: NotifyDelegate?
    var timer = Timer()
    var counter = 0
    var arrBanner: [BannerM]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        }
    }
    
    @objc func changeImage() {
        
        if counter < self.arrBanner?.count ?? 0 {
//        if counter < 3 {
            let index = IndexPath.init(item: counter, section: 0)
            self.mainBannerCV.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            pageControll.currentPage = self.counter
            self.counter += 1
        } else {
            counter = 0
            let index = IndexPath.init(item: counter, section: 0)
            self.mainBannerCV.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
            pageControll.currentPage = self.counter
            self.counter = 1
        }
            
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if mainBannerCV == scrollView {
             let pageWidth = mainBannerCV.frame.size.width
            let page = floor(Double(Int((mainBannerCV.contentOffset.x) + 1)/Int(pageWidth)))
            self.pageControll.currentPage = Int(page)
        }
    }

}

extension NMainBannerTVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrBanner?.count ?? 0
//        3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(NMainBannerCVC.self, for: indexPath)
        cell.imgBanner.kf.setImage(with: URL(string:arrBanner?[indexPath.item].image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width 
        let height = collectionView.frame.size.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelect(withInfo: ["type": AppData.banner, "index": indexPath])
    }
}
