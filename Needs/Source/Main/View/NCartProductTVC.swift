//
//  NCartProductTVC.swift
//  Needs
//
//  Created by webcastle on 24/02/22.
//

import UIKit

class NCartProductTVC: UITableViewCell {

    @IBOutlet weak var lblPdtName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblStock: UILabel!
    @IBOutlet weak var lblOriginalPrice: UILabel!
    @IBOutlet weak var txtQty: UITextField!
    @IBOutlet weak var imgPdt: UIImageView!
    
    var delegate: NotifyDelegate?
    var iPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(model: ProductM) {
        self.imgPdt.kf.setImage(with: URL(string:model.image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        self.lblPdtName.text = model.name
        self.lblOriginalPrice.attributedText = "₹ \(model.selling_price ?? "0")".updateStrikeThroughFont(.lightGray)
        self.lblPrice.text = "₹ \(model.price ?? "0")"
        self.txtQty.text = "\(model.cart_quantity ?? 1)"
        self.lblStock.text = model.stock ?? 0 > 0 ? "" : "Out of Stock"
    }

    // MARK: - Button action
    
    @IBAction func tappedDelete(_ sender: UIButton) {
        
        self.delegate?.didSelect(withInfo: ["type": AppData.delete, "index": iPath])
        
    }
    
    @IBAction func tappedCustomize(_ sender: UIButton) {
        
        self.delegate?.didSelect(withInfo: ["type": AppData.customize, "index": iPath])
        
    }
    
    @IBAction func tappedAdd(_ sender: UIButton) {
        
        self.delegate?.didSelect(withInfo: ["type": AppData.addCart, "index": iPath])
        
    }
    
    @IBAction func tappedMinus(_ sender: UIButton) {
        
        self.delegate?.didSelect(withInfo: ["type": AppData.remove, "index": iPath])
        
    }
}
