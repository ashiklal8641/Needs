//
//  CustomTabbar.swift
//
//  CustomTabbar.swift
//  BulkBuySplitter
//
//  Created by WC46 on 18/08/21.
//

import UIKit

class NCustomTabbar: UITabBarController {
    
    var prevIndex : Int = 0
    var Index : Int = 0
    var currentIndex : Int = 0
    var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        tabBar.tintColor = #colorLiteral(red: 0.003921568627, green: 0.5137254902, blue: 0.3254901961, alpha: 1)
        tabBar.unselectedItemTintColor = #colorLiteral(red: 0.1843137255, green: 0.1803921569, blue: 0.2549019608, alpha: 1)
        tabBar.isTranslucent = true
        tabBar.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 0)
        self.addObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()
        
        let appearance = UITabBarItem.appearance()
        let attributes = [NSAttributedString.Key.font:UIFont(name: "Roboto-Medium", size: 13)]
        appearance.setTitleTextAttributes(attributes as [NSAttributedString.Key : Any], for: .normal)
        
    }
    
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        prevIndex = selectedIndex
        let index = self.tabBar.items?.index(of: item)
        currentIndex = index ?? 0
        print("didSelectedTab1\(currentIndex)")
    }
    
    func addObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(listenerForCartChange(_:)), name: NSNotification.Name(rawValue: "didChangeChatCount"), object: nil)
    }
    
    @objc func listenerForCartChange(_ notification:Notification){
        DispatchQueue.main.async {
            if let tabItems = self.tabBar.items {
                // In this case we want to modify the badge number of the third tab:
                let tabItem = tabItems[1]
//                tabItem.badgeValue = AppState.chatCount == 0 ?  nil : AppState.chatCount?.description
            }
        }
    }
    
    deinit {
        print("deinit CustomTabbarContoller")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "didChangeChatCount"), object: nil)
    }
    
}


@IBDesignable class TabBarWithCorners: UITabBar {
    @IBInspectable var color: UIColor?
    @IBInspectable var radii: CGFloat = 22.0
    
    private var shapeLayer: CALayer?
    
    override func draw(_ rect: CGRect) {
        addShape()
    }
    
    private func addShape() {
        let shapeLayer = CAShapeLayer()
        
        shapeLayer.path = createPath()
        shapeLayer.strokeColor = UIColor.gray.withAlphaComponent(0.1).cgColor
        shapeLayer.fillColor = color?.cgColor ?? UIColor.white.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.shadowColor = UIColor.black.cgColor
        shapeLayer.shadowOffset = CGSize(width: 0   , height: -3);
        shapeLayer.shadowOpacity = 0.2
//        shapeLayer.shadowPath =  UIBezierPath(roundedRect: bounds, cornerRadius: radii).cgPath
        
        
        if let oldShapeLayer = self.shapeLayer {
            layer.replaceSublayer(oldShapeLayer, with: shapeLayer)
        } else {
            layer.insertSublayer(shapeLayer, at: 0)
        }
        
        self.shapeLayer = shapeLayer
    }
    
    private func createPath() -> CGPath {
//        let path = UIBezierPath(
//            roundedRect: bounds,
//            byRoundingCorners: [.topLeft, .topRight],
//            cornerRadii: CGSize(width: radii, height: 0.0))
//
//        return path.cgPath
        
        let height: CGFloat = 30.0
        let path = UIBezierPath()
        let centerWidth = self.frame.width / 2
        path.move(to: CGPoint(x: 0, y: 0)) // start top left
        path.addLine(to: CGPoint(x: (centerWidth - height * 2), y: 0)) // the beginning of the trough
        
        path.addCurve(to: CGPoint(x: centerWidth, y: -15),
                      controlPoint1: CGPoint(x: (centerWidth - 20), y: 0), controlPoint2: CGPoint(x: centerWidth - 20, y: -15))
        
        path.addCurve(to: CGPoint(x: (centerWidth + height * 2), y: 0),
                      controlPoint1: CGPoint(x: centerWidth + 20, y: -15), controlPoint2: CGPoint(x: (centerWidth + 20), y: 0))
        
        path.addLine(to: CGPoint(x: self.frame.width, y: 0))
        path.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height))
        path.addLine(to: CGPoint(x: 0, y: self.frame.height))
        path.close()
        
        return path.cgPath
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.isTranslucent = true
        var tabFrame            = self.frame
        tabFrame.size.height    = 65 + (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? CGFloat.zero)
        tabFrame.origin.y       = self.frame.origin.y +   ( self.frame.height - 65 - (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? CGFloat.zero))
        self.layer.cornerRadius = 20
        self.frame            = tabFrame
        
        
        
        self.items?.forEach({ $0.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: -5.0) })
        
        
    }
    
}


extension NCustomTabbar: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
    }
    
    //Delegate methods
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        print("Should select viewController: \(viewController.title ?? "") ?")
        return true;
    }
    
}
