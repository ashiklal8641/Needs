//
//  NOfferVC.swift
//  Needs
//
//  Created by webcastle on 09/02/22.
//

import UIKit
import Alamofire

class NProductListVC: ParentController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var shortCartView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var catCollectionView: UICollectionView!
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var imgBtn: UIImageView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var lblCartCount: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    var selectedIndex = 0
    var selCatIndex = 0
    var pdtPageType:PageType = .offerZone
    var type = -1
    var catId = -1
    var page = 1
    var lastPage = 0
    var isFromTab = true
    var pdtList = [ProductM]()
    var arrCat = [CategoryM]()
    var pdtModel = ProductM()
    var OptionModel = [Option]()
    var OptionItemModel = [OptionItems]()
    var selectedId = -1
    weak var delegate: NotifyDelegate!
    var arrMainCat = [CategoryM]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.registerCell()
        self.initializeView()
        self.btnDropDown.isHidden = isFromTab        
        //        self.imgBtn.isHidden = isFromTab
        self.shortCartView.isHidden = isFromTab
        if pdtPageType == .category {
            self.lblTitle.text = self.arrMainCat[selCatIndex].title
            self.catId = self.arrMainCat[selCatIndex].id ?? -1
            self.getSubCategoryList()
        }
        self.getProductList(catId: self.catId)
        self.getShortCartInfo()
    }
    
    //    MARK: - Button actions
    
    @IBAction func tappedDropDown(_ sender: UIButton) {
        let vc = Storyboards.Home.viewController(controller: NCategoryDropDownVc.self)
        vc.delegate = self
        vc.selectedIndex = self.selCatIndex
        vc.arrCat = self.arrMainCat
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func tappedSearch(_ sender: UIButton) {
        let vc = Storyboards.Home.viewController(controller: NSearchVC.self)
        vc.type = self.pdtPageType.rawValue
        self.pushVC = vc
    }
    
    @IBAction func tappedViewCart(_ sender: UIButton) {
        let vc = Storyboards.Main.viewController(controller: NCartVC.self)
        vc.isFromPdt = true
        self.pushVC = vc
    }
    
    //---------------------------------------------------------------------------------
    // MARK: - UI UPDATIONS
    //---------------------------------------------------------------------------------
    
    private func registerCell(){
        self.tblView.register(NProductTVC.self)
    }
    
    func initializeView() {
        switch pdtPageType {
        case .newArrival:
            self.lblTitle.text = "New Arrival"
            self.imgBtn.isHidden = true
            self.btnDropDown.isEnabled = false
            self.heightConstraint.constant = 0
            self.type = PageType.newArrival.rawValue
            break
        case .dealOfTheWeek:
            self.lblTitle.text = "Deal Of The Week"
            self.imgBtn.isHidden = true
            self.btnDropDown.isEnabled = false
            self.heightConstraint.constant = 0
            self.type = PageType.dealOfTheWeek.rawValue
            break
        case .offerZone:
            self.lblTitle.text = "Offer Zone"
            self.imgBtn.isHidden = true
            self.btnDropDown.isEnabled = false
            self.heightConstraint.constant = 0
            self.type = PageType.offerZone.rawValue
            break
        case .category:
            self.lblTitle.text = "Fruits"
            self.imgBtn.isHidden = false
            self.btnDropDown.isEnabled = true
            self.heightConstraint.constant = 70
            self.type = PageType.category.rawValue
            break
            
        default:
           break
        }
        
        self.btnView.isHidden = isFromTab
    }
    
/*    func setui(model: ProductM)
    {
        self.OptionModel = [Option]()
        self.OptionItemModel = [OptionItems]()
        
        for i in 0..<(model.variants?.count ?? 0)
        {
            
            let variant = model.variants?[i]
            for k in 0..<(model.variants?[i].option_values?.count ?? 0)
            {
                if OptionModel.count == 0
                {
                    let value = OptionItems(productId: [variant?.id ?? 0], themeId: variant?.option_values?[k].theme_id ?? 0, themeName: variant?.option_values?[k].theme_name ?? "", themeValue: variant?.option_values?[k].theme_value ?? "",themecolor:"2")
                    OptionModel.append(Option(optionTitle: variant?.option_values?[k].theme_name ?? "", themeId: variant?.option_values?[k].theme_id ?? 0, optionItems:[value]))
                    self.selectedId = variant?.id ?? 0
                }
                else
                {
                    
                    for var opt in OptionModel
                    {
                        
                        if opt.themeId == (variant?.option_values?[k].theme_id ?? 0)
                        {
                            
                            
                            for var optitem in opt.optionItems
                            {
                                
                                if optitem.themeValue == (variant?.option_values?[k].theme_value ?? "")
                                {

                                    optitem.productId?.append(variant?.id ?? 0)
//                                    if self.selectedId == variant?.id {
//                                        optitem.themecolor = "2"
//                                    }
                                }
                                else
                                {
                                    
                                    opt.optionItems.append(OptionItems(productId: [variant?.id ?? 0], themeId: variant?.option_values?[k].theme_id ?? 0, themeName: variant?.option_values?[k].theme_name ?? "", themeValue: variant?.option_values?[k].theme_value ?? "",themecolor:"1"))
                                    for i in 0..<OptionModel.count
                                    {


                                        if OptionModel[i].optionTitle == variant?.option_values?[k].theme_name ?? ""
                                        {
                                            print("newcount\(OptionModel[i].optionItems[0].themeValue)")
                                            OptionModel[i] = opt


                                        }
                                    }
                                    
                                    
                                }
                                
                            }
                            
                        }
                        else
                        {
                            
                            
                            if !OptionModel.contains(where: { (theme) -> Bool in
                                
                                theme.optionTitle == variant?.option_values?[k].theme_name ?? ""
                                
                            })
                            {
                                
                                let value = OptionItems(productId: [variant?.id ?? 0] , themeId: variant?.option_values?[k].theme_id ?? 0, themeName: variant?.option_values?[k].theme_name ?? "", themeValue: variant?.option_values?[k].theme_value ?? "",themecolor:"1")
                                OptionModel.append(Option(optionTitle: variant?.option_values?[k].theme_name ?? "", themeId: variant?.option_values?[k].theme_id ?? 0, optionItems:[value]))
                                
                            }
                            
                        }
                        
                    }
                    
                    
                    
                }
                
            }
            
        }
        
        for i in 0 ..< OptionModel.count
        {
            OptionModel[i].optionItems = OptionModel[i].optionItems.unique{$0.themeValue}
            print("hello1\(OptionModel[i].optionItems.count)")
        }
    }
    */
    
    func setupui(model: ProductM) {
        self.OptionModel = [Option]()
        self.OptionItemModel = [OptionItems]()
        
        for i in 0..<(model.variants?.count ?? 0)
        {
            
            let variant = model.variants?[i]
            for k in 0..<(model.variants?[i].option_values?.count ?? 0)
            {
                if OptionModel.count == 0
                {
                    let value = OptionItems(prodId: [variant?.id ?? 0], id: variant?.option_values?[k].theme_id ?? 0, name: variant?.option_values?[k].theme_name ?? "", value: variant?.option_values?[k].theme_value ?? "",color:"2")
                    OptionModel.append(Option(title: variant?.option_values?[k].theme_name ?? "", id: variant?.option_values?[k].theme_id ?? 0, items:[value]))
                    self.selectedId = variant?.id ?? 0
                }
                else
                {
                    let optitem = OptionModel.filter { $0.themeId == variant?.option_values?[k].theme_id}
                    if optitem.count <= 0 {
                        let value = OptionItems(prodId: [variant?.id ?? 0], id: variant?.option_values?[k].theme_id ?? 0, name: variant?.option_values?[k].theme_name ?? "", value: variant?.option_values?[k].theme_value ?? "",color:"2")
                        OptionModel.append(Option(title: variant?.option_values?[k].theme_name ?? "", id: variant?.option_values?[k].theme_id ?? 0, items:[value]))
                        self.selectedId = variant?.id ?? 0
                    }else {
                        for opt in optitem {
                            let optionitem = opt.optionItems.filter { $0.themeValue == variant?.option_values?[k].theme_value}
                            if optionitem.count <= 0 {
                                opt.optionItems.append(OptionItems(prodId: [variant?.id ?? 0], id: variant?.option_values?[k].theme_id ?? 0, name: variant?.option_values?[k].theme_name ?? "", value: variant?.option_values?[k].theme_value ?? "",color:"1"))
                            }else {
                                for item in optionitem {
                                    item.productId?.append(variant?.id ?? 0)
                                }
                            }
                        }
                        
                    }
                                        
                }
                
            }
            
        }
    }
    
}

extension NProductListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrCat.count > 0 {
            return arrCat.count+1
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"NCategoryCVC", for: indexPath) as! NCategoryCVC
        if indexPath.item == 0 {
            cell.categorylbl.text = "All"
        }else {
            cell.categorylbl.text = self.arrCat[(indexPath.item) - 1].title
        }
        DispatchQueue.main.async {
            cell.bGview.layer.cornerRadius = cell.bGview.frame.height/2
        }
        if selectedIndex == indexPath.row {
            cell.bGview.backgroundColor = #colorLiteral(red: 0.003921568627, green: 0.5137254902, blue: 0.3254901961, alpha: 1)
            cell.categorylbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        } else {
            cell.bGview.backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9803921569, blue: 0.9725490196, alpha: 1)
            cell.categorylbl.textColor = #colorLiteral(red: 0.2420214713, green: 0.2405150235, blue: 0.3237009943, alpha: 1)
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let Categorylbl = UILabel()
        if indexPath.item == 0 {
            Categorylbl.text = "All"
        }else {
            Categorylbl.text = self.arrCat[(indexPath.item) - 1].title
        }
        Categorylbl.sizeToFit()
        let width = Categorylbl.frame.size.width + 40
        return CGSize(width: width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        catCollectionView.reloadData()
        if indexPath.item == 0 {
            self.getProductList(catId: self.catId)
        }else {
            self.getProductList(catId: arrCat[selectedIndex - 1].id ?? -1)
        }
        self.catCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
    }
}

extension NProductListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.pdtList.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(NProductTVC.self, for: indexPath)
        cell.configureCell(model: self.pdtList[indexPath.row])
        cell.delegate = self
        cell.iPath = indexPath
        cell.type = .product
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Storyboards.Home.viewController(controller: NProductDetailVC.self)
        vc.delegate = self
        vc.pdtId = self.pdtList[indexPath.row].id ?? -1
        vc.slug = self.pdtList[indexPath.row].slug ?? ""
        self.pushVC = vc
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if page < lastPage {
//            page += 1
//            self.getProductList(catId: arrCat[selectedIndex].id ?? -1)
//        }
    }
    
    
    
}

extension NProductListVC: NotifyDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
            if let path = dic["index"] as? IndexPath {
                if let type = dic["type"] as? String {
                    switch type {
                    case AppData.category:
                        self.lblTitle.text = arrMainCat[path.item].title
                        self.selectedIndex = 0
                        self.selCatIndex = path.item
                        //                        self.catId = arrCat[path.item].id ?? -1
                        self.catId = arrMainCat[path.item].id ?? -1
//                        self.catCollectionView.reloadData()
//                        self.catCollectionView.scrollToItem(at: IndexPath(item: selectedIndex, section: path.section), at: .centeredHorizontally, animated: false)
                        self.getSubCategoryList()
                        self.getProductList(catId: self.catId)
                        self.catCollectionView.reloadData()
                        break
                        
                    case AppData.product:
                        
                        let model = self.pdtList[path.row]
//                        self.setui(model: model)
                        self.setupui(model: model)
                        let vc = Storyboards.Cart.viewController(controller: NProductVariantVC.self)
                        vc.delegate = self
                        vc.variantProductId = self.selectedId
                        vc.pdtModel = model
                        vc.OptionModel = self.OptionModel
                        vc.modalPresentationStyle = .overFullScreen
                        self.present(vc, animated: false, completion: nil)
                        break
                        
                    case AppData.favourite:
                        self.updateWishList(index: path.row)
                        break
                        
                    case AppData.addCart:
                        let model = self.pdtList[path.row]
                        if model.stock == 0  {
                            Utility.displayBanner(message: "Product is Out of Stock", style: .danger)
                            return
                        }
                        if model.variants?.count ?? 0 > 0 {
                            //                        self.setui(model: model)
                            self.setupui(model: model)
                            let vc = Storyboards.Cart.viewController(controller: NProductVariantVC.self)
                            vc.delegate = self
                            vc.iPath = path
                            vc.variantProductId = self.selectedId
                            vc.pdtModel = model
                            vc.OptionModel = self.OptionModel
                            vc.modalPresentationStyle = .overFullScreen
                            self.present(vc, animated: false, completion: nil)
                        }else {
                            self.addToCart(index: path.row)
                        }
                        break

                    case AppData.add:
                        
                        let model = self.pdtList[path.row]
                        if model.variants?.count ?? 0 > 0 {
                            self.updateCart(index: path.row,id: self.pdtList[path.row].id ?? -1, isFirst: true, isAdd: true, isFromCart: false)
//                            self.setupui(model: model)
//                            let vc = Storyboards.Cart.viewController(controller: NProductVariantVC.self)
//                            vc.delegate = self
//                            vc.variantProductId = self.selectedId
//                            vc.isFromCart = true
//                            vc.modalPresentationStyle = .overFullScreen
//                            self.present(vc, animated: false, completion: nil)
                        }else {
                            self.updateCart(index: path.row, id: self.pdtList[path.row].id ?? -1, isFirst: false, isAdd: true, isFromCart: false)
                        }
 
                        break
                        
                    case AppData.cart:
                        self.getProductList(catId: self.catId)
                        break
                        
                    case AppData.minus:
                        
                        let model = self.pdtList[path.row]
                        if model.variants?.count ?? 0 > 0 {
                            var count = self.pdtList[path.row].cart_quantity
                            if count ?? 0 > 1 {
                                let vc = Storyboards.New.viewController(controller: NLogoutpopupVC.self)
                                vc.delegate = self
                                vc.page = .delete
                                vc.iPath = path
                                vc.modalPresentationStyle = .overFullScreen
                                self.present(vc, animated: false, completion: nil)
                            }else {
                                count = (count ?? 0) - 1
                                self.updateCart(index: path.row, id: self.pdtList[path.row].id ?? -1, isFirst: false, isAdd: false, isFromCart: false)
                            }
                        }else {
                            self.updateCart(index: path.row, id: self.pdtList[path.row].id ?? -1, isFirst: false, isAdd: false, isFromCart: false)
                        }
//                        var count = self.pdtList[path.row].cart_quantity
//                        if count ?? 0 > 1 {
//                            count = (count ?? 0) - 1
//                        }else {
//                            count = (count ?? 0) - 1
//                        }
                        break
                        
                    case AppData.yes:
                        let vc = Storyboards.Main.viewController(controller: NCartVC.self)
                        vc.isFromPdt = true
                        self.pushVC = vc
                        break
                        
                    default:
                        break
                    }
                    
                }
            }else if let type = dic["type"] as? String {
                switch type {
                case AppData.save:
                    //                    let vc = Storyboards.Cart.viewController(controller: NChoosePickUpTypeVC.self)
                    //                    self.pushVC = vc
                    break
                case AppData.addCart, AppData.addToCart:
                    self.getProductList(catId: self.catId)
                    break
                    
                default:
                    break
                }
            }
        }
    }
    
    
}

extension NProductListVC {
    
    func getProductList(catId: Int){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "type": type,
            "id": catId,
            "batchSize": 3,
            "page": page
        ]
        
        NHomeViewModel.getProductList(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.pdtList = [ProductM]()
                        self.pdtList = data.data ?? [ProductM]()
                        self.lastPage = data.last_page ?? 0
                        self.page = data.current_page ?? 1
                        self.viewNoData.isHidden = self.pdtList.count > 0 ? true: false                        
                        self.tblView.reloadData()
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func getSubCategoryList(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "id": catId,
            "batchSize": 20,
            "page": page
        ]
        
        NHomeViewModel.getSubCategoryList(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.arrCat = [CategoryM]()
                        self.arrCat = data
                        self.catCollectionView.reloadData()
                    }
                } else {
                    self.arrCat = [CategoryM]()
                    self.catCollectionView.reloadData()
//                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func updateWishList(index: Int){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "product_id": self.pdtList[index].id ?? 0
        ]
        
        NHomeViewModel.updateWishList(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        let tvc = self.tblView.cellForRow(at: IndexPath(row: index, section: 0)) as! NProductTVC
                        self.pdtList[index].is_wishlist = !(self.pdtList[index].is_wishlist ?? false)
                        self.pdtList[index].is_wishlist ?? false ? Utility.displayBanner(message: "Product added to wishlist", style: .success) : Utility.displayBanner(message: "Product removed from wishlist", style: .success)
                        tvc.imgFav.image = self.pdtList[index].is_wishlist ?? false ? UIImage(named: "fav") : UIImage(named: "notFav")
                        self.delegate.didSelect(withInfo: ["type": AppData.favourite])
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func addToCart(index: Int){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "product_id": self.pdtList[index].id ?? 0,
            "quantity": 1
        ]
        
        NHomeViewModel.addToCart(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
//                    self.addCart.isHidden = !(self.pdtModel.is_cart ?? false)
//                    self.lblAddCart.text = self.pdtModel.is_cart ?? false ? "View Cart" : "Add Cart"
                    let tvc = self.tblView.cellForRow(at: IndexPath(row: index, section: 0)) as! NProductTVC
                    tvc.txtQty.text = "1"
                    tvc.viewAddCart.isHidden = true
                    tvc.viewCart.isHidden = false
                    self.pdtList[index].is_cart = true
                    self.pdtList[index].cart_quantity = 1
//                    self.getProductList(catId: self.catId)
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func updateCart(index: Int ,id: Int, isFirst: Bool, isAdd: Bool, isFromCart: Bool) {
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "product_id": id,
            "firstTime": isFirst,
            "fromCart": isFromCart,
            "isAdd": isAdd ? 1 : 0
        ]
        
        NHomeViewModel.updateCart(parameters: parameter) { result in
            switch result {
            case .success(let response):
                let model = self.pdtList[index]
                if response.errorcode == 0 {
                    let cartCount = model.cart_quantity
                    var count = Int(cartCount ?? 0)
                    let tvc = self.tblView.cellForRow(at: IndexPath(row: index, section: 0)) as! NProductTVC
                    if isAdd {
                        if count > 0 {
                            count = count  + 1
                            tvc.txtQty.text = String(count)
                        }
                        tvc.viewAddCart.isHidden = true
                        tvc.viewCart.isHidden = false
                        self.pdtList[index].is_cart = true
                        self.pdtList[index].cart_quantity = count
                    }else {
                        
                        if count > 1 {
                            count = count - 1
                            tvc.txtQty.text = String(count)
                            self.pdtList[index].is_cart = true
                        }else {
                            count = count - 1
                            tvc.txtQty.text = String(count)
                            tvc.viewAddCart.isHidden = false
                            tvc.viewCart.isHidden = true
                            self.pdtList[index].is_cart = false
                        }
                        self.pdtList[index].cart_quantity = count
                    }
//                    self.getProductList(catId: self.catId)
                    
                } else if response.errorcode == 1 {
                    if let data = response.data {
                        if isFirst {
                            self.setupui(model: model)
                            let vc = Storyboards.Cart.viewController(controller: NProductVariantVC.self)
                            vc.delegate = self
                            vc.variantPdtModel = data
                            vc.pdtModel = model
                            vc.OptionModel = self.OptionModel
                            vc.variantProductId = data.previousAddedProductId ?? -1
                            vc.isFromCart = true
                            vc.modalPresentationStyle = .overFullScreen
                            self.present(vc, animated: false, completion: nil)
                        }else {
                            if isAdd {
                                
                            }else {
                                
                            }
//                            self.getProductList(catId: self.catId)
                        }
                    }
                    
                }else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func getShortCartInfo(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID
        ]
        
        NHomeViewModel.getShortCartInfo(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {                        
                        self.shortCartView.isHidden = data.cart_count ?? 0 > 0 ? false : true
                        self.lblCartCount.text = "\(data.cart_count ?? 0) Items"
                        self.lblPrice.text = "₹ \(data.total_price ?? "0000")"
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}




//                        let tvc = tblView.cellForRow(at: IndexPath(row: path.row, section: 0)) as! NProductTVC
//                        let cartCount = tvc.txtQty.text ?? "0"
//                        var count = Int(cartCount)
//                        if count ?? 0 > 0 {
//                            count = (count ?? 0) + 1
//                            self.updateCart(id: self.pdtList[path.row].id ?? -1, isFirst: true, isAdd: true, isFromCart: false)
//                            tvc.txtQty.text = String(count ?? 0)
//                        }
//                        tvc.viewAddCart.isHidden = true
