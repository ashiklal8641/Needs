//
//  NHomeVC.swift
//  Needs
//
//  Created by webcastle on 09/02/22.
//

import UIKit
import  Alamofire

class NHomeVC: ParentController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblLicense: UILabel!
    
    var homeData: HomeModel?
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getDashboardDetails()
    }
    

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
    
    // MARK: - Button action
    
    @IBAction func tappedContinue(_ sender: UIButton) {

    }
    
    @IBAction func tappedNotif(_ sender: UIButton) {
        let vc = Storyboards.Cart.viewController(controller: NNotificationVC.self)        
        self.pushVC = vc
    }
    
    @IBAction func tappedSearch(_ sender: UIButton) {
        let vc = Storyboards.Home.viewController(controller: NSearchVC.self)
        self.pushVC = vc
    }

    @IBAction func tappedLocation(_ sender: UIButton) {
        let vc = Storyboards.Login.viewController(controller: NChangeLocationVC.self)
        vc.delegate = self
        self.pushVC = vc
    }
}

extension NHomeVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
          return  3
        case 1:
            if homeData?.new_arrivals?.count ?? 0 > 0{
                return  2
            }
            return  0
        case 2:
          return  2
        case 3:
            if homeData?.offer_zone?.count ?? 0 > 0{
                return  2
            }
            return 0
        case 4:
          return  1
        case 5:
            if homeData?.deal_of_the_week?.count ?? 0 > 0{
                return  2
            }
          return  0
        default:
            return  0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        switch section {
        case 0:
            if row == 0 {
                
                let cell = tableView.dequeueReusableCell(NTopBannerTVC.self, for: indexPath)
                cell.monthlyBkt = self.homeData?.monthly_bucket ?? MonthlyBucketM()
                cell.moreNeeds = self.homeData?.more_needs ?? MoreNeedsM()
                cell.topBannerCV.reloadData()
                cell.cvHeightConstraint.constant = cell.topBannerCV.collectionViewLayout.collectionViewContentSize.height
                cell.delegate = self
                return cell
                
            }else if row == 1 {
                
                let cell = tableView.dequeueReusableCell(NItemTVC.self, for: indexPath)
                cell.arrCategory = self.homeData?.category ?? [CategoryM]()
                cell.itemCV.reloadData()
                cell.itemCVHeightConstraint.constant = cell.itemCV.collectionViewLayout.collectionViewContentSize.height
                cell.delegate = self
                return cell
                
            }else {
                
                let cell = tableView.dequeueReusableCell(NMainBannerTVC.self, for: indexPath)
                cell.arrBanner = self.homeData?.main_banner ?? [BannerM]()
                cell.pageControll.numberOfPages = self.homeData?.main_banner?.count ?? 0
                cell.mainBannerCV.reloadData()
                cell.delegate = self
                return cell
                
            }
        case 1:
            if row == 0 {
                let cell = tableView.dequeueReusableCell(NHeaderTVC.self, for: indexPath)
                cell.type = .newArrival
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(NNewArrivalTVC.self, for: indexPath)
                cell.arrNewArrivals = self.homeData?.new_arrivals ?? [ProductM]()
                cell.index = indexPath
                cell.delegate = self
                return cell
            }
        case 2:
            if row == 0 {
               let cell = tableView.dequeueReusableCell(NBannerTVC.self, for: indexPath)
                cell.imgBanner.kf.setImage(with: URL(string:self.homeData?.big_sale?.image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
               return cell
           }else {
               let cell = tableView.dequeueReusableCell(NPdtCategoryTVC.self, for: indexPath)
               cell.arrCategory = self.homeData?.category ?? [CategoryM]()
               cell.pdtCategoryCV.reloadData()
               cell.pdtCVHeightConstraint.constant = cell.pdtCategoryCV.collectionViewLayout.collectionViewContentSize.height
               cell.delegate = self
               return cell
           }
        case 3:
            if row == 0 {
                let cell = tableView.dequeueReusableCell(NHeaderTVC.self, for: indexPath)
                cell.type = .offerZone
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(NDealOfTheDayTVC.self, for: indexPath)
                cell.arrProducts = self.homeData?.offer_zone ?? [ProductM]()
                cell.index = indexPath
                cell.pdtCV.reloadData()
                cell.cvHeightConstraint.constant = cell.pdtCV.collectionViewLayout.collectionViewContentSize.height
                cell.delegate = self
                return cell
            }
        case 4:
            let cell = tableView.dequeueReusableCell(NBannerTVC.self, for: indexPath)
            cell.imgBanner.kf.setImage(with: URL(string:self.homeData?.mega_offer?.image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
            return cell
        case 5:
            if row == 0 {
                let cell = tableView.dequeueReusableCell(NHeaderTVC.self, for: indexPath)
                cell.type = .dealOfTheWeek
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(NDealOfTheDayTVC.self, for: indexPath)
                cell.arrProducts = self.homeData?.deal_of_the_week ?? [ProductM]()
                cell.index = indexPath
                cell.pdtCV.reloadData()
                cell.cvHeightConstraint.constant = cell.pdtCV.collectionViewLayout.collectionViewContentSize.height
                cell.delegate = self
                return cell
            }
        default:
            return  UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let row = indexPath.row
        if section == 1  {
            if row == 0 {
                let vc = Storyboards.Main.viewController(controller: NProductListVC.self)
                vc.isFromTab = false
                vc.pdtPageType = .newArrival
                vc.delegate = self
                self.pushVC = vc
            }
        }else  if section == 3  {
            if row == 0 {
                let vc = Storyboards.Main.viewController(controller: NProductListVC.self)
                vc.pdtPageType = .offerZone
                vc.isFromTab = false
                vc.delegate = self
                self.pushVC = vc
            }
        }else  if section == 5  {
            if row == 0 {
                let vc = Storyboards.Main.viewController(controller: NProductListVC.self)
                vc.pdtPageType = .dealOfTheWeek
                vc.isFromTab = false
                vc.delegate = self
                self.pushVC = vc
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
}

extension NHomeVC: NotifyDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
            if let path = dic["index"] as? IndexPath {
                if let type = dic["type"] as? String {
                    switch type {
                    case AppData.topBanner:
                        if path.item == 0 {
                            let vc = Storyboards.Home.viewController(controller: NWishListVC.self)
                            vc.isFromWishList = false
                            self.pushVC = vc
                        }else {
                            let vc = Storyboards.Home.viewController(controller: NMoreNeedsVC.self)             
                            self.pushVC = vc
                        }
                        break
                    case AppData.product:
                        let index = dic["sec"] as? IndexPath
                        let vc = Storyboards.Home.viewController(controller: NProductDetailVC.self)
                        let sec = index?.section
                        switch sec {
                        case 1:
                            vc.pdtId = self.homeData?.new_arrivals?[path.row].id ?? -1
                            vc.slug = self.homeData?.new_arrivals?[path.row].slug ?? ""
                            break
                        case 3:
                            vc.pdtId = self.homeData?.offer_zone?[path.row].id ?? -1
                            vc.slug = self.homeData?.offer_zone?[path.row].slug ?? ""
                            break
                        case 5:
                            vc.pdtId = self.homeData?.deal_of_the_week?[path.row].id ?? -1
                            vc.slug = self.homeData?.deal_of_the_week?[path.row].slug ?? ""
                            break
                        default:
                            break
                        }
                        self.pushVC = vc
                        break
                        
                    case AppData.banner:
                        
//                        let vc = Storyboards.Home.viewController(controller: NProductDetailVC.self)
//                        self.pushVC = vc
                        break
                        
                    case AppData.category:
                        
                        let vc = Storyboards.Main.viewController(controller: NProductListVC.self)
                        vc.pdtPageType = .category
                        vc.arrMainCat = self.homeData?.category ?? [CategoryM]()
                        vc.selCatIndex = path.item
                        vc.delegate = self
                        vc.isFromTab = false
                        self.pushVC = vc
                        
                    case AppData.favourite:
                        if let index = dic["sec"] as? IndexPath {
                            let sec = index.section
                            switch sec {
                            case 1:
                                if let tvc = self.tblView.cellForRow(at: index) as? NNewArrivalTVC {
                                    let cvc = tvc.pdtCV.cellForItem(at: path) as! NProductCVC
                                    self.homeData?.new_arrivals?[path.row].is_wishlist = !(self.homeData?.new_arrivals?[path.row].is_wishlist ?? false)
                                    self.homeData?.new_arrivals?[path.row].is_wishlist ?? false ? Utility.displayBanner(message: "Product added to wishlist", style: .success) : Utility.displayBanner(message: "Product removed from wishlist", style: .success)
                                    cvc.imgFav.image = self.homeData?.new_arrivals?[path.row].is_wishlist ?? false ? UIImage(named: "fav") : UIImage(named: "notFav")
                                }
                                break
                            case 3:
                                if let tvc = self.tblView.cellForRow(at: index) as? NDealOfTheDayTVC {
                                    let cvc = tvc.pdtCV.cellForItem(at: path) as! NProductCVC
                                    self.homeData?.offer_zone?[path.row].is_wishlist = !(self.homeData?.offer_zone?[path.row].is_wishlist ?? false)
                                    self.homeData?.offer_zone?[path.row].is_wishlist ?? false ? Utility.displayBanner(message: "Product added to wishlist", style: .success) : Utility.displayBanner(message: "Product removed from wishlist", style: .success)
                                    cvc.imgFav.image = self.homeData?.offer_zone?[path.row].is_wishlist ?? false ? UIImage(named: "fav") : UIImage(named: "notFav")
                                }
                                
                                break
                            case 5:
                                if let tvc = self.tblView.cellForRow(at: index) as? NDealOfTheDayTVC {
                                    let cvc = tvc.pdtCV.cellForItem(at: path) as! NProductCVC
                                    self.homeData?.deal_of_the_week?[path.row].is_wishlist = !(self.homeData?.deal_of_the_week?[path.row].is_wishlist ?? false)
                                    self.homeData?.deal_of_the_week?[path.row].is_wishlist ?? false ? Utility.displayBanner(message: "Product added to wishlist", style: .success) : Utility.displayBanner(message: "Product removed from wishlist", style: .success)
                                    cvc.imgFav.image = self.homeData?.deal_of_the_week?[path.row].is_wishlist ?? false ? UIImage(named: "fav") : UIImage(named: "notFav")
                                }
                                
                                break
                            default:
                                break
                            }
                            
                            
                        }
                        
                        break
                    default:
                        break
                    }
                    
                }
            }else if let type = dic["type"] as? String {
                switch type {
                case AppData.favourite:
                    
                    break
                    
                case AppData.changeAddress:
                    self.getDashboardDetails()
                    break
                    
                default:
                    break
                }
            }
        }
    }
    
    
}

extension NHomeVC {
    
     func getDashboardDetails(){
         
            let parameter:Parameters = [
                "user_id": SessionHandler.uID ?? SessionHandler.preUserID
            ]
        
            NHomeViewModel.getDashboardDetails(parameters: parameter) { result in
                switch result {
                case .success(let response):
                    if response.errorcode == 0 {
                        if let data = response.data {
                            self.homeData = HomeModel()
                            self.homeData = data
                            self.lblAddress.text = "License Number : \(self.homeData?.about_app?.license_no ?? "0") \n\(self.homeData?.about_app?.address ?? "0")"
                            self.lblLocation.text = self.homeData?.location
                            self.tblView.reloadData()
                        }
                    } else {
                        Utility.displayBanner(message: response.message ?? "", style: .danger)
                    }
                case .failure(let error):
                    DebugLogger.error(error.localizedDescription)
                }
            }
        
    }
 
}
