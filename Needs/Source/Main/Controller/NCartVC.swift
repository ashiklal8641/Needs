//
//  NCartVC.swift
//  Needs
//
//  Created by webcastle on 09/02/22.
//

import UIKit
import Alamofire
import Razorpay

class NCartVC: ParentController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var proceedView: UIView!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var viewNoData: UIView!
    
    var isCheckOut = false
    var isFromPdt = false
    var isFromAddress = false
    var cartModel = CartM()
    var addId = -1
    var isLoyality = false
    var isOfferApplied = false
    var isCouponApplied = false
    var descptn = ""
    var loyalityPoints = 0
    var couponId = ""
    var razorpay : RazorpayCheckout!
    var paymentKey = "rzp_test_CCbSyEjgrTpxGW"
    var razorderid = ""
    var Amount = ""
    var OrderID = 0
    var razpaymentID = "0"
    var type = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.registerCell()
        self.proceedView.isHidden = isCheckOut
        self.btnView.isHidden = !isCheckOut
        if !isCheckOut {
            if isFromPdt {
                self.btnView.isHidden = false
            }
        }
        razorpay = RazorpayCheckout.initWithKey(paymentKey, andDelegateWithData: self)
        self.lblTitle.text = isCheckOut ? "Checkout" : "Cart"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        razorpay = RazorpayCheckout.initWithKey(paymentKey, andDelegateWithData: self)
        if isCheckOut {
        self.getCheckOutDetails()
        }else {
            self.getCartDetails()
        }
    }

    //---------------------------------------------------------------------------------
    // MARK: - UI UPDATIONS
    //---------------------------------------------------------------------------------
    
    private func registerCell(){
        self.tblView.register(NCartProductTVC.self)
        self.tblView.register(NProductTVC.self)
    }

    //---------------------------------------------------------------------------------
    // MARK: - Button Actions
    //---------------------------------------------------------------------------------
    
    @IBAction func tappedCheckout(_ sender: UIButton) {
//        if isCheckOut {
            let vc = Storyboards.Cart.viewController(controller: NPaymentMethodVC.self)
            vc.delegate = self
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: false, completion: nil)
//        }else {
//            let vc = Storyboards.Cart.viewController(controller: NChoosePickUpTypeVC.self)
//            self.pushVC = vc
//        }
    }
    
    @IBAction func tappedProceed(_ sender: UIButton) {
//        if isCheckOut {
//            let vc = Storyboards.Cart.viewController(controller: NPaymentMethodVC.self)
//            vc.delegate = self
//            vc.modalPresentationStyle = .overFullScreen
//            self.present(vc, animated: false, completion: nil)
//        }else {
        if SessionHandler.isGuest {
            let vc = Storyboards.New.viewController(controller: NLogoutpopupVC.self)
            vc.delegate = self
            vc.page = .login
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: false, completion: nil)
        }else {
            if !(self.cartModel.isAddressAddedd ?? false) {
                Utility.displayBanner(message: "Please add address", style: .danger)
                return
            }
            self.checkOutOfStockCart()
        }        
//        }
    }
}

extension NCartVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if isCheckOut {
                return self.cartModel.products?.count ?? 0 > 0 ? (self.cartModel.products?.count ?? 0)+2 : 0
            }else {
                return self.cartModel.products?.count ?? 0 > 0 ? (self.cartModel.products?.count ?? 0)+1 : 0
            }
        }else if section == 1 {
            if isCheckOut {
                return self.isCouponApplied ? 1 : 0
            }else {
                return self.cartModel.isCouponAvailable ?? false ? 1 : 0
            }
        }else if section == 2 {
            if self.cartModel.loyalityPoints != 0 {
                if isCheckOut {
                    if self.cartModel.isLoyalityApplied ?? false{
                        return 1
                    }else {
                        return 0
                    }
                }else {
                    return 2
                }
            }else {
                return 0
            }
        }else if section == 3 {
            if isCheckOut {
                if isOfferApplied {
                    return 2
                }
                return 1
            }else {
                return 1
            }
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                if isCheckOut {
                    let cell = tableView.dequeueReusableCell(NSavedAddressTVC.self, for: indexPath)
                    if self.cartModel.address != nil {
                        cell.viewChange.isHidden = true
                        cell.lblName.text = self.cartModel.address?.name
//                        let mob = LocalPersistant.string(for: .phone_number) // \nMobile: +91 \(mob ?? "")
                        cell.lblAddress.text = "\(self.cartModel.address?.home ?? ""),\(self.cartModel.address?.address ?? ""),\(self.cartModel.address?.landmark ?? ""),\(self.cartModel.address?.pincode ?? "")"
                        if AddressType(rawValue: self.cartModel.address?.type ?? 0) == .home {
                            cell.lblType.text = "Home"
                        }else if AddressType(rawValue: self.cartModel.address?.type ?? 0) == .office {
                            cell.lblType.text = "Office"
                        }else {
                            cell.lblType.text = "Other"
                        }
                        cell.viewChange.isHidden = true
                    }
                    return cell
                }else {
                    if self.cartModel.isAddressAddedd ?? false {
                        let cell = tableView.dequeueReusableCell(NSavedAddressTVC.self, for: indexPath)
                        cell.viewChange.isHidden = false
                        cell.lblName.text = self.cartModel.address?.name
                        cell.lblAddress.text = "\(self.cartModel.address?.home ?? ""),\(self.cartModel.address?.address ?? ""),\(self.cartModel.address?.landmark ?? ""),\(self.cartModel.address?.pincode ?? "") \nMobile: +91 \(self.cartModel.address?.mobile ?? "")"
                        if AddressType(rawValue: self.cartModel.address?.type ?? 0) == .home {
                            cell.lblType.text = "Home"
                        }else if AddressType(rawValue: self.cartModel.address?.type ?? 0) == .office {
                            cell.lblType.text = "Office"
                        }else {
                            cell.lblType.text = "Other"
                        }
                        cell.delegate = self
                        return cell
                    }else {
                        let cell = tableView.dequeueReusableCell(NAddAddressTVC.self, for: indexPath)
                        return cell
                    }
                }
            }else {
                if isCheckOut {
                    if indexPath.row == (self.cartModel.products?.count ?? 0)+1 {
                        let cell = tableView.dequeueReusableCell(NSplDescTVC.self, for: indexPath)
                        return cell
                    }else {
                        let cell = tableView.dequeueReusableCell(NProductTVC.self, for: indexPath)
                        cell.configureCell(model: self.cartModel.products?[(indexPath.row) - 1] ?? ProductM())
                        cell.iPath = indexPath
                        cell.delegate = self
                        cell.type = .checkOut
                        return cell
                    }
                }else {
                    let cell = tableView.dequeueReusableCell(NCartProductTVC.self, for: indexPath)
                    cell.configureCell(model: self.cartModel.products?[(indexPath.row) - 1] ?? ProductM())
                    cell.iPath = indexPath
                    cell.delegate = self
                    return cell
                }
            }
        }else if indexPath.section == 1 {
            if isCheckOut {
                let cell = tableView.dequeueReusableCell(NApplyCouponTVC.self, for: indexPath)
                cell.lblDesc.text = "Coupon Code Applied"
                cell.imgArrow.isHidden = true
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(NApplyCouponTVC.self, for: indexPath)
                cell.lblDesc.text = "Apply Coupon"
                return cell
            }
        } else if indexPath.section == 2 {
            if isCheckOut {
                let cell = tableView.dequeueReusableCell(NLoyaltyTVC.self, for: indexPath)
                cell.viewTick.isHidden = true
                cell.lblDesc.text = "Loyalty point Redeemed"
                return cell
            }else {
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(NLoyaltyBannerTVC.self, for: indexPath)
                    return cell
                }else {
                    let cell = tableView.dequeueReusableCell(NLoyaltyTVC.self, for: indexPath)
                    return cell
                }
            }
        } else {
            if isCheckOut {
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(NPaymentDetailsTVC.self, for: indexPath)
                    let count = self.cartModel.payment_details?.count ?? 0
                    var count1 = 0
                    var item = String()
                    var itemprice = String()
                    for i in 0 ..< count {
                        count1 = count1+1
                        if count1 == count
                        {
                            
                            item.append("\(self.cartModel.payment_details?[i].text ?? "")")
                            itemprice.append("\(self.cartModel.payment_details?[i].value ?? "")")
                        }
                        else
                        {
                            item.append("\(self.cartModel.payment_details?[i].text ?? "")\n\n")
                            itemprice.append("\(self.cartModel.payment_details?[i].value ?? "")\n\n")
                        }
                    }
                    
                    cell.lblTitle.text = item
                    cell.lblValue.text = itemprice
                    cell.lblTotal.text = "₹ \(self.cartModel.grand_total ?? "")"
                    return cell
                }else {
                    let cell = tableView.dequeueReusableCell(NOfferBannerTVC.self, for: indexPath)
                    return cell
                }
            }else {
                let cell = tableView.dequeueReusableCell(NPaymentDetailsTVC.self, for: indexPath)
                let count = self.cartModel.payment_details?.count ?? 0
                var count1 = 0
                var item = String()
                var itemprice = String()
                for i in 0 ..< count {
                    count1 = count1+1
                    if count1 == count
                    {
                        
                        item.append("\(self.cartModel.payment_details?[i].text ?? "")")
                        itemprice.append("\(self.cartModel.payment_details?[i].value ?? "")")
                    }
                    else
                    {
                        item.append("\(self.cartModel.payment_details?[i].text ?? "")\n\n")
                        itemprice.append("\(self.cartModel.payment_details?[i].value ?? "")\n\n")
                    }
                }
                
                cell.lblTitle.text = item
                cell.lblValue.text = itemprice
                cell.lblTotal.text = "₹ \(self.cartModel.grand_total ?? "")"
                return cell

            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                if !isCheckOut {
                    let vc = Storyboards.Cart.viewController(controller: NChooseAddressVC.self)
                    vc.delegate = self
                    vc.modalPresentationStyle = .overFullScreen
                    self.present(vc, animated: false, completion: nil)
                }
            }
        }else {
            if indexPath.row == 0 {
                if !isCheckOut {
                    let vc = Storyboards.New.viewController(controller: NApplyCouponVC.self)
                    vc.amount = self.cartModel.grand_total ?? ""
                    self.pushVC = vc
                }
            }
        }
    }
    
    
    
}

extension NCartVC: NotifyDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
            if let path = dic["index"] as? IndexPath {
                if let type = dic["type"] as? String {
                    switch type {
                    case AppData.topBanner:
                      
                        break
                        
                    case AppData.delete:
                        let vc = Storyboards.New.viewController(controller: NLogoutpopupVC.self)
                        vc.delegate = self
                        vc.page = .cart
                        vc.iPath = path
                        vc.modalPresentationStyle = .overFullScreen
                        self.present(vc, animated: false, completion: nil)
                        break
                        
                    case AppData.yes:
                        self.removeFromCart(id: self.cartModel.products?[(path.row) - 1].id ?? -1)
                        break
                        
                    case AppData.addCart:
                        if self.cartModel.products?[(path.row) - 1].stock ?? 0 <= 0 {
                            let vc = Storyboards.New.viewController(controller: NLogoutpopupVC.self)
                            vc.delegate = self
                            vc.page = .outOfStock
                            vc.modalPresentationStyle = .overFullScreen
                            self.present(vc, animated: false, completion: nil)
                          return
                        }
                        self.updateCart(id: self.cartModel.products?[(path.row) - 1].id ?? -1, isFirst: false, isAdd: true, isFromCart: false)
                        break
                        
                    case AppData.remove:
                        
                        self.updateCart(id: self.cartModel.products?[(path.row) - 1].id ?? -1, isFirst: false, isAdd: false, isFromCart: false)
                        
                        break
                        
                    case AppData.edit:
                        
                        let model = dic["model"] as? AddressM
                        let vc = Storyboards.Cart.viewController(controller: NAddNewAddressVC.self)
                        vc.delegate = self
                        vc.isFromEdit = true
                        vc.addressModel = model ?? AddressM()
                        vc.long = Double(model?.long ?? "0") ?? 0.0
                        vc.lat = Double(model?.lat ?? "0") ?? 0.0
                        self.pushVC = vc
                        
                        break
                        
                        
                    default:
                        break
                    }
                    
                }
            }else if let type = dic["type"] as? String {
                switch type {
                case AppData.save:
//                    let vc = Storyboards.Cart.viewController(controller: NChoosePickUpTypeVC.self)
//                    self.pushVC = vc
//                    let vc = Storyboards.Cart.viewController(controller: NChooseAddressVC.self)
//                    vc.delegate = self
//                    vc.modalPresentationStyle = .overFullScreen
//                    self.present(vc, animated: false, completion: nil)
                    let add_id = dic["address_id"] as? Int
                    self.addId = add_id ?? -1
                    self.getCartDetails()
                    
                    
                    break
                case AppData.addAddress:
                    let vc = Storyboards.Cart.viewController(controller: NAddNewAddressVC.self)
                    vc.delegate = self
                    self.pushVC = vc
                    break
//                case AppData.delete:
//                    let vc = Storyboards.New.viewController(controller: NLogoutpopupVC.self)
//                    vc.delegate = self
//                    vc.isDelete = true
//                    vc.modalPresentationStyle = .overFullScreen
//                    self.present(vc, animated: false, completion: nil)
//                    break
                case AppData.changeAddress:
                    let vc = Storyboards.Cart.viewController(controller: NChooseAddressVC.self)
                    vc.delegate = self
                    vc.modalPresentationStyle = .overFullScreen
                    self.present(vc, animated: false, completion: nil)
                    break
                    
                case AppData.cntnue:
                    let iscod = dic["method"] as? Bool
                    self.placeOrder(isCod: iscod ?? false)
                    break
                case AppData.customize:
//                    let vc = Storyboards.Cart.viewController(controller: NOrderPlacedVC.self)
//                    self.pushVC = vc
                    let vc = Storyboards.Cart.viewController(controller: NProductVariantVC.self)
                    vc.isFromCart = true
                    vc.modalPresentationStyle = .overFullScreen
                    self.present(vc, animated: false, completion: nil)
                    break
                case AppData.desc:
                    let desc = dic["desc"] as? String
                    self.descptn = desc ?? ""
                    break
                    
                case AppData.login:
                    SessionHandler.isGuestCheckOut = true
                    let vc = Storyboards.Login.viewController(controller: NLoginVC.self)
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)
                    break
                    
                default:
                    break
                }
            }
        }
    }
    
    
}

extension NCartVC {
    
    func getCartDetails() {
        
        var parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "isloyality": isLoyality
        ]
        
        if addId != -1 {
            parameter["address_id"] = addId
        }
        
        NMainViewModel.getCartDetails(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.cartModel = CartM()
                        self.cartModel = data
                        self.addId = self.cartModel.address?.id ?? -1
                        self.lblTotal.text = "₹ \(self.cartModel.grand_total ?? "")"
                        self.viewNoData.isHidden = self.cartModel.products?.count ?? 0 > 0 ? true: false
                        self.tblView.reloadData()
                    }
                } else {
                   
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func getCheckOutDetails(){
        
        self.viewNoData.isHidden = true
        var parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "isOfferApplied": isOfferApplied,
            "isloyality": isLoyality,
            "address_id": addId
        ]
        
        if type != -1{
            parameter["type"] = type
        }
        
        NMainViewModel.getCheckOutDetails(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.cartModel = CartM()
                        self.cartModel = data
                        self.addId = self.cartModel.address?.id ?? -1
                        self.lblTotal.text = "₹ \(self.cartModel.grand_total ?? "")"
                        self.tblView.reloadData()
                    }
                } else {
                   
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func removeFromCart(id: Int){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "product_id": id,
        ]
        
        NHomeViewModel.removeCart(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                        self.getCartDetails()
                } else {
                   
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func updateCart(id: Int, isFirst: Bool, isAdd: Bool, isFromCart: Bool){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "product_id": id,
            "firstTime": isFirst,
            "fromCart": isFromCart,
            "isAdd": isAdd ? 1 : 0
        ]
        
        NHomeViewModel.updateCart(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    self.getCartDetails()
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func checkOutOfStockCart(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID
        ]
        
        NHomeViewModel.checkOutOfStock(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        if data.isOutOfStock ?? false {
                            let vc = Storyboards.New.viewController(controller: NLogoutpopupVC.self)
                            vc.delegate = self
                            vc.page = .cartOutOfStock
                            vc.modalPresentationStyle = .overFullScreen
                            self.present(vc, animated: false, completion: nil)
                        }else {
                            if self.cartModel.isOfferAvailable ?? false {
                                let vc = Storyboards.Cart.viewController(controller: NChoosePickUpTypeVC.self)
                                vc.radius = self.cartModel.pickupRadius ?? 0.0
                                self.pushVC = vc
                            }else {
                                let vc = Storyboards.Main.viewController(controller: NCartVC.self)
                                vc.isCheckOut = true
                                self.pushVC = vc
                            }
                        }
                    }
                    
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func placeOrder(isCod: Bool){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "address_id": addId,
            "payment_method": isCod ? "cod" : "online",
            "coupon_id": 0,
            "loyality_points": "",
        ]
        
        NHomeViewModel.placeOrder(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        if !isCod {
                            self.Amount = "\(data.amount ?? 0)"
                            self.razorderid = data.razorpay_order_id ?? ""
                            self.OrderID = data.order_id ?? 0
                            var contact = ""
                            var email = ""
                           
                            let mail = LocalPersistant.string(for: .email)
                            let phone = LocalPersistant.string(for: .phone_number)
//                            {
                            contact = phone ?? ""
                            email = mail ?? ""
                                let options: [String:Any] = ["amount":"\(self.Amount )","order_id":"\(self.razorderid)",
                                                             "name": "Needs", "description":"Purchase description","image": UIImage(named: "AppIcon") ?? UIImage(),  "prefill": [ "contact": contact, "email": email ], "theme": [ "color": "#018353" ]]
                                self.razorpay.open(options, displayController: self)
//                            }
                        }else {
                            let vc = Storyboards.Cart.viewController(controller: NOrderPlacedVC.self)
                            self.pushVC = vc
                        }
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func checkPaymentResponse(status: Int, razOrderId: String, paymentId: String){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "raz_transaction_id": paymentId,
            "order_id": self.OrderID,
            "total_amount": self.Amount,
            "razorpay_order_id": razOrderId,
            "status": status
        ]
        
        NHomeViewModel.checkPaymentResponse(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        let vc = Storyboards.Cart.viewController(controller: NOrderPlacedVC.self)
                        self.pushVC = vc
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}

extension NCartVC: RazorpayPaymentCompletionProtocol,RazorpayPaymentCompletionProtocolWithData {
    
    func onPaymentError(_ code: Int32, description str: String) {
        print("error: ", code, str)
        self.presentAlert(withTitle: "Alert", message: str)
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        print("success: ", payment_id)
        self.presentAlert(withTitle: "Success", message: "Payment Succeeded")
    }
    
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]?) {
        print("error: ", str)
        let data = response?["metadata"] as? [AnyHashable: Any]
            self.checkPaymentResponse(status: 0,razOrderId:data?["order_id"] as? String ?? "",paymentId:data?["payment_id"] as? String ?? "")
//        self.checkPaymentRespomse(status: 0)
    }
    
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]?) {
        print("success: ", payment_id)
        razpaymentID = payment_id
        self.checkPaymentResponse(status: 1,razOrderId:response?["order_id"] as? String ?? "",paymentId:response?["payment_id"] as? String ?? "")
    }
    
    func presentAlert(withTitle title: String?, message : String?) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "Okay", style: .default)
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
