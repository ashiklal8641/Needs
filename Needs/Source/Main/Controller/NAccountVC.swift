//
//  NAccountVC.swift
//  Needs
//
//  Created by webcastle on 09/02/22.
//

import UIKit
import Alamofire

class NAccountVC: ParentController {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblLoyaltyPoint: UILabel!
    
    var dataSource: [ProfileMenuItem] = ProfileMenuDisplayM.menuItems
    var profileModel = ProfileM()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.lblName.text = "Hi George!"
//        self.lblPhone.text = "+91 0000000000"
//        self.lblEmail.text = "George123@gmail.com"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getProfileDetails()
    }

    // MARK: - Button action
    
    @IBAction func tappedLoyality(_ sender: UIButton) {
        let vc = Storyboards.New.viewController(controller: NLoyaltyPointsVC.self)       
        self.pushVC = vc
    }
    
    @IBAction func tappedEdit(_ sender: UIButton) {
        if SessionHandler.isGuest {
            let vc = Storyboards.New.viewController(controller: NLogoutpopupVC.self)
            vc.delegate = self
            vc.page = .login
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: false, completion: nil)
        }else {
            let vc = Storyboards.New.viewController(controller: NEditProfileVC.self)
            vc.delegate = self
            vc.profileModel = self.profileModel
            self.pushVC = vc
        }
    }
    
}

extension NAccountVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(NAccountTVC.self, for: indexPath)
        cell.configureCell(model: dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let type = dataSource[indexPath.row].type
        switch type {
        case .wishlist:
            if SessionHandler.isGuest {
                let vc = Storyboards.New.viewController(controller: NLogoutpopupVC.self)
                vc.delegate = self
                vc.page = .login
                vc.modalPresentationStyle = .overFullScreen
                self.present(vc, animated: false, completion: nil)
            }else {
                let vc = Storyboards.Home.viewController(controller: NWishListVC.self)
                vc.isFromWishList = true
                self.pushVC = vc
            }
        case .address:
            if SessionHandler.isGuest {
                let vc = Storyboards.New.viewController(controller: NLogoutpopupVC.self)
                vc.delegate = self
                vc.page = .login
                vc.modalPresentationStyle = .overFullScreen
                self.present(vc, animated: false, completion: nil)
            }else {
                let vc = Storyboards.New.viewController(controller: NManageAddressVC.self)
                self.pushVC = vc
            }
        case .invite:
            if SessionHandler.isGuest {
                let vc = Storyboards.New.viewController(controller: NLogoutpopupVC.self)
                vc.delegate = self
                vc.page = .login
                vc.modalPresentationStyle = .overFullScreen
                self.present(vc, animated: false, completion: nil)
            }else {
                let vc = Storyboards.New.viewController(controller: NInviteFriendVC.self)
                self.pushVC = vc
            }
        case .about:
            let vc = Storyboards.New.viewController(controller: NAboutVC.self)
            self.pushVC = vc
        case .faq:
            let vc  = Storyboards.New.viewController(controller: NFAQVC.self)
            self.pushVC = vc
        case.privacy:
            let vc  = Storyboards.New.viewController(controller: NPrivacyVC.self)
            self.pushVC = vc
        case.terms:
            let vc  = Storyboards.New.viewController(controller: NTandCVC.self)
            self.pushVC = vc
        case.contact:
            let vc = Storyboards.New.viewController(controller: NContactVC.self)
            self.pushVC = vc
        case.logout:
            if SessionHandler.isGuest {
                let vc = Storyboards.New.viewController(controller: NLogoutpopupVC.self)
                vc.delegate = self
                vc.page = .login
                vc.modalPresentationStyle = .overFullScreen
                self.present(vc, animated: false, completion: nil)
            }else {
                let vc = Storyboards.New.viewController(controller: NLogoutpopupVC.self)
                vc.delegate = self
                vc.isLogout = true
                vc.modalPresentationStyle = .overFullScreen
                self.present(vc, animated: false, completion: nil)
            }
        default:
            break
        }
    }
}

extension NAccountVC: NotifyDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
             if let type = dic["type"] as? String {
                switch type {
                case AppData.cntnue:
                    self.logout()
//                    let vc = Storyboards.Login.viewController(controller: NLoginVC.self)
//                    let nav = UINavigationController(rootViewController: vc)
//                    nav.isNavigationBarHidden = true
//                    Utility.resetRootViewComtroller(viewController: nav)
                    break
                case AppData.edit:
                    self.getProfileDetails()
                    
                case AppData.login:
                    
                    let vc = Storyboards.Login.viewController(controller: NLoginVC.self)
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.push(viewController: vc, transitionType: CATransitionType.fade.rawValue)
                    break
                
                default:
                    break
                }
            }
        }
    }
    
    
}

extension NAccountVC {
    
    private func getProfileDetails(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID
        ]
        
        NMainViewModel.getProfileDetails(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.profileModel = ProfileM()
                        self.profileModel = data
                        self.lblName.text = "Hi \(data.name ?? "User")!"
                        self.lblPhone.text = "+91 \(data.mobile ?? "0000000000")"
                        self.lblEmail.text = data.email
                        self.lblLoyaltyPoint.text = data.loyality_points?.cleanValue
                    }
                } else {
                   
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func logout() {
        
        let parameter:Parameters = [
          "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
          "session_id": SessionHandler.bearer_token ?? ""
        ]
       
        NMainViewModel.logOutUser(parameters: parameter){ [weak self] result in
   
            guard let self = self else { return }
            switch result {
            case .success(let response):
                print(response)
                if response.errorcode == 0 {
                    Utility.clearDefualts()
//                    self.setToRootView()
                    let vc = Storyboards.Login.viewController(controller: NSplashVC.self)
                    let nav = UINavigationController(rootViewController: vc)
                    nav.isNavigationBarHidden = true
                    Utility.resetRootViewComtroller(viewController: nav)
                }
            case .failure(let error):
                Utility.displayBanner(message: error.localizedDescription, style: .danger)
            }
        }

    }
}
