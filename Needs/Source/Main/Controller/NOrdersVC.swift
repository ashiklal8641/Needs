//
//  NOrdersVC.swift
//  Needs
//
//  Created by webcastle on 09/02/22.
//

import UIKit
import Alamofire

class NOrdersVC: ParentController {

    @IBOutlet weak var tblView: UITableView!
    
    var arrOrders = [OrderM]()
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getOrderList()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NOrdersVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(NOrderTVC.self, for: indexPath)
        cell.configureCell(model: self.arrOrders[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Storyboards.Home.viewController(controller: NOrderDetailVC.self)
        vc.orderId = self.arrOrders[indexPath.row].id ?? -1
        self.pushVC = vc
    }
    
}

extension NOrdersVC {
    
    func getOrderList(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID
        ]
        
        NMainViewModel.getOrerList(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.arrOrders = [OrderM]()
                        self.arrOrders = data
                        self.tblView.reloadData()
                    }
                } else {
                   
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}
