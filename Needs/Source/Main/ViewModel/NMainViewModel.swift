//
//  NMainViewModel.swift
//  Needs
//
//  Created by webcastle on 10/03/22.
//

import UIKit
import  Alamofire

class NMainViewModel: NSObject {

    static func getProfileDetails(parameters:Parameters,completion:@escaping Completion<ProfileM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getProfileDetails)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func editProfileDetails(parameters:Parameters,completion:@escaping Completion<ProfileM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.EditProfileDetails)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func logOutUser(parameters:Parameters,completion:@escaping Completion<[ProfileM]>) {
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.logoutUser)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getFaqDetails(parameters:Parameters,completion:@escaping Completion<ProfileM>){
         APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getFaq)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getAboutUsDetails(parameters:Parameters,completion:@escaping Completion<AboutM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getAboutUs)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getTermsAndCondition(parameters:Parameters,completion:@escaping Completion<AboutM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getTermsConditions)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getPrivacyPolicy(parameters:Parameters,completion:@escaping Completion<AboutM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getprivacypolicy)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getReturnPolicy(parameters:Parameters,completion:@escaping Completion<AboutM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getReturnPolicy)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getCartDetails(parameters:Parameters,completion:@escaping Completion<CartM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.myCart)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getCheckOutDetails(parameters:Parameters,completion:@escaping Completion<CartM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.checkout)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getOrerList(parameters:Parameters,completion:@escaping Completion<[OrderM]>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getOrderList)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getOrerDetail(parameters:Parameters,completion:@escaping Completion<OrderM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getOrderDetail)),
                                 loader: true,
                                 completion: completion)
    }
    
}
