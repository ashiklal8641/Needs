//
//  ProfileModel.swift
//  Needs
//
//  Created by webcastle on 11/02/22.
//

import Foundation

struct ProfileMenuDisplayM {
    
    static var menuItems : [ProfileMenuItem] {
        get {
            let array = [ProfileMenuItem(title: "Wishlist", img: "wishlist", type: .wishlist),
                         ProfileMenuItem(title: "Manage Address", img: "address", type: .address),
                         ProfileMenuItem(title: "Invite Friend", img: "invite", type: .invite),
                         ProfileMenuItem(title: "FAQ's", img: "faq", type: .faq),
                         ProfileMenuItem(title: "About", img: "about", type: .about),
                         ProfileMenuItem(title: "Terms & Conditions", img: "terms", type: .terms),
                         ProfileMenuItem(title: "Privacy Policy", img: "privacy", type: .privacy),
                         ProfileMenuItem(title: "Contact Us", img: "contact", type: .contact),
                         ProfileMenuItem(title: "Logout", img: "logout", type: .logout)]
            
            return array
            
        }
    }
    
}


struct ProfileMenuItem {
    var title: String?
    var img: String?
    var type: ProfileCellType?
}

struct CategoryItem {
    var title: String?
    var img: String?
    var desc: String?
}


struct ProfileM : Codable{
    var address : String?
    var email : String?
    var name: String?
    var mobile: String?
    var loyality_points: Double?
}

struct PaymentM : Codable{
    var text : String?
    var value : String?
}

struct CartM : Codable{
    var products : [ProductM]?
    var address : AddressM?
    var isAddressAddedd: Bool?
    var isOfferAvailable: Bool?
    var isOfferApplied: Bool?
    var pickupRadius: Double?
    var isCouponApplied: Bool?
    var payment_details: [PaymentM]?
    var grand_total: String?
    var loyalityPoints: Double?
    var isLoyalityApplied: Bool?
    var isCouponCode: String?
    var offer_type: String?
    var isCouponAvailable: Bool?
}

struct OrderM : Codable{
    var id : Int?
    var image : String?
    var order_no: String?
    var date: String?
    var items: Int?
    var amount: Double?
    var status: String?
    var is_failed: Bool?
    var is_cancel: Bool?
    var status_list: [StatusM]?
    var deliver_address_id: Int?
    var deliver_address: AddressModel?
    var products: [ProductM]?
    var payment_method: String?
    var price_details: [PaymentM]?
    var sub_total: String?
    var cod_charge: Double?
    var tax: String?
    var shipping_cost: String?
    var coupon_price: String?
    var grand_total: String?
    var is_delivered: Bool?
}

struct StatusM : Codable{
    var name : String?
    var slug : String?
    var date: String?
}
