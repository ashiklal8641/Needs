//
//  NewModel.swift
//  Needs
//
//  Created by webcastle on 24/03/22.
//

import Foundation

class FaqModel: Codable {
    var answer: String?
    var id: Int?
    var question: String?
    var isSelected: Bool?
}

class FaqListModel: Codable {
    var faq: [FaqModel]?
}

class CouponListModel: Codable {
    var available_coupons: [CouponModel]?
    var unavailable_coupons: [CouponModel]?
}

class CouponModel: Codable {
    var id: Int?
    var code: String?
    var title: String?
    var sub_title: String?
}

class ContactModel: Codable {
    var address: String?
    var email: String?
    var contact_no: String?
    var description: String?
}
