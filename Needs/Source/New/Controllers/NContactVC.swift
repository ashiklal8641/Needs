//
//  NContactVC.swift
//  Needs
//
//  Created by Admin on 22/02/22.
//

import UIKit
import Alamofire

class NContactVC: UIViewController {

    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblMail: UILabel!
    @IBOutlet weak var lblMob: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    var contactModel = ContactModel()
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getContactDetails()
    }
    


    // MARK: - Custom Functions
    
    func setUpUi() {
        self.lblAddress.text = contactModel.address
        self.lblMail.text = contactModel.email
        self.lblMob.text = contactModel.contact_no
        self.lblDesc.text = contactModel.description
    }

}

extension NContactVC {
 
    func getContactDetails(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID
        ]
        
        NNewViewModel.getContactDetails(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.contactModel = ContactModel()
                        self.contactModel = data
                        self.setUpUi()
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
}
