//
//  NManageAddressVC.swift
//  Needs
//
//  Created by webcastle on 15/02/22.
//

import UIKit
import Alamofire

class NManageAddressVC: ParentController {

    @IBOutlet weak var tblView: UITableView!
    var selectedIndex = 0
    var addressModel = [AddressM]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getAddressList()
    }
    

    // MARK: - Button action
    
    @IBAction func tappedAddAddress(_ sender: UIButton) {
//        let vc = Storyboards.Cart.viewController(controller: NAddNewAddressVC.self)
//        vc.delegate = self
//        self.pushVC = vc
        let vc = Storyboards.Login.viewController(controller: NLocationVC.self)
        vc.isFromAddress = true
        self.pushVC = vc
    }

}

extension NManageAddressVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.addressModel.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(NChooseAddressTVC.self, for: indexPath)
        cell.configureCell(model: self.addressModel[indexPath.row])
        if indexPath.row == selectedIndex {
            cell.imgRadio.image = UIImage(named: "selectedRadioBtn")
        }else {
            cell.imgRadio.image = UIImage(named: "radioBtn")
        }
        cell.delegate = self
        cell.iPath = indexPath
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        self.tblView.reloadData()
    }
}

extension NManageAddressVC {
    
    func getAddressList(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "session_id": SessionHandler.bearer_token ?? ""
        ]
        
        NCartViewModel.getAddressList(parameters: parameter) { result in
               switch result {
               case .success(let response):
                   if response.errorcode == 0 {
                       if let data = response.data {
                        self.addressModel = [AddressM]()
                        self.addressModel = data
                        self.tblView.reloadData()
                       }
                   } else {
                       Utility.displayBanner(message: response.message ?? "", style: .danger)
                   }
               case .failure(let error):
                   DebugLogger.error(error.localizedDescription)
               }
           }
       
   }
    
    func deleteAddress(index: Int){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "session_id": SessionHandler.bearer_token ?? "",
            "add_id": self.addressModel[index].id ?? -1
        ]
        
        NCartViewModel.deleteAddress(parameters: parameter) { result in
               switch result {
               case .success(let response):
                   if response.errorcode == 0 {
                        self.getAddressList()
                   } else {
                       Utility.displayBanner(message: response.message ?? "", style: .danger)
                   }
               case .failure(let error):
                   DebugLogger.error(error.localizedDescription)
               }
           }
       
   }
}

extension NManageAddressVC: NotifyDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
            if let path = dic["index"] as? IndexPath {
                if let type = dic["type"] as? String {
                    switch type {
                    case AppData.edit:
                        let vc = Storyboards.Cart.viewController(controller: NAddNewAddressVC.self)
                        vc.delegate = self
                        vc.isFromEdit = true
                        vc.addressModel = self.addressModel[path.row]
                        vc.long = Double(self.addressModel[path.row].long ?? "0") ?? 0.0
                        vc.lat = Double(self.addressModel[path.row].lat ?? "0") ?? 0.0
                        self.pushVC = vc
                        break
                    case AppData.delete:
                        self.deleteAddress(index: path.row)
                        break

                    default:
                        break
                    }
                    
                }
            }else if let type = dic["type"] as? String {
                switch type {
                case AppData.save:
                    self.getAddressList()
                    break
                default:
                    break
                }
            }
        }
    }
    
    
}
