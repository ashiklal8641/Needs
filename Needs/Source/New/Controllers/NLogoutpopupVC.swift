//
//  NLogoutpopupVC.swift
//  Needs
//
//  Created by Admin on 18/02/22.
//

import UIKit


class NLogoutpopupVC: UIViewController {

    @IBOutlet weak var viewLogout: UIView!
    @IBOutlet weak var viewRetry: UIView!
    @IBOutlet weak var viewDelete: UIView!
    @IBOutlet weak var lblDltTitle: UILabel!
    @IBOutlet weak var lblDltDesc: UILabel!
    @IBOutlet weak var lblYes: UILabel!
    @IBOutlet weak var lblNo: UILabel!
    
    var delegate: NotifyDelegate?
    var iPath: IndexPath?
    var isLogout = false
    var isDelete = false
    var isFromFav = false
    var isFromCart = false
    var page: PopUpType = .logout
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        self.viewLogout.isHidden = !isLogout
//        self.viewRetry.isHidden = isLogout
        
        self.setUpUI()
        
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.1) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        }
    }
    
    func setUpUI() {
        
        switch page {
        
        case .logout:
            self.viewRetry.isHidden = true
            self.viewDelete.isHidden = true
            self.viewLogout.isHidden = false
            break
            
        case .cart:
            self.viewRetry.isHidden = true
            self.viewDelete.isHidden = false
            self.viewLogout.isHidden = true
            self.lblDltTitle.text = "Remove item"
            self.lblDltDesc.text = "Are you sure you want to remove this item?"
            break
            
        case .fav:
            self.viewRetry.isHidden = true
            self.viewDelete.isHidden = false
            self.viewLogout.isHidden = true
            self.lblDltTitle.text = "Remove item"
            self.lblDltDesc.text = "Are you sure you want to remove this item?"
            break
            
        case .login:
            self.viewRetry.isHidden = true
            self.viewDelete.isHidden = false
            self.viewLogout.isHidden = true
            self.lblDltTitle.text = "Login Now"
            self.lblDltDesc.text = "You must login first."
            break
            
        case .delete:
            self.viewRetry.isHidden = true
            self.viewDelete.isHidden = false
            self.viewLogout.isHidden = true
            self.lblDltTitle.text = "Remove item"
            self.lblDltDesc.text = "This item has multiple customizations added. Proceed to cart to remove item?"
            break
            
        case .location:
            self.viewRetry.isHidden = false
            self.viewDelete.isHidden = true
            self.viewLogout.isHidden = true
            
        case .outOfStock:
            self.viewRetry.isHidden = true
            self.viewDelete.isHidden = false
            self.viewLogout.isHidden = true
            self.lblNo.isHidden = true
            self.lblYes.text = "OK"
            self.lblDltTitle.text = "Remove item"
            self.lblDltDesc.text = "Your product is out of stock.Please remove to proceed"
            
            break
            
        case .cartOutOfStock:
            self.viewRetry.isHidden = true
            self.viewDelete.isHidden = false
            self.viewLogout.isHidden = true
            self.lblNo.isHidden = true
            self.lblYes.text = "OK"
            self.lblDltTitle.text = "Remove item"
            self.lblDltDesc.text = "Your cart contain out of stock product.Remove product to Proceed"
            
            break
            
        }
        
//        if isLogout && !isDelete {
//            self.viewRetry.isHidden = true
//            self.viewDelete.isHidden = true
//            self.viewLogout.isHidden = false
//        }else if !isLogout && isDelete {
//            self.viewRetry.isHidden = true
//            self.viewDelete.isHidden = false
//            self.viewLogout.isHidden = true
//        }else {
//            self.viewRetry.isHidden = false
//            self.viewDelete.isHidden = true
//            self.viewLogout.isHidden = true
//        }
//
//        if isFromFav {
//            self.lblDltTitle.text = "Remove item"
//            self.lblDltDesc.text = "Are you sure you want to remove this item?"
//        }else {
//            if isFromCart {
//                self.lblDltTitle.text = "Remove item"
//                self.lblDltDesc.text = "Are you sure you want to remove this item?"
//            }else {
//                self.lblDltTitle.text = "Remove item"
//                self.lblDltDesc.text = "This item has multiple customizations added. Proceed to cart to remove item?"
//            }
//        }
        
    }
    
    // MARK: - Button action
    
    @IBAction func tappedContinue(_ sender: UIButton) {
        self.dismiss(animated: false) {
            self.delegate?.didSelect(withInfo: ["type": AppData.cntnue])
        }
    }
    
    @IBAction func tappedClose(_ sender: UIButton) {
        self.dismiss(animated: false) {
           
        }
    }
     
    @IBAction func tappedYes(_ sender: UIButton) {
        self.dismiss(animated: false) {
            if self.page == .login {
                self.delegate?.didSelect(withInfo: ["type": AppData.login])
            }else if self.page == .outOfStock || self.page == .cartOutOfStock {
                self.delegate?.didSelect(withInfo: ["type": AppData.outOfStock])
            }else {
                self.delegate?.didSelect(withInfo: ["type": AppData.yes, "index": self.iPath ?? IndexPath(row: 0, section: 0)])
            }
        }
    }
    
    @IBAction func tappedRetry(_ sender: UIButton) {
        self.dismiss(animated: false) {
            self.delegate?.didSelect(withInfo: ["type": AppData.save])
        }
    }
}
