//
//  NFAQVC.swift
//  Needs
//
//  Created by Admin on 17/02/22.
//

import UIKit
import Alamofire

class NFAQVC: UIViewController {
    
    @IBOutlet weak var faqtv: UITableView!
    
    var arrFaq = [FaqModel]()
    var selectedindex:Int?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.getFaqDetails()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NFAQVC:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFaq.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NFAQTVC") as! NFAQTVC
        cell.Questionlbl.text = self.arrFaq[indexPath.row].question?.htmlToString
        if self.arrFaq[indexPath.row].isSelected ?? false {
            cell.AnswerLbl.text = self.arrFaq[indexPath.row].answer?.htmlToString
            cell.expansionimg.image = UIImage(named: "noun-plus-5")
        }
        else {
            cell.AnswerLbl.text = ""
            cell.expansionimg.image = UIImage(named: "noun-plus-6")
        }
        return  cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedindex = indexPath.row
        self.arrFaq[indexPath.row].isSelected = !(self.arrFaq[indexPath.row].isSelected ?? false)
        faqtv.reloadData()
    }
    
    
}

extension NFAQVC {
    
    private func getFaqDetails(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID
        ]
        
        NNewViewModel.getFaqList(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.arrFaq = [FaqModel]()
                        self.arrFaq = data.faq ?? [FaqModel]()
                        for i in self.arrFaq {
                            i.isSelected = false
                        }
                        self.faqtv.reloadData()
                    }
                } else {
                   
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}
