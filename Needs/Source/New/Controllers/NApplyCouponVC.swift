//
//  NApplyCouponVC.swift
//  Needs
//
//  Created by Admin on 22/02/22.
//

import UIKit
import Alamofire

class NApplyCouponVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    
    var amount = ""
    var couponList = CouponListModel()
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getCouponList()
    }
    

    @IBAction func tappedApply(_ sender: UIButton) {
        if txtSearch.text == "" {
            Utility.displayBanner(message: "Enter coupon code", style: .danger)
            return
        }
        self.applyCoupon(code: txtSearch.text ?? "")
    }

}

extension NApplyCouponVC:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return (couponList.available_coupons?.count  ?? 0 ) > 0 ? (couponList.available_coupons?.count  ?? 0) + 1 : 0
        }else {
            return (couponList.unavailable_coupons?.count  ?? 0 ) > 0 ? (couponList.unavailable_coupons?.count  ?? 0) + 1 : 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sec = indexPath.section
        let row = indexPath.row
        if row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NHeaderTVC") as! NHeaderTVC
            if sec == 0 {
                cell.lblTitle.text = "Available Coupons"
            }else {
                cell.lblTitle.text = "Unavailable Coupons"
            }
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CouponTVC") as! CouponTVC
            if sec == 0 {
                cell.configureCell(model: couponList.available_coupons?[row - 1] ?? CouponModel())
            }else {
                cell.configureCell(model: couponList.unavailable_coupons?[row - 1] ?? CouponModel())
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sec = indexPath.section
        let row = indexPath.row
        if sec == 0{
            self.applyCoupon(code: self.couponList.available_coupons?[row - 1].code ?? "")
        }
    }
    
}

extension NApplyCouponVC {
    
    func getCouponList(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID
        ]
        
        NNewViewModel.getCouponList(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.couponList = CouponListModel()
                        self.couponList = data
                        self.tblView.reloadData()
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func applyCoupon(code: String){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "coupon_id": code,
            "amount": amount
        ]
        
        NNewViewModel.getApplyCoupon(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.couponList = CouponListModel()
                        self.couponList = data
                        self.tblView.reloadData()
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}

class CouponTVC:UITableViewCell {
  
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
   
    func configureCell(model: CouponModel) {
//        self.imgIcon.kf.setImage(with: URL(string:model.image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        self.lblTitle.text = model.title
        self.lblDesc.text = model.sub_title
        self.lblCode.text = model.code
        self.lblStatus.text = "Apply"
    }
}



