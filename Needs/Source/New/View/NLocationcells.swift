//
//  NLocationcells.swift
//  Needs
//
//  Created by Admin on 17/02/22.
//

import UIKit

class locationheaderTVC :UITableViewCell{
    
    @IBOutlet weak var lblTitle: UILabel!
    
}

class locationselectedTVC : UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
}

class locationlistTVC :UITableViewCell {
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var imgRadio: UIImageView!

    func configureCell(model: AddressM){
        self.lblName.text = model.name
        let mob = LocalPersistant.string(for: .phone_number)
        self.lblAddress.text = "\(model.home ?? ""),\(model.address ?? ""),\(model.landmark ?? ""),\(model.pincode ?? "") \nMobile: +91 \(model.mobile ?? "")"
        if AddressType(rawValue: model.type ?? 0) == .home {
            self.lblType.text = "Home"
        }else if AddressType(rawValue: model.type ?? 0) == .office {
            self.lblType.text = "Office"
        }else {
            self.lblType.text = "Other"
        }
    }
}


class RecentlistTVC :UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    
}
