//
//  NNewViewModel.swift
//  Needs
//
//  Created by webcastle on 24/03/22.
//

import UIKit
import Alamofire

class NNewViewModel: NSObject {

    static func getFaqList(parameters:Parameters,completion:@escaping Completion<FaqListModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getFaq)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getCouponList(parameters:Parameters,completion:@escaping Completion<CouponListModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getCoupons)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getApplyCoupon(parameters:Parameters,completion:@escaping Completion<CouponListModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.applyCoupon)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getContactDetails(parameters:Parameters,completion:@escaping Completion<ContactModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.contact)),
                                 loader: true,
                                 completion: completion)
    }
}
