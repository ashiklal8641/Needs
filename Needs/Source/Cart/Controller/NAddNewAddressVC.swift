//
//  NAddNewAddressVC.swift
//  Needs
//
//  Created by webcastle on 14/02/22.
//

import UIKit
import Alamofire
import GoogleMaps
import GooglePlaces

class NAddNewAddressVC: ParentController {

    @IBOutlet weak var txtHouse: UITextField!
    @IBOutlet weak var txtLandmark: UITextField!
    @IBOutlet weak var txtAddress: UITextView!
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgOffice: UIImageView!
    @IBOutlet weak var imgOther: UIImageView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblLoc: UILabel!
    
    weak var delegate: NotifyDelegate!
    var lat = 0.0
    var long = 0.0
    var locationManager = CLLocationManager()
    var loc = ""
    var city = ""
    var place : GMSPlace?
    var type = 0
    var isFromEdit = false
    var addressModel = AddressM()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mapView.delegate = self
        locationManager.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isFromEdit {
            self.initializeView()
        }
        if long == 0.0, lat == 0.0 {            
            self.configureLocationServices()
        }else {
            locateWithLong(lon: self.long, lat: self.lat)
        }
    }
    // MARK: - Button action
    
    @IBAction func tappedHomeBtn(_ sender: UIButton) {
        self.imgHome.image = UIImage(named: "selectedRadioBtn")
        self.imgOffice.image = UIImage(named: "radioBtn")
        self.imgOther.image = UIImage(named: "radioBtn")
        self.type = 0
    }
    
    @IBAction func tappedOfficeBtn(_ sender: UIButton) {
        self.imgHome.image = UIImage(named: "radioBtn")
        self.imgOffice.image = UIImage(named: "selectedRadioBtn")
        self.imgOther.image = UIImage(named: "radioBtn")
        self.type = 1
    }
    
    @IBAction func tappedOtherBtn(_ sender: UIButton) {
        self.imgHome.image = UIImage(named: "radioBtn")
        self.imgOffice.image = UIImage(named: "radioBtn")
        self.imgOther.image = UIImage(named: "selectedRadioBtn")
        self.type = 2
    }
    
    @IBAction func tappedSaveBtn(_ sender: UIButton) {
        if isFromEdit {
            self.editAddress()
        }else {
            self.addNewAddress()
        }
//        if !isFromEdit {
//            self.navigationController?.popToViewController(ofClass: NCustomTabbar.self)
//        }else {
//            self.navigateBackward()
//        }
        
    }
    
    @IBAction func tappedLocation(_ sender: UIButton) {
        let vc = Storyboards.Login.viewController(controller: NLocationVC.self)
        vc.lat = self.lat
        vc.long = self.long
        vc.loc = self.loc
        vc.isFromAddress = true
        vc.isFromMap = true
        vc.addressModel = self.addressModel
        vc.delegate = self
        self.pushVC = vc
    }
    
    private func configureLocationServices(){
        
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            locationManager.distanceFilter = 50
        }
    }

    private func initializeView(){
        self.long = Double(addressModel.long ?? "0") ?? 0.0
        self.lat = Double(addressModel.lat ?? "0") ?? 0.0
        self.txtHouse.text = addressModel.home
        self.txtLandmark.text = addressModel.landmark
        self.txtAddress.text = addressModel.address
        if AddressType(rawValue: addressModel.type ?? 0) == .home {
            self.imgHome.image = UIImage(named: "selectedRadioBtn")
            self.imgOffice.image = UIImage(named: "radioBtn")
            self.imgOther.image = UIImage(named: "radioBtn")
        }else if AddressType(rawValue: addressModel.type ?? 0) == .office {
            self.imgHome.image = UIImage(named: "radioBtn")
            self.imgOffice.image = UIImage(named: "selectedRadioBtn")
            self.imgOther.image = UIImage(named: "radioBtn")
        }else {
            self.imgHome.image = UIImage(named: "radioBtn")
            self.imgOffice.image = UIImage(named: "radioBtn")
            self.imgOther.image = UIImage(named: "selectedRadioBtn")
        }
        self.type = addressModel.type ?? 0
    }

    func locateWithLong(lon: Double, lat: Double) {
        DispatchQueue.main.async {
            let latDouble = lat
            let lonDouble = lon
            self.mapView.clear()
            let camera = GMSCameraPosition.camera(withLatitude: latDouble ?? 20.0, longitude: lonDouble ?? 10.0, zoom: 15)
            self.mapView.camera = camera
            self.locationManager.stopUpdatingHeading()
        }
    }
    
}

extension NAddNewAddressVC: GMSMapViewDelegate,CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")

        locateWithLong(lon: (locationManager.location?.coordinate.longitude ?? 0)!, lat: (locationManager.location?.coordinate.latitude ?? 0)!)
        
        print("current location didupdate :\(locationManager.location?.coordinate.latitude ?? 0) \(locationManager.location?.coordinate.longitude ?? 0)")
        self.lat = locationManager.location?.coordinate.latitude ?? 0
        self.long = locationManager.location?.coordinate.longitude ?? 0
        let userLocation :CLLocation = locations[0] as CLLocation
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverse geocode")
            }
            let placemark = (placemarks ?? [CLPlacemark]()) as [CLPlacemark]
            if placemark.count > 0 {
                let placemark = placemarks![0]
                print(placemark.locality!)
                print(placemark.administrativeArea!)
                print(placemark.country!)
       
                var addressString : String = ""
                                    if placemark.subLocality != nil {
                                        addressString = addressString + placemark.subLocality! + ", "
                                    }
                                    if placemark.locality != nil {
                                        addressString = addressString + placemark.locality! + ", "
                                    }
                                    if placemark.administrativeArea != nil {
                                        addressString = addressString + placemark.administrativeArea! + " "
                                    }
                                    if placemark.postalCode != nil {
                                        addressString = addressString + placemark.postalCode! + ", "
                                    }
                                   if placemark.country != nil {
                                           addressString = addressString + placemark.country! + " "
                                   }
                
                var locString : String = ""
                                        if placemark.subLocality != nil {
                                            locString = locString + placemark.subLocality!
                                        } else if placemark.locality != nil {
                                            locString = locString + placemark.locality!
                                        }else if placemark.administrativeArea != nil {
                                            locString = locString + placemark.administrativeArea!
                                        }else if placemark.country != nil {
                                            locString = locString + placemark.country!
                                        }
                self.loc = addressString
                self.lblAddress.text = self.loc
                self.lblLoc.text = locString
    
            }
        }
        locationManager.stopUpdatingLocation()

    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error: \(error)")
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        let geocoder = GMSGeocoder()
        
     geocoder.reverseGeocodeCoordinate(coordinate) { [self] response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
         
            let country = address.country ?? ""
            let postalCode = address.postalCode ?? ""
            let administrativeArea =  address.administrativeArea ?? ""
            let locality = address.locality ?? ""
            let subLocality = address.subLocality ?? ""
            let thoroughfare = address.thoroughfare ?? ""
        
         var addressString : String = ""
                             if address.subLocality != nil {
                                 addressString = addressString + address.subLocality! + ", "
                             }
                             if address.locality != nil {
                                 addressString = addressString + address.locality! + ", "
                             }
                             if address.administrativeArea != nil {
                                 addressString = addressString + address.administrativeArea! + " "
                             }
                             if address.postalCode != nil {
                                 addressString = addressString + address.postalCode! + ", "
                             }
                            if address.country != nil {
                                    addressString = addressString + address.country! + " "
                            }

        var locString : String = ""
                                if address.subLocality != nil {
                                    locString = locString + address.subLocality!
                                } else if address.locality != nil {
                                    locString = locString + address.locality!
                                }else if address.administrativeArea != nil {
                                    locString = locString + address.administrativeArea!
                                }else if address.country != nil {
                                    locString = locString + address.country!
                                }

         print("cordinate",address)
         print("cordinate",coordinate)
         print("country",country)
         print("postalCode",postalCode)
         print("administrativeArea",administrativeArea)
         print("locality",locality)
         print("subLocality",subLocality)
         print("thoroughfare",thoroughfare)
         print("address",addressString)

            
        self.lat = address.coordinate.latitude
        self.long = address.coordinate.longitude
        self.loc = addressString
        self.lblAddress.text = self.loc
        self.lblLoc.text = locString

   
        }
    }

}

extension NAddNewAddressVC: NotifyDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
            if let path = dic["index"] as? IndexPath {
                if let type = dic["type"] as? String {
                    switch type {
                    case AppData.category:
                        
                        break
                                                
                    default:
                        break
                    }
                    
                }
            }else if let type = dic["type"] as? String {
                switch type {
                case AppData.changeAddress:
                    self.lat = dic["lat"] as? Double ?? 0.0
                    self.long = dic["long"] as? Double ?? 0.0
                    locateWithLong(lon: self.long, lat: self.lat)
                    break
                    
                default:
                    break
                }
            }
        }
    }
    
}


extension NAddNewAddressVC {
    
    func addNewAddress(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "session_id": SessionHandler.bearer_token ?? "",
            "landmark": txtLandmark.text ?? "",
            "address": txtAddress.text ?? "",
            "home": txtHouse.text ?? "",
            "type": self.type,
            "lat": lat,
            "long": long
        ]
        
        NCartViewModel.addNewAddress(parameters: parameter) { result in
               switch result {
               case .success(let response):
                   if response.errorcode == 0 {
                        self.delegate.didSelect(withInfo: ["type": AppData.save])
//                        self.navigateBackward()
                    self.navigationController?.popToViewController(ofClass: NCustomTabbar.self)
                   } else {
                       Utility.displayBanner(message: response.message ?? "", style: .danger)
                   }
               case .failure(let error):
                   DebugLogger.error(error.localizedDescription)
               }
           }
       
   }
  
    func editAddress(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "session_id": SessionHandler.bearer_token ?? "",
            "landmark": txtLandmark.text ?? "",
            "address": txtAddress.text ?? "",
            "home": txtHouse.text ?? "",
            "type": self.type,
            "lat": lat,
            "long": long,
            "add_id": self.addressModel.id ?? -1
        ]
        
        NCartViewModel.editAddress(parameters: parameter) { result in
               switch result {
               case .success(let response):
                   if response.errorcode == 0 {
                        self.delegate.didSelect(withInfo: ["type": AppData.save])
                        Utility.displayBanner(message: "Address updated successfully", style: .success)
                        self.navigateBackward()
                   } else {
                       Utility.displayBanner(message: "Address updation failed", style: .danger)
                   }
               case .failure(let error):
                   DebugLogger.error(error.localizedDescription)
               }
           }
       
   }
    
}
