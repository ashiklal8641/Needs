//
//  NNotificationVC.swift
//  Needs
//
//  Created by webcastle on 14/02/22.
//

import UIKit
import Alamofire

class NNotificationVC: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    
    var arrNotifListModel = [NotifListM]()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.getNotifications()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension NNotificationVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.arrNotifListModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrNotifListModel[section].content?.count ?? 0 > 0 ? (self.arrNotifListModel[section].content?.count ?? 0) + 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        if row == 0 {
            let cell = tableView.dequeueReusableCell(NNotificationHeaderTVC.self, for: indexPath)
            cell.lblTitle.text = self.arrNotifListModel[indexPath.section].date
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(NNotificationTVC.self, for: indexPath)
            cell.configureCell(model: arrNotifListModel[indexPath.section].content?[row - 1] ?? NotifM())
            return cell
        }
    }
    
    
}

extension NNotificationVC {
    
    func getNotifications(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID
        ]
        
        NHomeViewModel.getNotificationList(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.arrNotifListModel = [NotifListM]()
                        self.arrNotifListModel = data
                        self.tblView.reloadData()
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}
