//
//  NOrderPlacedVC.swift
//  Needs
//
//  Created by webcastle on 15/02/22.
//

import UIKit

class NOrderPlacedVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    var isFromOrder = true
    var orderModel = PlaceOrderM()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if isFromOrder {
            self.lblTitle.text = "Your order has been placed successfully"
//            self.lblName.text = ""
            self.lblSubTitle.text = ""
            self.lblDesc.text = "Your return of product has been \nplaced successfully, \nour executive will contact \nback soon"
        }else {
            self.lblTitle.text = self.orderModel.title//"Order Return Placed Successfully"
//            self.lblName.text = ""
            self.lblSubTitle.text = ""
            self.lblDesc.text = self.orderModel.description//"Your return of product has been \nplaced successfully, \nour executive will contact \nback soon"
        }
    }
    

    // MARK: - Button action
    
    @IBAction func tappedContinue(_ sender: UIButton) {
        self.navigationController?.popToViewController(ofClass: NCustomTabbar.self)
    }
    

}
