//
//  NPaymentMethodVC.swift
//  Needs
//
//  Created by webcastle on 22/02/22.
//

import UIKit

class NPaymentMethodVC: UIViewController {

    @IBOutlet weak var imgOnline: UIImageView!
    @IBOutlet weak var imgCod: UIImageView!
    
    var delegate: NotifyDelegate?
    var isCod = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.1) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        }
    }
    
    // MARK: - Button action
    
    @IBAction func tappedContinue(_ sender: UIButton) {
        self.dismiss(animated: false) {
            self.delegate?.didSelect(withInfo: ["type": AppData.cntnue, "method": self.isCod ])
        }
    }
    
    @IBAction func tappedClose(_ sender: UIButton) {
        self.dismiss(animated: false) {
            
        }
    }

    @IBAction func tappedCOD(_ sender: UIButton) {
        isCod = true
        self.imgCod.image = isCod ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        self.imgOnline.image = !isCod ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
    }
    
    @IBAction func tappedOnline(_ sender: UIButton) {
        isCod = false
        self.imgCod.image = isCod ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        self.imgOnline.image = !isCod ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
    }

}

//if indexPath.row == selectedIndex {
//    cell.imgRadio.image = UIImage(named: "selectedRadioBtn")
//}else {
//    cell.imgRadio.image = UIImage(named: "radioBtn")
//}
