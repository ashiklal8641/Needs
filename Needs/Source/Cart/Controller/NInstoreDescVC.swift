//
//  NInstoreDescVC.swift
//  Needs
//
//  Created by webcastle on 15/02/22.
//

import UIKit
import Alamofire

class NInstoreDescVC: UIViewController {

    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgStore: UIImageView!
    @IBOutlet weak var txtDesc: UITextView!
    
    var delegate: NotifyDelegate?
    var storeModel = StoreM()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setUpUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.1) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        }
    }
    
    func setUpUI() {
        self.imgStore.kf.setImage(with: URL(string:self.storeModel.image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        self.lblStoreName.text = self.storeModel.title
        self.lblLocation.text = self.storeModel.location
    }

    // MARK: - Button action
    
    @IBAction func tappedContinue(_ sender: UIButton) {
        let desc = txtDesc.text.trimmingCharacters(in: .whitespaces)
        if desc == "" {
            Utility.displayBanner(message: "Enter description", style: .danger)
            return
        }
        self.redeemInstoreOffer()
    }
    
    @IBAction func tappedClose(_ sender: UIButton) {
        self.dismiss(animated: false) {
            
        }
    }


}

extension NInstoreDescVC {
    
    func redeemInstoreOffer(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "description": txtDesc.text ?? "",
            "store_id": storeModel.id ?? -1
        ]
        
        NCartViewModel.redeemInstoreOffer(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.dismiss(animated: false) {
                            self.delegate?.didSelect(withInfo: ["type": AppData.save])
                        }                       
                    }
                } else {
                   
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}
