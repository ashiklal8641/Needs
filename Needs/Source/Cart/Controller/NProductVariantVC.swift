//
//  NProductVariantVC.swift
//  Needs
//
//  Created by webcastle on 17/02/22.
//

import UIKit
import Alamofire

class NProductVariantVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewRepeat: UIView!
    @IBOutlet weak var viewVariant: UIView!
    @IBOutlet weak var lblPdtName: UILabel!
    @IBOutlet weak var imgPdt: UIImageView!
    @IBOutlet weak var lblVariantName: UILabel!
    @IBOutlet weak var lblPdtPrice: UILabel!
    @IBOutlet weak var lblRptPdtName: UILabel!
    @IBOutlet weak var imgRptPdt: UIImageView!
    @IBOutlet weak var lblRptVariantName: UILabel!
    @IBOutlet weak var lblRptPdtPrice: UILabel!
    
    var isFromCart = false
    var OptionModel = [Option]()
    var pdtModel = ProductM()
    var variantPdtModel = ProductM()
    var variantProductId = -1
    var varName = ""
    var delegate: NotifyDelegate?
    var iPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.viewVariant.isHidden = isFromCart
        self.SetVariant(_index: 0)
        self.setUpUI(index: 0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.1) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        }
    }
    

    @IBAction func tappedClose(_ sender: UIButton){
//        self.delegate?.didSelect(withInfo: ["type": AppData.returnItem, "index": iPath ?? IndexPath(row: 0, section: 0)])
        self.dismiss(animated: false) {
            
        }
    }

    @IBAction func tappedAddTocart(_ sender: UIButton){
        self.addToCart()
        self.dismiss(animated: false) {
            self.delegate?.didSelect(withInfo: ["type": AppData.addToCart, "index": self.iPath ?? IndexPath(row: 0, section: 0)])
        }
    }
    
    @IBAction func tappedChoose(_ sender: UIButton){
        self.viewVariant.isHidden = false
    }
    
    @IBAction func tappedRepeat(_ sender: UIButton){
        self.viewVariant.isHidden = true
        
//        self.dismiss(animated: false) {
            self.updateCart(isFirst: false, isAdd: true, isFromCart: false)
//        }
    }
    
    func didTapped(id: Int, index: IndexPath) {
        variantProductId = id
        for i in 0 ..<  (self.pdtModel.variants?.count ?? 0) {
            if pdtModel.variants?[i].id == id {
                self.varName = self.OptionModel[index.section].optionItems[(index.row) - 1].themeValue ?? ""
                SetVariant(_index:i)
                
            }
        }
    }
    
    func  SetVariant(_index:Int)
       {
           for i in 0 ..<  (self.OptionModel.count )
           {
               for k in 0 ..<  (self.OptionModel[i].optionItems.count )
               {
                let id = self.OptionModel[i].optionItems[k].productId?.contains(variantProductId)
                print("id", id)
                if id ?? false
//                if self.OptionModel[i].optionItems[k].productId?.first == variantProductId
                   {
                       OptionModel[i].optionItems[k].themecolor = "2"
                   }
                   else
                   {
                       OptionModel[i].optionItems[k].themecolor = "1"
                   }
               }
           }
        self.tblView.reloadData()
        self.setUpUI(index: _index)
       }
    
    func setUpUI(index: Int) {
        
        self.imgPdt.kf.setImage(with: URL(string:self.pdtModel.variants?[index].image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        self.lblPdtName.text = self.pdtModel.variants?[index].name
        self.lblVariantName.text = self.varName
        self.lblPdtPrice.text = "₹ \(self.pdtModel.variants?[index].selling_price ?? "0")"
        self.imgRptPdt.kf.setImage(with: URL(string:self.variantPdtModel.image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        self.lblRptPdtName.text = self.variantPdtModel.name
        self.lblRptVariantName.text = self.variantPdtModel.variant_name
        self.lblRptPdtPrice.text = "₹ \(self.variantPdtModel.price ?? "0")"
        
    }
}

extension NProductVariantVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.OptionModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.OptionModel[section].optionItems.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        if row == 0{
            let cell = tableView.dequeueReusableCell(NHeaderTVC.self, for: indexPath)
            cell.lblTitle.text =  self.OptionModel[section].optionTitle
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(NProductVariantTVC.self, for: indexPath)
            cell.lblTitle.text =  self.OptionModel[section].optionItems[row - 1].themeValue
            if self.OptionModel[section].optionItems[row - 1].themecolor == "1" {
                cell.imgRadio.image = UIImage(named: "radioBtn")
            }else {
                cell.imgRadio.image = UIImage(named: "selectedRadioBtn")
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let row = indexPath.row
//        if self.OptionModel[section].optionItems[row - 1].themecolor == "1" {
//            self.OptionModel[section].optionItems[row - 1].themecolor = "2"
//        }else {
//            self.OptionModel[section].optionItems[row - 1].themecolor = "1"
//        }
        
        if row != 0 {
            if self.OptionModel[section].optionItems[row - 1].productId?.count ?? 0 > 0 {
                if  self.OptionModel[section].optionItems[row - 1].productId!.contains(variantProductId) {
                    self.didTapped(id: variantProductId, index: indexPath)
                }else {
                    self.didTapped(id: self.OptionModel[section].optionItems[row - 1].productId?.first ?? 0, index: indexPath)
                }
                
            }
        }
        self.tblView.reloadData()
    }
    
    
}

extension NProductVariantVC {
    
    func addToCart(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "product_id": variantProductId
        ]
        
        NHomeViewModel.addToCart(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    self.pdtModel.is_cart = true
                    self.viewVariant.isHidden = true
                    self.delegate?.didSelect(withInfo: ["type": AppData.addCart])
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func updateCart(isFirst: Bool, isAdd: Bool, isFromCart: Bool) {
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "product_id": variantProductId,
            "firstTime": isFirst,
            "fromCart": isFromCart,
            "isAdd": isAdd ? 1 : 0
        ]
        
        NHomeViewModel.updateCart(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if isAdd {
                        
                    }else {
                        
                    }
                    self.dismiss(animated: false) {
                        self.delegate?.didSelect(withInfo: ["type": AppData.addCart])
                    }
                } else {
//                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
}
