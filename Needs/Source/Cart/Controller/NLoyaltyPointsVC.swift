//
//  NLoyaltyPointsVC.swift
//  Needs
//
//  Created by webcastle on 15/02/22.
//

import UIKit
import Alamofire

class NLoyaltyPointsVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblPoints: UILabel!
    
    var loyaltyModel = LoyaltyListM()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getLoyaltyHistory()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NLoyaltyPointsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.loyaltyModel.loyalty?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(NLoyaltyHistoryTVC.self, for: indexPath)
        cell.configureCell(self.loyaltyModel.loyalty?[indexPath.row] ?? LoyaltyM())
        return cell
    }
    
    
}

extension NLoyaltyPointsVC {
    
    func getLoyaltyHistory(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID
        ]
        
        NCartViewModel.getLoyaltyHistory(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.loyaltyModel = LoyaltyListM()
                        self.loyaltyModel = data
                        self.lblPoints.text = self.loyaltyModel.max_redeemable_points?.cleanValue ?? "0"
                        self.tblView.reloadData()
                    }
                } else {
                   
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}
