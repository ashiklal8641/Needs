//
//  NEditProfileVC.swift
//  Needs
//
//  Created by webcastle on 15/02/22.
//

import UIKit
import Alamofire

class NEditProfileVC: UIViewController {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    weak var delegate: NotifyDelegate!
    var profileModel = ProfileM()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.lblName.text = "Hi \(profileModel.name ?? "User")!"
        self.txtName.text = profileModel.name
        self.txtMobile.text = "+91 \(profileModel.mobile ?? "0000000000")"
        self.txtEmail.text = profileModel.email
        
//        self.lblName.text = "Hi George!"
//        self.txtName.text = "George"
//        self.txtMobile.text = "+91 0000000000"
//        self.txtEmail.text = "George123@gmail.com"
      
    }
    

    @IBAction func tappedUpdate(_ sender: UIButton) {
        if isValid() {
            self.editProfileDetails()
        }
        //        self.navigateBackward()
    }

    private func isValid() -> Bool {
        if !BaseValidator.isNotEmpty(string: self.txtName.text) {
            Utility.displayBanner(message: "The name field is required", style: .danger)
            return false
        }else if !BaseValidator.isNotEmpty(string: self.txtEmail.text) {
            Utility.displayBanner(message: "The email id field is required", style: .danger)
            return false
        }else if !BaseValidator.isValid(email: self.txtEmail.text) {
            Utility.displayBanner(message: "Enter valid email id", style: .danger)
            return false
        }
        return true
    }
}

extension NEditProfileVC {
    
    private func editProfileDetails(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "name": txtName.text ?? "",
            "email": txtEmail.text ?? ""
        ]
        
        NMainViewModel.editProfileDetails(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.lblName.text = "Hi \(data.name ?? "User")!"
                        self.delegate.didSelect(withInfo: ["type": AppData.edit])
                        self.navigateBackward()
                    }
                } else {
                   
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}
