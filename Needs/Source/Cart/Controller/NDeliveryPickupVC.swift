//
//  NDeliveryPickupVC.swift
//  Needs
//
//  Created by webcastle on 14/02/22.
//

import UIKit

class NDeliveryPickupVC: ParentController {

    @IBOutlet weak var imgInstore: UIImageView!
    @IBOutlet weak var imgPickup: UIImageView!
    
    var isInstore = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    // MARK: - Button action
    
    @IBAction func tappedSkip(_ sender: UIButton) {
//        let vc = Storyboards.Cart.viewController(controller: NOrderPlacedVC.self)
//        self.pushVC = vc
        let vc = Storyboards.Main.viewController(controller: NCartVC.self)
        vc.isCheckOut = true
        self.pushVC = vc
    }
    
    @IBAction func tappedInstoreBtn(_ sender: UIButton) {
        self.isInstore = true
        self.imgInstore.image = self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        self.imgPickup.image = !self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        let vc = Storyboards.Cart.viewController(controller: NChoosePickUpTypeVC.self)
        self.pushVC = vc
    }
    
    @IBAction func tappedDeliveryPickupBtn(_ sender: UIButton) {
        self.isInstore = false
        self.imgInstore.image = self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        self.imgPickup.image = !self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        let vc = Storyboards.Cart.viewController(controller: NChoosePickUpTypeVC.self)
        vc.isInstore = false
        self.pushVC = vc
    }

}
