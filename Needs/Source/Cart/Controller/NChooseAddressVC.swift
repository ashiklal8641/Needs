//
//  NChangeAddressVC.swift
//  Needs
//
//  Created by webcastle on 14/02/22.
//

import UIKit
import Alamofire

class NChooseAddressVC: ParentController {

    @IBOutlet weak var tblView: UITableView!
    
    var selectedIndex = 0
    var addId = -1
    var addressModel = [AddressM]()
    
    weak var delegate: NotifyDelegate?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getAddressList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.1) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        }
    }
    
    // MARK: - Button action
    
    @IBAction func tappedContinue(_ sender: UIButton) {
        if self.addressModel.count > 0 {
            self.addId = self.addressModel[self.selectedIndex].id ?? -1
            self.delegate?.didSelect(withInfo: ["type": AppData.save, "address_id": self.addId])
        }
        self.dismiss(animated: false) {
            
        }
    }
    
    @IBAction func tappedClose(_ sender: UIButton) {
        self.dismiss(animated: false) {
            
        }
    }
    
    @IBAction func tappedAddAddress(_ sender: UIButton) {
        self.dismiss(animated: false) {
            self.delegate?.didSelect(withInfo: ["type": AppData.addAddress])
        }
    }

    @IBAction func tappedEdit(_ sender: UIButton) {
        
        
        
    }
    
}

extension NChooseAddressVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.addressModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(NChooseAddressTVC.self, for: indexPath)
        cell.configureCell(model: self.addressModel[indexPath.row])
        if indexPath.row == selectedIndex {
            cell.imgRadio.image = UIImage(named: "selectedRadioBtn")
        }else {
            cell.imgRadio.image = UIImage(named: "radioBtn")
        }
        cell.delegate = self
        cell.iPath = indexPath
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        self.tblView.reloadData()
    }
    
}

extension NChooseAddressVC {
    
    func getAddressList(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "session_id": SessionHandler.bearer_token ?? ""
        ]
        
        NCartViewModel.getAddressList(parameters: parameter) { result in
               switch result {
               case .success(let response):
                   if response.errorcode == 0 {
                       if let data = response.data {
                        self.addressModel = [AddressM]()
                        self.addressModel = data
                        self.tblView.reloadData()
                       }
                   } else {
                       Utility.displayBanner(message: response.message ?? "", style: .danger)
                   }
               case .failure(let error):
                   DebugLogger.error(error.localizedDescription)
               }
           }
       
   }
    
    func deleteAddress(index: Int){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "session_id": SessionHandler.bearer_token ?? "",
            "add_id": self.addressModel[index].id ?? -1
        ]
        
        NCartViewModel.deleteAddress(parameters: parameter) { result in
               switch result {
               case .success(let response):
                   if response.errorcode == 0 {
                        self.getAddressList()
                   } else {
                       Utility.displayBanner(message: response.message ?? "", style: .danger)
                   }
               case .failure(let error):
                   DebugLogger.error(error.localizedDescription)
               }
           }
       
   }
}

extension NChooseAddressVC: NotifyDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
            if let path = dic["index"] as? IndexPath {
                if let type = dic["type"] as? String {
                    switch type {
                                            
                    case AppData.edit:
                        self.dismiss(animated: false) {
                            self.delegate?.didSelect(withInfo: ["type": AppData.edit, "index": path, "model": self.addressModel[path.row]])
                        }
                        break
                        
                    default:
                        break
                    }
                    
                }
            }else if let type = dic["type"] as? String {
                switch type {
                case AppData.addAddress:
                    let vc = Storyboards.Cart.viewController(controller: NAddNewAddressVC.self)
                    vc.delegate = self
                    self.pushVC = vc
                    break

                default:
                    break
                }
            }
        }
    }
}
