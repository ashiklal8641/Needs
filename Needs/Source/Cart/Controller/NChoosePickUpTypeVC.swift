//
//  NPickUpTypeVC.swift
//  Needs
//
//  Created by webcastle on 14/02/22.
//

import UIKit
import Alamofire
import GoogleMaps
import GooglePlaces

enum OfferType: Int {
    case Instore = 0
    case delivery
}

class NChoosePickUpTypeVC: ParentController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewPickup: UIView!
    @IBOutlet weak var imgInstore: UIImageView!
    @IBOutlet weak var imgPickup: UIImageView!
    @IBOutlet weak var deliveryImgInstore: UIImageView!
    @IBOutlet weak var deliveryImgPickup: UIImageView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var txtDesc: UITextView!
    
    var isInstore = true
    var arrStore = [StoreM]()
    var radius = 0.0
    var lat = 0.0
    var long = 0.0
    var locationManager = CLLocationManager()
    var loc = ""
    var city = ""
    var place : GMSPlace?
    var type = 0
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.viewPickup.isHidden = isInstore
        self.imgInstore.image = self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        self.imgPickup.image = !self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        self.deliveryImgInstore.image = self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        self.deliveryImgPickup.image = !self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        
        self.mapView.delegate = self
        locationManager.delegate = self
        
        if long == 0.0, lat == 0.0 {
            self.configureLocationServices()
        }else {
            locateWithLong(lon: self.long, lat: self.lat)
        }
        self.getInstoreDetails()
        
    }
    

    // MARK: - Button action
    
    @IBAction func tappedContinue(_ sender: UIButton) {
        if isInstore {
//            self.getInstoreDetails()
        }else {
            if self.txtDesc.text == "" {
                Utility.displayBanner(message: "Enter your description", style: .danger)
                return
            }
            self.redeemDeliveryPickupOffer()
        }
        
    }
    
    @IBAction func tappedSkip(_ sender: UIButton) {
//        let vc = Storyboards.Cart.viewController(controller: NOrderPlacedVC.self)
//        self.pushVC = vc
        let vc = Storyboards.Main.viewController(controller: NCartVC.self)
        vc.isCheckOut = true
        self.pushVC = vc
    }
    
    @IBAction func tappedPickUpLocation(_ sender: UIButton) {

        let vc = Storyboards.Login.viewController(controller: NLocationVC.self)
        vc.delegate = self
        vc.showRadius = true
        vc.radius = self.radius
        vc.isFromAddress = true
        self.pushVC = vc
    }
    
    @IBAction func tappedDeliveryPickupBtn(_ sender: UIButton) {
        isInstore = false
        self.imgInstore.image = self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        self.imgPickup.image = !self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        self.deliveryImgInstore.image = self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        self.deliveryImgPickup.image = !self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        self.viewPickup.isHidden = isInstore
    }
    
    @IBAction func tappedInstoreBtn(_ sender: UIButton) {
        isInstore = true
        self.imgInstore.image = self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        self.imgPickup.image = !self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        self.deliveryImgInstore.image = self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        self.deliveryImgPickup.image = !self.isInstore ? UIImage(named: "selectedRadioBtn") : UIImage(named: "radioBtn")
        self.viewPickup.isHidden = isInstore
    }

    // MARK: - Custom function
    
    private func configureLocationServices(){
        
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            locationManager.distanceFilter = 50
        }
    }
    
    func locateWithLong(lon: Double, lat: Double) {
        DispatchQueue.main.async {
            let latDouble = lat
            let lonDouble = lon
            self.mapView.clear()
            let camera = GMSCameraPosition.camera(withLatitude: latDouble ?? 20.0, longitude: lonDouble ?? 10.0, zoom: 15)
            self.mapView.camera = camera
            self.locationManager.stopUpdatingHeading()
        }
    }
    
}

extension NChoosePickUpTypeVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrStore.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(NInstoreTVC.self, for: indexPath)
        cell.configureCell(model: self.arrStore[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Storyboards.Cart.viewController(controller: NInstoreDescVC.self)
        vc.storeModel = self.arrStore[indexPath.row]
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false, completion: nil)
    }
}

extension NChoosePickUpTypeVC: NotifyDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
            if let path = dic["index"] as? IndexPath {
                if let type = dic["type"] as? String {
                    switch type {
                    case AppData.topBanner:
                      
                        break
                   
                    default:
                        break
                    }
                    
                }
            }else if let type = dic["type"] as? String {
                switch type {
                case AppData.save:

//                    let vc = Storyboards.Main.viewController(controller: NCartVC.self)
//                    vc.isOfferApplied = true
//                    vc.isCheckOut = true
//                    self.pushVC = vc
                    
                    let vc = Storyboards.Main.viewController(controller: NCartVC.self)
                    vc.isOfferApplied = true
                    vc.isCheckOut = true
                    vc.type = OfferType.Instore.rawValue
                    self.pushVC = vc
                    
                    break
                    
                case AppData.location:
                    self.lat = dic["lat"] as? Double ?? 0.0
                    self.long = dic["long"] as? Double ?? 0.0
                    locateWithLong(lon: self.long, lat: self.lat)
                    break
                    
                default:
                    break
                }
            }
        }
    }
    
    
}

extension NChoosePickUpTypeVC: GMSMapViewDelegate,CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")

        locateWithLong(lon: (locationManager.location?.coordinate.longitude ?? 0)!, lat: (locationManager.location?.coordinate.latitude ?? 0)!)
        
        print("current location didupdate :\(locationManager.location?.coordinate.latitude ?? 0) \(locationManager.location?.coordinate.longitude ?? 0)")
        self.lat = locationManager.location?.coordinate.latitude ?? 0
        self.long = locationManager.location?.coordinate.longitude ?? 0
        let userLocation :CLLocation = locations[0] as CLLocation
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverse geocode")
            }
            let placemark = (placemarks ?? [CLPlacemark]()) as [CLPlacemark]
            if placemark.count > 0 {
                let placemark = placemarks![0]
                print(placemark.locality!)
                print(placemark.administrativeArea!)
                print(placemark.country!)
       
                var addressString : String = ""
                                    if placemark.subLocality != nil {
                                        addressString = addressString + placemark.subLocality! + ", "
                                    }
                                    if placemark.locality != nil {
                                        addressString = addressString + placemark.locality! + ", "
                                    }
                                    if placemark.administrativeArea != nil {
                                        addressString = addressString + placemark.administrativeArea! + " "
                                    }
                                    if placemark.postalCode != nil {
                                        addressString = addressString + placemark.postalCode! + ", "
                                    }
                                   if placemark.country != nil {
                                           addressString = addressString + placemark.country! + " "
                                   }
                
                var locString : String = ""
                                        if placemark.subLocality != nil {
                                            locString = locString + placemark.subLocality!
                                        } else if placemark.locality != nil {
                                            locString = locString + placemark.locality!
                                        }else if placemark.administrativeArea != nil {
                                            locString = locString + placemark.administrativeArea!
                                        }else if placemark.country != nil {
                                            locString = locString + placemark.country!
                                        }
                self.loc = addressString
//                self.lblAddress.text = self.loc
//                self.lblLoc.text = locString
    
            }
        }
        locationManager.stopUpdatingLocation()

    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error: \(error)")
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        let geocoder = GMSGeocoder()
        
     geocoder.reverseGeocodeCoordinate(coordinate) { [self] response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
         
            let country = address.country ?? ""
            let postalCode = address.postalCode ?? ""
            let administrativeArea =  address.administrativeArea ?? ""
            let locality = address.locality ?? ""
            let subLocality = address.subLocality ?? ""
            let thoroughfare = address.thoroughfare ?? ""
        
         var addressString : String = ""
                             if address.subLocality != nil {
                                 addressString = addressString + address.subLocality! + ", "
                             }
                             if address.locality != nil {
                                 addressString = addressString + address.locality! + ", "
                             }
                             if address.administrativeArea != nil {
                                 addressString = addressString + address.administrativeArea! + " "
                             }
                             if address.postalCode != nil {
                                 addressString = addressString + address.postalCode! + ", "
                             }
                            if address.country != nil {
                                    addressString = addressString + address.country! + " "
                            }

        var locString : String = ""
                                if address.subLocality != nil {
                                    locString = locString + address.subLocality!
                                } else if address.locality != nil {
                                    locString = locString + address.locality!
                                }else if address.administrativeArea != nil {
                                    locString = locString + address.administrativeArea!
                                }else if address.country != nil {
                                    locString = locString + address.country!
                                }

         print("cordinate",address)
         print("cordinate",coordinate)
         print("country",country)
         print("postalCode",postalCode)
         print("administrativeArea",administrativeArea)
         print("locality",locality)
         print("subLocality",subLocality)
         print("thoroughfare",thoroughfare)
         print("address",addressString)

            
        self.lat = address.coordinate.latitude
        self.long = address.coordinate.longitude
        self.loc = addressString
        
//        self.lblAddress.text = self.loc
//        self.lblLoc.text = locString
  
        }
    }

}

extension NChoosePickUpTypeVC {
    
    func getInstoreDetails(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "distance": self.radius,
            "address_id": 91
        ]
        
        NCartViewModel.getInstoreList(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.arrStore = [StoreM]()
                        self.arrStore = data
                        self.tblView.reloadData()
                    }
                } else {
                   
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func redeemDeliveryPickupOffer(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "description": self.txtDesc.text ?? "",
            "location": self.loc,
            "latitude": self.lat,
            "longitude": self.long
        ]
        
        NCartViewModel.redeemPickupOffer(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
//                    if let data = response.data {
                        let vc = Storyboards.Main.viewController(controller: NCartVC.self)
                        vc.isOfferApplied = true
                        vc.isCheckOut = true
                        vc.type = OfferType.delivery.rawValue
                        self.pushVC = vc
                        self.tblView.reloadData()
//                    }
                } else {
                   
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}
