//
//  NInstoreTVC.swift
//  Needs
//
//  Created by webcastle on 15/02/22.
//

import UIKit

class NInstoreTVC: UITableViewCell {

    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgStore: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(model: StoreM) {
        self.imgStore.kf.setImage(with: URL(string:model.image ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        self.lblStoreName.text = model.title
        self.lblLocation.text = model.location
    }

}
