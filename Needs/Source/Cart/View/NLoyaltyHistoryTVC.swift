//
//  NLoyaltyTVC.swift
//  Needs
//
//  Created by webcastle on 15/02/22.
//

import UIKit

class NLoyaltyHistoryTVC: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(_ model: LoyaltyM) {
        self.lblTitle.text = model.title
        self.lblDesc.text = model.description
        self.lblPoints.text = model.points
    }
}
