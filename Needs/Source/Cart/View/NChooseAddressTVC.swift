//
//  NChooseAddressTVC.swift
//  Needs
//
//  Created by webcastle on 14/02/22.
//

import UIKit

enum AddressType: Int {
    case home = 0
    case office
    case other
}

class NChooseAddressTVC: UITableViewCell {

    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var imgRadio: UIImageView!
    
    weak var delegate: NotifyDelegate!
    var iPath: IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(model: AddressM){
        self.lblName.text = model.name
//        let mob = LocalPersistant.string(for: .phone_number)// \nMobile: +91 \(model.mobile ?? "")
        
        self.lblAddress.text = "\(model.home ?? ""),\(model.address ?? ""),\(model.landmark ?? ""),\(model.pincode ?? "")"
        if AddressType(rawValue: model.type ?? 0) == .home {
            self.lblType.text = "Home"
        }else if AddressType(rawValue: model.type ?? 0) == .office {
            self.lblType.text = "Office"
        }else {
            self.lblType.text = "Other"
        }
    }

    @IBAction func tappedEdit(_ sender: UIButton) {
        self.delegate.didSelect(withInfo: ["type": AppData.edit, "index": self.iPath])
    }
    
    @IBAction func tappedDelete(_ sender: UIButton) {
        self.delegate.didSelect(withInfo: ["type": AppData.delete, "index": self.iPath])
    }
    

}
