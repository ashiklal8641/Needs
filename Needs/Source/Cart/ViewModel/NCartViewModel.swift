//
//  NCartViewModel.swift
//  Needs
//
//  Created by webcastle on 09/03/22.
//

import UIKit
import Alamofire

class NCartViewModel: NSObject {

    static func getAddressList(parameters:Parameters,completion:@escaping Completion<[AddressM]>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getAddress)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func addNewAddress(parameters:Parameters,completion:@escaping Completion<HomeModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.addAddress)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func editAddress(parameters:Parameters,completion:@escaping Completion<HomeModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.editAddress)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func deleteAddress(parameters:Parameters,completion:@escaping Completion<HomeModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.deleteAddress)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getInstoreList(parameters:Parameters,completion:@escaping Completion<[StoreM]>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getInstoreList)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func redeemInstoreOffer(parameters:Parameters,completion:@escaping Completion<HomeModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.redeemInstoreOffer)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func redeemPickupOffer(parameters:Parameters,completion:@escaping Completion<HomeModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.redeemDeliveryPickupOffer)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getLoyaltyHistory(parameters:Parameters,completion:@escaping Completion<LoyaltyListM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getLoyalityInfo)),
                                 loader: true,
                                 completion: completion)
    } 
    
}
