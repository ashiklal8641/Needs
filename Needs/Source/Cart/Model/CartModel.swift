//
//  CartModel.swift
//  Needs
//
//  Created by webcastle on 09/03/22.
//

import Foundation

struct AddressM : Codable{
    var address : String?
    var id : Int?
    var type : Int?
    var name: String?
    var home: String?
    var landmark: String?
    var pincode: String?
    var mobile: String?
    var lat: String?
    var long: String?
}


struct StoreM : Codable{
    var image : String?
    var id : Int?
    var title : String?
    var location: String?
}
