//
//  NHomeViewModel.swift
//  Needs
//
//  Created by webcastle on 08/03/22.
//

import UIKit
import Alamofire

class NHomeViewModel: NSObject {

    static func getDashboardDetails(parameters:Parameters,completion:@escaping Completion<HomeModel>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.home)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getProductList(parameters:Parameters,completion:@escaping Completion<ProductListM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getProducts)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getSubCategoryList(parameters:Parameters,completion:@escaping Completion<[CategoryM]>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getCategories)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getProductDetails(parameters:Parameters,completion:@escaping Completion<ProductM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getProductDetail)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getWishList(parameters:Parameters,completion:@escaping Completion<[ProductM]>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getWishlist)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func updateWishList(parameters:Parameters,completion:@escaping Completion<ProductListM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.updateWishList)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func updateMonthlyList(parameters:Parameters,completion:@escaping Completion<ProductListM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.updateMonthlyBucket)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func addToCart(parameters:Parameters,completion:@escaping Completion<ProductM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.addToCart)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func updateCart(parameters:Parameters,completion:@escaping Completion<ProductM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.updateCart)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func removeCart(parameters:Parameters,completion:@escaping Completion<ProductM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.removeFromCart)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func placeOrder(parameters:Parameters,completion:@escaping Completion<PlaceOrderM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.placeOrder)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func checkPaymentResponse(parameters:Parameters,completion:@escaping Completion<PlaceOrderM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.paymentResponse)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getReturnReasons(parameters:Parameters,completion:@escaping Completion<[ReasonM]>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.returnReason)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func submitReturnProduct(parameters:Parameters,completion:@escaping Completion<PlaceOrderM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.returnItem)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func checkOutOfStock(parameters:Parameters,completion:@escaping Completion<ProductM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.outOfStock)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getNotificationList(parameters:Parameters,completion:@escaping Completion<[NotifListM]>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.getNotifications)),
                                 loader: true,
                                 completion: completion)
    }
    
    static func getShortCartInfo(parameters:Parameters,completion:@escaping Completion<ProductM>){
        APIClient.performRequest(route:  APIConfiguration(request: (parameters,.shortCartInfo)),
                                 loader: true,
                                 completion: completion)
    }

}
