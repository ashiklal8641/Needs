//
//  HomeModel.swift
//  Needs
//
//  Created by webcastle on 08/03/22.
//

import Foundation

class HomeModel : Codable{
    var monthly_bucket : MonthlyBucketM?
    var more_needs : MoreNeedsM?
    var main_banner : [BannerM]?
    var category : [CategoryM]?
    var new_arrivals : [ProductM]?
    var offer_zone : [ProductM]?
    var deal_of_the_week : [ProductM]?
    var big_sale: BannerM?
    var mega_offer: BannerM?
    var about_app: AboutM?
    var location: String?
}

struct CategoryM : Codable{
    var title : String?
    var image : String?
    var id : Int?
}


struct MonthlyBucketM : Codable{
    var title : String?
    var description : String?
    var image : String?
    var id : Int?
}

struct MoreNeedsM : Codable{
    var title : String?
    var description : String?
    var image : String?
    var id : Int?
}

struct BannerM : Codable{
    var type : String?
    var web_url : String?
    var image : String?
    var id : Int?
}

struct AboutM : Codable{
    var license_no : String?
    var address : String?
    var about: String?
    var terms: String?
    var policy: String?
}

struct NotifM : Codable{
    var notification_text : String?
    var notification_image : String?
    var notification_time: String?
}

struct NotifListM : Codable{
    var date : String?
    var content : [NotifM]?
}

class ProductM : Codable{
    var id : Int?
    var sku : String?
    var slug : String?
    var type : Int?
    var brand : String?
    var image : String?
    var name: String?
    var rating: Double?
    var shipping_cost: String?
    var selling_price:String?
    var price:String?
    var percent_off: Double?
    var stock: Int?
    var min_stock: String?
    var is_cart: Bool?
    var cart_quantity: Int?
    var quantity: Int?
    var is_wishlist: Bool?
    var variants: [ProductM]?
    var images: [String]?
    var first_theme_value: String?
    var dummy: String?
    var option_values: [OptionM]?
    var category_name: String?
    var category_slug: String?
    var current_price: Double?
    var previousAddedProductId: Int?
    var firstOptionValue: String?
    var delivery_days: String?
//    var discount: Double?
    var is_cod: Bool?
    var cod_charge: Double?
    var main_banner: [String]?
    var description: String?
    var features: String?
    var is_selected: Bool?
    var related_products: [ProductM]?
    var isOutOfStock: Bool?
    var variant_name: String?
    var cart_count: Int?
    var total_price: String?
}

struct OptionM : Codable{
    var theme_id : Int?
    var theme_name : String?
    var theme_value : String?
}

struct ProductListM : Codable{
    var current_page : Int?
    var data : [ProductM]?
    var from : Int?
    var last_page: Int?
    var per_page: Int?
    var to: Int?
    var total: Int?
}

struct LoyaltyM : Codable{
    var title : String?
    var image : String?
    var id : Int?
    var description : String?
    var points : String?
}

struct LoyaltyListM : Codable{
    var max_redeemable_points : Double?
    var loyalty : [LoyaltyM]?
}

struct PlaceOrderM : Codable{
    var payment_method : String?
    var razorpay_order_id : String?
    var order_id : Int?
    var order_no: Int?
    var amount: Double?
    var grand_total: String?
    var user_name: String?
//    var address: AddressM?
    var order_date: String?
    var delivery_date: String?
    var title: String?
    var description: String?
    
}

struct ReasonM : Codable{
    var id : Int?
    var title : String?
    var detail : [DetailM]?
}

struct DetailM : Codable{
    var id : Int?
    var title : String?
}

class Option {
    var optionTitle :String?
    var themeId :Int?
    var optionItems : [OptionItems]
    
//    init() {}
    
    init(title: String, id: Int, items: [OptionItems]) {
        self.optionTitle = title
        self.themeId = id
        self.optionItems = items
    }
}

class OptionItems {
    var productId : [Int]?
    var themeId : Int?
    var themeName : String?
    var themeValue : String?
    var themecolor : String?
    
    init() {}
    
    init(prodId: [Int],id: Int, name: String, value: String, color: String) {
        self.productId = prodId
        self.themeId = id
        self.themeName = name
        self.themeValue = value
        self.themecolor = color
    }
}
