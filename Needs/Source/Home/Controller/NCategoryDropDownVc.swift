//
//  NCategoryDropDownVc.swift
//  Needs
//
//  Created by webcastle on 24/02/22.
//

import UIKit

class NCategoryDropDownVc: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    
    var delegate: NotifyDelegate?
    var selectedIndex = -1
    var arrCat = [CategoryM]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.lblTitle.text = self.arrCat[selectedIndex].title ?? ""
        self.tblView.reloadData()
        tblHeightConstraint.constant = min(self.tblView.contentSize.height, self.view.frame.height/2)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.1) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        }
    }

    // MARK: - Button action
    
    @IBAction func tappedContinue(_ sender: UIButton) {
        
    }
    
    @IBAction func tappedClose(_ sender: UIButton) {
        self.dismiss(animated: false) {
            
        }
    }


}

extension NCategoryDropDownVc: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrCat.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(NCategoryDropDownTVC.self, for: indexPath)
        cell.lblTitle.text = arrCat[indexPath.row].title
        if indexPath.row == selectedIndex {
            cell.contentView.backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9803921569, blue: 0.9725490196, alpha: 1)
        }else {
            cell.contentView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tblView.reloadData()
        self.dismiss(animated: false) {
            self.delegate?.didSelect(withInfo: ["type": AppData.category,"index": indexPath])
        }
    }
}
