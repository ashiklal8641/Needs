//
//  NProductDetailVC.swift
//  Needs
//
//  Created by webcastle on 13/02/22.
//

import UIKit
import FSPagerView
import Alamofire

class NProductDetailVC: ParentController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bannerCV: UICollectionView!
    @IBOutlet weak var lblAddToBucket: UILabel!
    @IBOutlet weak var lblPdtName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSellingPrice: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var viewCart: UIView!
    @IBOutlet weak var addCart: UIView!
    @IBOutlet weak var txtCount: UITextField!
    @IBOutlet weak var imgFav: UIImageView!
    @IBOutlet weak var lblAddCart: UILabel!
    @IBOutlet weak var pageControll : FSPageControl!{
        didSet {
            self.pageControll.setPath(UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: 5, height: 5),byRoundingCorners: [.topLeft, .topRight,.bottomRight,.bottomLeft], cornerRadii: CGSize(width:5, height: 5)), for: .normal)
            self.pageControll.setPath(UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: 14, height: 5),byRoundingCorners: [.topLeft, .topRight,.bottomRight,.bottomLeft], cornerRadii: CGSize(width:14, height: 5)), for: .selected)
            
            self.pageControll.interitemSpacing = 0
            self.pageControll.itemSpacing = 18
           
            self.pageControll.contentHorizontalAlignment = .center
            self.pageControll.backgroundColor = UIColor.clear
            self.pageControll.setFillColor(#colorLiteral(red: 0.003921568627, green: 0.5137254902, blue: 0.3254901961, alpha: 1), for: .selected)
            self.pageControll.setFillColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            self.pageControll.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)

        }}
    
    var timer = Timer()
    var counter = 0
    var isAddedToBucket = false
    var isFav = false
    var pdtId = -1
    var slug = ""
    var pdtModel = ProductM()
    var OptionModel = [Option]()
    var OptionItemModel = [OptionItems]()
    var isSelectedDesc = true
    var selectedId = -1
    weak var delegate: NotifyDelegate?
        
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getProductDetails()
       
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        }
        
    }
    

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
    
    @objc func changeImage() {
        if counter < self.pdtModel.main_banner?.count ?? 0 {
            let index = IndexPath.init(item: counter, section: 0)
            self.bannerCV.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            pageControll.currentPage = self.counter
            self.counter += 1
        } else {
            counter = 0
            let index = IndexPath.init(item: counter, section: 0)
            self.bannerCV.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
            pageControll.currentPage = self.counter
            self.counter = 1
        }
            
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if bannerCV == scrollView {
             let pageWidth = bannerCV.frame.size.width
            let page = floor(Double(Int((bannerCV.contentOffset.x) + 1)/Int(pageWidth)))
            self.pageControll.currentPage = Int(page)
        }
    }
    
    @IBAction func tappedViewCart(_ sender: UIButton) {
       if self.pdtModel.is_cart ?? false {
            let vc = Storyboards.Main.viewController(controller: NCartVC.self)
            vc.isFromPdt = true
            self.pushVC = vc
       }else {
        if self.pdtModel.stock == 0  {
            Utility.displayBanner(message: "Product is Out of Stock", style: .danger)
            return
        }
        if self.pdtModel.variants?.count ?? 0 > 0 {
            self.setupui(model: pdtModel)
            let vc = Storyboards.Cart.viewController(controller: NProductVariantVC.self)
            vc.delegate = self
            vc.variantProductId = self.selectedId
            vc.pdtModel = pdtModel
            vc.OptionModel = self.OptionModel
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: false, completion: nil)
        }else {
            self.addToCart(qty: 1)
        }
       }
      
    }
    
    @IBAction func tappedMonthlyBucket(_ sender: UIButton) {
        if isAddedToBucket {
            let vc = Storyboards.Home.viewController(controller: NWishListVC.self)
            vc.isFromWishList = false
            self.pushVC = vc
        }else {
            self.updateMonthlyBucket()
//            self.lblAddToBucket.text = "View Monthly Bucket"
//            self.isAddedToBucket = true
        }
    }

    @IBAction func tappedFav(_ sender: UIButton) {
        self.updateWishList()
    }
    
    @IBAction func tappedAdd(_ sender: UIButton) {
        
//        var count = self.pdtModel.cart_quantity
//                        let tvc = tblView.cellForRow(at: IndexPath(row: path.row, section: 0)) as! NProductTVC
//                        let cartCount = tvc.txtQty.text ?? "0"
//                        var count = Int(cartCount)
//        if count ?? 0 > 0 {
//            count = (count ?? 0) + 1
//            self.addToCart(qty: count ?? 0)
////                            tvc.txtQty.text = String(count ?? 0)
//        }
//        self.updateCart(id: self.pdtModel.id ?? -1, isFirst: true, isAdd: false, isFromCart: false)
        
        let model = self.pdtModel
        if model.variants?.count ?? 0 > 0 {
            let count = self.pdtModel.cart_quantity
//            if count ?? 0 > 1 {
//                self.setupui(model: pdtModel)
//                let vc = Storyboards.Cart.viewController(controller: NProductVariantVC.self)
//                vc.delegate = self
//                vc.variantProductId = self.selectedId
//                vc.pdtModel = pdtModel
//                vc.OptionModel = self.OptionModel
//                vc.modalPresentationStyle = .overFullScreen
//                self.present(vc, animated: false, completion: nil)
//            }else {
                self.updateCart(id: model.id ?? -1, isFirst: true, isAdd: true, isFromCart: false)
//            }            
        }else {
            self.updateCart(id: model.id ?? -1, isFirst: false, isAdd: true, isFromCart: false)
        }
        
    }
    
    @IBAction func tappedMinus(_ sender: UIButton) {
//        var count = self.pdtModel.cart_quantity
//        if count ?? 0 > 1 {
//            count = (count ?? 0) - 1
//        }else {
//            count = (count ?? 0) - 1
//        }
//        self.updateCart(id: self.pdtModel.id ?? -1, isFirst: true, isAdd: false, isFromCart: false)
        
        if self.pdtModel.variants?.count ?? 0 > 0 {
            var count = self.pdtModel.cart_quantity
            if count ?? 0 > 1 {
                let vc = Storyboards.New.viewController(controller: NLogoutpopupVC.self)
                vc.delegate = self
                vc.isDelete = true
                vc.modalPresentationStyle = .overFullScreen
                self.present(vc, animated: false, completion: nil)
            }else {
                count = (count ?? 0) - 1
                self.updateCart(id: self.pdtModel.id ?? -1, isFirst: false, isAdd: false, isFromCart: false)
            }
        }else {
            self.updateCart(id: self.pdtModel.id ?? -1, isFirst: false, isAdd: false, isFromCart: false)
        }
       
    }
    
    func setUpUI() {
        self.imgFav.image = self.pdtModel.is_wishlist ?? false ? UIImage(named: "fav") : UIImage(named: "notFav")
        self.lblPdtName.text = self.pdtModel.name
        self.lblTitle.text = self.pdtModel.name
        self.lblPrice.attributedText = "₹ \(self.pdtModel.selling_price ?? "0")".updateStrikeThroughFont(.lightGray)
        self.lblSellingPrice.text = "₹ \(self.pdtModel.price ?? "0")"
        self.addCart.isHidden = !(self.pdtModel.is_cart ?? false)
        self.lblAddCart.text = self.pdtModel.is_cart ?? false ? "View Cart" : "Add Cart"
        self.txtCount.text = "\(self.pdtModel.cart_quantity ?? 0)"
    }

    
    func setupui(model: ProductM) {
        self.OptionModel = [Option]()
        self.OptionItemModel = [OptionItems]()
        
        for i in 0..<(model.variants?.count ?? 0)
        {
            
            let variant = model.variants?[i]
            for k in 0..<(model.variants?[i].option_values?.count ?? 0)
            {
                if OptionModel.count == 0
                {
                    let value = OptionItems(prodId: [variant?.id ?? 0], id: variant?.option_values?[k].theme_id ?? 0, name: variant?.option_values?[k].theme_name ?? "", value: variant?.option_values?[k].theme_value ?? "",color:"2")
                    OptionModel.append(Option(title: variant?.option_values?[k].theme_name ?? "", id: variant?.option_values?[k].theme_id ?? 0, items:[value]))
                    self.selectedId = variant?.id ?? 0
                }
                else
                {
                    let optitem = OptionModel.filter { $0.themeId == variant?.option_values?[k].theme_id}
                    if optitem.count <= 0 {
                        let value = OptionItems(prodId: [variant?.id ?? 0], id: variant?.option_values?[k].theme_id ?? 0, name: variant?.option_values?[k].theme_name ?? "", value: variant?.option_values?[k].theme_value ?? "",color:"2")
                        OptionModel.append(Option(title: variant?.option_values?[k].theme_name ?? "", id: variant?.option_values?[k].theme_id ?? 0, items:[value]))
                        self.selectedId = variant?.id ?? 0
                    }else {
                        for opt in optitem {
                            let optionitem = opt.optionItems.filter { $0.themeValue == variant?.option_values?[k].theme_value}
                            if optionitem.count <= 0 {
                                opt.optionItems.append(OptionItems(prodId: [variant?.id ?? 0], id: variant?.option_values?[k].theme_id ?? 0, name: variant?.option_values?[k].theme_name ?? "", value: variant?.option_values?[k].theme_value ?? "",color:"1"))
                            }else {
                                for item in optionitem {
                                    item.productId?.append(variant?.id ?? 0)
                                }
                            }
                        }
                        
                    }
                                        
                }
                
            }
            
        }
    }
    
}

extension NProductDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.pdtModel.main_banner?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(NMainBannerCVC.self, for: indexPath)
        cell.imgBanner.kf.setImage(with: URL(string:self.pdtModel.main_banner?[indexPath.item] ?? ""), placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width
        let height = collectionView.frame.size.height
        return CGSize(width: width, height: height)
    }
    
}

extension NProductDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
          return  1
        }else {
            if pdtModel.related_products?.count ?? 0 > 0{
                return 1
            }else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(NProductDescTVC.self, for: indexPath)
            cell.delegate = self
            if isSelectedDesc {
                cell.lblContent.text = self.pdtModel.description?.htmlToString
                cell.lblDesc.textColor = .black
                cell.lblFeature.textColor = .lightGray
            }else {
                cell.lblContent.text = self.pdtModel.features?.htmlToString
                cell.lblDesc.textColor = .lightGray
                cell.lblFeature.textColor = .black
            }
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(NProductDetailTVC.self, for: indexPath)
            cell.arrPrdts = self.pdtModel.related_products ?? [ProductM]()
            cell.pdtCV.reloadData()
            cell.cvHeightConstraint.constant = cell.pdtCV.collectionViewLayout.collectionViewContentSize.height
            return cell
        }
    }
    
    
}

extension NProductDetailVC {
    
    func getProductDetails(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "slug": slug
        ]
        
        NHomeViewModel.getProductDetails(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.pageControll.numberOfPages = data.main_banner?.count ?? 0
                        self.pdtModel = ProductM()
                        self.pdtModel = data
                        self.setUpUI()
                        self.bannerCV.reloadData()
                        self.tblView.reloadData()
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func updateMonthlyBucket(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "product_id": pdtId
        ]
        
        NHomeViewModel.updateMonthlyList(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
//                    if let data = response.data {
                        self.isAddedToBucket = !self.isAddedToBucket
                        self.lblAddToBucket.text = "View Monthly Bucket"
                        self.isAddedToBucket ? Utility.displayBanner(message: "Product added to monthly bucket", style: .success) : Utility.displayBanner(message: "Product removed from monthly bucket", style: .success)
//                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
//
    }
    
    func updateWishList(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "product_id": pdtId
        ]
        
        NHomeViewModel.updateWishList(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                        self.pdtModel.is_wishlist = !(self.pdtModel.is_wishlist ?? false)
                        self.imgFav.image = self.pdtModel.is_wishlist ?? false ? UIImage(named: "fav") : UIImage(named: "notFav")
                        self.pdtModel.is_wishlist ?? false ? Utility.displayBanner(message: "Product added to wishlist", style: .success) : Utility.displayBanner(message: "Product removed from wishlist", style: .success)
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func addToCart(qty: Int){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "product_id": pdtId,
            "quantity": qty
        ]
        
        NHomeViewModel.addToCart(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    self.pdtModel.is_cart = qty <= 0 ? false : true
                    self.addCart.isHidden = !(self.pdtModel.is_cart ?? false)
                    self.lblAddCart.text = self.pdtModel.is_cart ?? false ? "View Cart" : "Add Cart"
                    self.txtCount.text = "\(qty)"
                    self.delegate?.didSelect(withInfo: ["type": AppData.cart])
                    self.getProductDetails()
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func updateCart(id: Int, isFirst: Bool, isAdd: Bool, isFromCart: Bool){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "product_id": id,
            "firstTime": isFirst,
            "fromCart": isFromCart,
            "isAdd": isAdd ? 1 : 0
        ]
        
        NHomeViewModel.updateCart(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if isFirst {
//                        self.setupui(model: self.pdtModel)
//                        let vc = Storyboards.Cart.viewController(controller: NProductVariantVC.self)
//                        vc.delegate = self
//                        vc.pdtModel = self.pdtModel
//                        vc.variantProductId = self.selectedId
//                        vc.isFromCart = true
//                        vc.OptionModel = self.OptionModel
//                        vc.modalPresentationStyle = .overFullScreen
//                        self.present(vc, animated: false, completion: nil)
                    }else {
                        if isAdd {
                            
                        }else {
                            
                        }
                        self.delegate?.didSelect(withInfo: ["type": AppData.cart])
                        self.getProductDetails()
                    }
                } else if response.errorcode == 1 {
                    if let data = response.data {
                        if isFirst {
                            self.setupui(model: self.pdtModel)
                            let vc = Storyboards.Cart.viewController(controller: NProductVariantVC.self)
                            vc.delegate = self
                            vc.variantPdtModel = data
                            vc.pdtModel = self.pdtModel
                            vc.OptionModel = self.OptionModel
                            vc.variantProductId = data.previousAddedProductId ?? -1
                            vc.isFromCart = true
                            vc.modalPresentationStyle = .overFullScreen
                            self.present(vc, animated: false, completion: nil)
                        }else {
                            if isAdd {
                                
                            }else {
                                
                            }
                            self.delegate?.didSelect(withInfo: ["type": AppData.cart])
                            self.getProductDetails()
                        }
                    }
                    
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}

extension NProductDetailVC: NotifyDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
            if let type = dic["type"] as? String {
                switch type {
                case AppData.desc:
                    self.isSelectedDesc = true
                    self.tblView.reloadData()
                    break
                case AppData.feature:
                    self.isSelectedDesc = false
                    self.tblView.reloadData()
                    break
                case AppData.cart:
                    self.delegate?.didSelect(withInfo: ["type": AppData.addCart])
                    break
                case AppData.addCart:
                    self.getProductDetails()
                    break
                case AppData.yes:
                    let vc = Storyboards.Main.viewController(controller: NCartVC.self)
                    vc.isFromPdt = true
                    self.pushVC = vc
                    break
                default:
                    break
                }
            }
        }
    }
}
