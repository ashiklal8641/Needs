//
//  NWishListVC.swift
//  Needs
//
//  Created by webcastle on 11/02/22.
//

import UIKit
import Alamofire

class NWishListVC: ParentController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var isFromWishList = true
    var pdtList = [ProductM]()
    var pdtModel = ProductM()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.registerCell()
        self.lblTitle.text = isFromWishList ? "Wishlist" : "Monthly Bucket"
        self.getWishList()
    }
    

    //---------------------------------------------------------------------------------
    // MARK: - UI UPDATIONS
    //---------------------------------------------------------------------------------
    
    private func registerCell(){
        self.tblView.register(NProductTVC.self)
    }


}

extension NWishListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.pdtList.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(NProductTVC.self, for: indexPath)
//        cell.lblOriginalPrice.attributedText = "₹ 150".updateStrikeThroughFont(.lightGray)
        cell.configureCell(model: self.pdtList[indexPath.row])
        cell.delegate = self
        cell.iPath = indexPath
        if isFromWishList {
            cell.type = .wishlist
        }else {
            cell.type = .monthlyBucket
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Storyboards.Home.viewController(controller: NProductDetailVC.self)
        vc.pdtId = self.pdtList[indexPath.row].id ?? -1
        vc.slug = self.pdtList[indexPath.row].slug ?? ""
        self.pushVC = vc
    }
}

extension NWishListVC: NotifyDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
            if let path = dic["index"] as? IndexPath {
                if let type = dic["type"] as? String {
                    switch type {
                    case AppData.delete:
                        let vc = Storyboards.New.viewController(controller: NLogoutpopupVC.self)
                        vc.delegate = self
                        vc.page = .fav
                        vc.iPath = path
                        vc.modalPresentationStyle = .overFullScreen
                        self.present(vc, animated: false, completion: nil)
                        break
                    case AppData.addCart:
                        let tvc = tblView.cellForRow(at: IndexPath(row: path.row, section: 0)) as! NProductTVC
                        tvc.txtQty.text = "1"
                        tvc.viewAddCart.isHidden = true
                        break
                    case AppData.add:
                        let tvc = tblView.cellForRow(at: IndexPath(row: path.row, section: 0)) as! NProductTVC
                        let cartCount = tvc.txtQty.text ?? "0"
                        var count = Int(cartCount)
                        if count ?? 0 > 0 {
                            count = (count ?? 0) + 1
                            tvc.txtQty.text = String(count ?? 0)
                        }
                        tvc.viewAddCart.isHidden = true
                        break
                    case AppData.minus:
                        let tvc = tblView.cellForRow(at: IndexPath(row: path.row, section: 0)) as! NProductTVC
                        let cartCount = tvc.txtQty.text ?? "0"
                        var count = Int(cartCount)
                        if count ?? 0 > 1 {
                            count = (count ?? 0) - 1
                            tvc.txtQty.text = String(count ?? 0)
                        }else {
                            count = (count ?? 0) - 1
                            tvc.txtQty.text = String(count ?? 0)
                            tvc.viewAddCart.isHidden = false
                        }
                        
                        break
                    case AppData.yes:
                        if isFromWishList {
                            self.updateWishList(index: path.row)
                        }else {
                            self.updateMonthlyBucket(index: path.row)
                        }
                        break
                    default:
                        break
                    }
                    
                }
            }else if let type = dic["type"] as? String {
                switch type {
                case AppData.favourite:
                    
                    break
                default:
                    break
                }
            }
        }
    }
    
    
}

extension NWishListVC {
    
    func getWishList(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "type": isFromWishList ? 0 : 1
        ]
        
        NHomeViewModel.getWishList(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.pdtList = [ProductM]()
                        self.pdtList = data
                        self.tblView.reloadData()
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func updateWishList(index: Int){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "product_id": self.pdtList[index].id ?? 0
        ]
        
        NHomeViewModel.updateWishList(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                        Utility.displayBanner(message: "Product removed from wishlist", style: .success)
                        self.getWishList()
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func updateMonthlyBucket(index: Int){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "product_id": self.pdtList[index].id ?? 0
        ]
        
        NHomeViewModel.updateMonthlyList(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        Utility.displayBanner(message: "Product removed from monthly bucket", style: .success)
                        self.getWishList()
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}

