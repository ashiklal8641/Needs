//
//  NSearchVC.swift
//  Needs
//
//  Created by webcastle on 18/02/22.
//

import UIKit
import Alamofire

class NSearchVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewNoData: UIView!
    
    var pdtList = [ProductM]()
    var pdtModel = ProductM()
    var type: Int = PageType.home.rawValue
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.registerCell()
    }
    

    //---------------------------------------------------------------------------------
    // MARK: - UI UPDATIONS
    //---------------------------------------------------------------------------------
    
    private func registerCell(){
        self.tblView.register(NProductTVC.self)
    }

    //---------------------------------------------------------------------------------
    // MARK: - Button Actions
    //---------------------------------------------------------------------------------
    
    @IBAction func didTappedTextfield(_ sender: UITextField) {
        let searchTerm = sender.text
        if searchTerm?.count ?? 0 > 2 {
            self.getProductList(term: searchTerm ?? "")
        }
    }
    
    @IBAction func didTappedClearBtn(_ sender: UITextField) {
        self.txtSearch.text = ""
    }
    
}

extension NSearchVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.pdtList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(NProductTVC.self, for: indexPath)
        cell.configureCell(model: self.pdtList[indexPath.row])
        cell.delegate = self
        cell.iPath = indexPath
        cell.type = .product
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = Storyboards.Cart.viewController(controller: NProductVariantVC.self)
////        vc.delegate = self
//        vc.modalPresentationStyle = .overFullScreen
//        self.present(vc, animated: false, completion: nil)
    }
}

extension NSearchVC {
    
    func getProductList(term: String){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "batchSize": 3,
            "page": 20,
            "type": type,
            "term": term
        ]
        
        NHomeViewModel.getProductList(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.pdtList = [ProductM]()
                        self.pdtList = data.data ?? [ProductM]()
                        self.viewNoData.isHidden = self.pdtList.count > 0 ? true: false
                        self.tblView.reloadData()
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}

extension NSearchVC: NotifyDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
            if let path = dic["index"] as? IndexPath {
                if let type = dic["type"] as? String {
                    switch type {
                    case AppData.category:
                       
                        break
                        
                    default:
                        break
                    }
                    
                }
            }else if let type = dic["type"] as? String {
                
                switch type {
                case AppData.save:
                  
                    break

                    
                default:
                    break
                }
                
            }
        }
    }
    
    
}
