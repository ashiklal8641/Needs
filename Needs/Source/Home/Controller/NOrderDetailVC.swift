//
//  NOrderDetailVC.swift
//  Needs
//
//  Created by webcastle on 16/02/22.
//

import UIKit
import Alamofire

class NOrderDetailVC: ParentController {

    @IBOutlet weak var tblView: UITableView!
    
    var orderId = -1
    var orderModel = OrderM()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.registerCell()
        self.getOrderDetails()
    }
    

    //---------------------------------------------------------------------------------
    // MARK: - UI UPDATIONS
    //---------------------------------------------------------------------------------
    
    private func registerCell(){
        self.tblView.register(NProductTVC.self)
    }


}

extension NOrderDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return orderModel.products?.count ?? 0 > 0 ? ((orderModel.products?.count ?? 0) + 1 ) : 0
        }else if section == 1{
            return orderModel.status_list?.count ?? 0
        }else {
            return 3
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        if section == 0 {
            let count = self.orderModel.products?.count ?? 0            
                if row == count {
                    let cell = tableView.dequeueReusableCell(NEmptyTVC.self, for: indexPath)
                    return cell
                }else {
                    let cell = tableView.dequeueReusableCell(NProductTVC.self, for: indexPath)
                    cell.configureCell(model: self.orderModel.products?[row] ?? ProductM())
                    cell.delegate = self
                    cell.iPath = indexPath
                    cell.type = .order
                    return cell
                }
        }else if section == 1 {
            let count = self.orderModel.status_list?.count ?? 0
            if count > 1 {
                if row == count - 1{
                    let cell = tableView.dequeueReusableCell(NOrderStatusCompletedTVC.self, for: indexPath)
                    cell.lblStatus.text = self.orderModel.status_list?[row].name
                    cell.lblDate.text = self.orderModel.status_list?[row].date
                    return cell
                }else {
                    let cell = tableView.dequeueReusableCell(NOrderStatusTVC.self, for: indexPath)
                    cell.lblStatus.text = self.orderModel.status_list?[row].name
                    cell.lblDate.text = self.orderModel.status_list?[row].date
                    return cell
                }
            }else {
                let cell = tableView.dequeueReusableCell(NOrderStatusCompletedTVC.self, for: indexPath)
                cell.lblStatus.text = self.orderModel.status_list?[row].name
                cell.lblDate.text = self.orderModel.status_list?[row].date
                return cell
            }
            
        }else {
            if row == 0 {
                let cell = tableView.dequeueReusableCell(NDeliveryByTVC.self, for: indexPath)

                return cell
            }else if row == 1 {
                let cell = tableView.dequeueReusableCell(NDeliveryAddressTVC.self, for: indexPath)
                cell.lblAddress.text = "\(self.orderModel.deliver_address?.name ?? ""),\(self.orderModel.deliver_address?.address ?? ""),\(self.orderModel.deliver_address?.landmark ?? ""),\(self.orderModel.deliver_address?.pincode ?? "") \nMobile: +91 \(self.orderModel.deliver_address?.mobile ?? "")"
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(NPaymentDetailsTVC.self, for: indexPath)
                let count = self.orderModel.price_details?.count ?? 0
                var count1 = 0
                var item = String()
                var itemprice = String()
                for i in 0 ..< count {
                    count1 = count1+1
                    if count1 == count
                    {
                        
                        item.append("\(self.orderModel.price_details?[i].text ?? "")")
                        itemprice.append("\(self.orderModel.price_details?[i].value ?? "")")
                    }
                    else
                    {
                        item.append("\(self.orderModel.price_details?[i].text ?? "")\n\n")
                        itemprice.append("\(self.orderModel.price_details?[i].value ?? "")\n\n")
                    }
                }
                
                cell.lblTitle.text = item
                cell.lblValue.text = itemprice
                cell.lblTotal.text = "₹ \(self.orderModel.grand_total ?? "")"
                return cell
            }
        }
    }
    
    
}

extension NOrderDetailVC: NotifyDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
            if let path = dic["index"] as? IndexPath {
                if let type = dic["type"] as? String {
                    
                    switch type {
                    case AppData.returnItem :
                        
                        let vc = Storyboards.Home.viewController(controller: NReturnPdtDetailVC.self)
                        vc.pdtId = self.orderModel.products?[path.row].id ?? -1
                        vc.orderId = self.orderModel.id ?? -1
                        self.pushVC = vc
                        break
                        
                    case AppData.product:
                        
                        let vc = Storyboards.Home.viewController(controller: NProductDetailVC.self)                   
                        self.pushVC = vc
                        break
                        
                    default:
                        break
                    }
                    
                }
            }else if let type = dic["type"] as? String {
                switch type {
                
                case AppData.favourite:
                    
                    
                    break
                    
                default:
                    break
                }
            }
        }
    }
    
    
}

extension NOrderDetailVC {
    
    func getOrderDetails(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "order_id": self.orderId
        ]
        
        NMainViewModel.getOrerDetail(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.orderModel = OrderM()
                        self.orderModel = data
                        self.tblView.reloadData()
                    }
                } else {
                   
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}
