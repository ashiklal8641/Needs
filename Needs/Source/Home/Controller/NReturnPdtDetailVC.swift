//
//  NReturnPdtDetailVC.swift
//  Needs
//
//  Created by webcastle on 17/02/22.
//

import UIKit
import Alamofire
import DropDown

class NReturnPdtDetailVC: ParentController {

    @IBOutlet weak var tblView: UITableView!
    
    let reasonDropDown = DropDown()
    let detailDropDown = DropDown()
    var isReasonOpen = false
    var isAddressOpen = false
    var isReturnOpen = false
    var pdtId = -1
    var orderId = -1
    var arrReason = [ReasonM]()
    var arrDesc = [DetailM]()
    var arrReasonTitle = [String]()
    var arrDescTitle = [String]()
    var seleReasonId = -1
    var seleDetailId = -1
    var selId = -1
    var comment = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.registerCell()
        self.getReturnReasons()
    }
    

    //---------------------------------------------------------------------------------
    // MARK: - UI UPDATIONS
    //---------------------------------------------------------------------------------
    
    private func registerCell(){
        self.tblView.register(NProductTVC.self)
    }
    
    //---------------------------------------------------------------------------------
    // MARK: - Button Actions
    //---------------------------------------------------------------------------------
    
    @IBAction func tappedReturnPolicy(_ sender: UIButton) {
        let vc  = Storyboards.New.viewController(controller: NPrivacyVC.self)
        vc.isFromMenu = false
        self.pushVC = vc
    }
    
    @IBAction func tappedContinue(_ sender: UIButton) {
        if validateFields() {
            self.submitReturnProduct()
        }
    }

    //---------------------------------------------------------------------------------
    // MARK: - Custom Actions
    //---------------------------------------------------------------------------------

    func initialiseReasonDropDown() {
        DropDown.appearance().cornerRadius = 10
//        dropDown.anchorView = viewAnchor
        reasonDropDown.direction = .bottom
        reasonDropDown.dataSource = arrReasonTitle
        reasonDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            let tvc = self.tblView.cellForRow(at: IndexPath(row: 1, section: 0)) as! NReasonForReturnTVC
            tvc.txtReason.text = item
            tvc.txtDetail.text = ""
            self.seleReasonId = self.arrReason[index].id ?? -1
            self.selId = index
            let model = self.arrReason[index].detail ?? [DetailM]()
            self.arrDescTitle = [String]()
            for i in model {
                self.arrDescTitle.append(i.title ?? "")
            }
            self.initialiseDescriptionDropDown()
            self.seleDetailId = -1
        }
    }
    
    func initialiseDescriptionDropDown() {
        DropDown.appearance().cornerRadius = 10
//        dropDown.anchorView = viewAnchor
        detailDropDown.direction = .bottom
        detailDropDown.dataSource = arrDescTitle
        detailDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            let tvc = self.tblView.cellForRow(at: IndexPath(row: 1, section: 0)) as! NReasonForReturnTVC
            tvc.txtDetail.text = item
            let model = self.arrReason[selId].detail ?? [DetailM]()
            self.seleDetailId = model[index].id ?? -1
        }
    }
    
    func validateFields() -> Bool {
        if seleReasonId == -1{
            Utility.displayBanner(message: "Please choose a reason", style: .danger)
            return false
        }
        
        if  seleDetailId == -1 {
            Utility.displayBanner(message: "Please choose a detail", style: .danger)
            return false
        }
        
        if comment == "" {
            Utility.displayBanner(message: "Please choose a comment", style: .danger)
            return false
        }
        
        return true
    }
    
}

extension NReturnPdtDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 2
        }else if section == 1 {
            return 5
        }else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        if section == 0 {
            if row == 0 {
                let cell = tableView.dequeueReusableCell(NProductTVC.self, for: indexPath)
//                cell.lblOriginalPrice.attributedText = "₹ 150".updateStrikeThroughFont(.lightGray)
                cell.type = .returnPdt
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(NReasonForReturnTVC.self, for: indexPath)
                cell.delegate = self
                return cell
            }
        }else if section == 1 {
            if row == 0 {
                let cell = tableView.dequeueReusableCell(NPickUpAddressTVC.self, for: indexPath)
                
                return cell
            }else if row == 4 {
                let cell = tableView.dequeueReusableCell(NContinueBtnTVC.self, for: indexPath)
               
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(NChooseAddressTVC.self, for: indexPath)
               
                return cell
            }
        }else {
            if row == 0 {
                let cell = tableView.dequeueReusableCell(NReturnAcitonTVC.self, for: indexPath)
                
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(NContinueBtnTVC.self, for: indexPath)
               
                return cell
            }
        }
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sec = indexPath.section
        let row = indexPath.row
        if sec == 1 {
            if row == 1 {
                isReasonOpen = !isReasonOpen
            }
        }
    }
    
}

extension NReturnPdtDetailVC {
    
    func getReturnReasons(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "product_id": pdtId
        ]
        
        NHomeViewModel.getReturnReasons(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        self.arrReason = [ReasonM]()
                        self.arrReason = data
                        if self.arrReason.count > 0 {
                            for reason in self.arrReason {
                                self.arrReasonTitle.append(reason.title ?? "")
                            }
                            
                            let model = self.arrReason.first?.detail ?? [DetailM]()
                            for i in model {
                                self.arrDescTitle.append(i.title ?? "")
                            }
                        }
                        self.initialiseReasonDropDown()
                        self.initialiseDescriptionDropDown()
                        self.tblView.reloadData()
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
    
    func submitReturnProduct(){
        
        let parameter:Parameters = [
            "user_id": SessionHandler.uID ?? SessionHandler.preUserID,
            "order_product_id": pdtId,
            "comments": comment,
            "order_id": orderId,
            "reason_detail_id": seleDetailId,
            "reason_id": seleReasonId
        ]
        
        NHomeViewModel.submitReturnProduct(parameters: parameter) { result in
            switch result {
            case .success(let response):
                if response.errorcode == 0 {
                    if let data = response.data {
                        let vc  = Storyboards.Cart.viewController(controller: NOrderPlacedVC.self)
                        vc.isFromOrder = false
                        vc.orderModel = data
                        self.pushVC = vc
                    }
                } else {
                    Utility.displayBanner(message: response.message ?? "", style: .danger)
                }
            case .failure(let error):
                DebugLogger.error(error.localizedDescription)
            }
        }
        
    }
}

extension NReturnPdtDetailVC: NotifyDelegate {
    
    func didSelect(withInfo info: [String : Any]?) {
        guard let info = info else { return }
        print("info",info)
        if let dic = info as? [String: Any] {
                if let type = dic["type"] as? String {
                    switch type {
                    case AppData.reason:
                        self.reasonDropDown.show()
                    case AppData.detail:
                        self.detailDropDown.show()
                    case AppData.desc:
                        if let text = dic["text"] as? String {
                            self.comment = text
                        }
                    default:
                        break
                    }
                }
            }
    }
    
    
}

