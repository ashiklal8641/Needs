//
//  NReasonForReturnTVC.swift
//  Needs
//
//  Created by webcastle on 17/02/22.
//

import UIKit

class NReasonForReturnTVC: UITableViewCell, UITextViewDelegate {

    @IBOutlet weak var txtReason: UITextField!
    @IBOutlet weak var txtDetail: UITextField!
    @IBOutlet weak var txtDesc: UITextView!
    
    weak var delegate: NotifyDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func tappedReasonTextField(_ sender: UIButton) {
        self.delegate.didSelect(withInfo: ["type": AppData.reason])
    }
    
    @IBAction func tappedDetailTextField(_ sender: UIButton) {
        self.delegate.didSelect(withInfo: ["type": AppData.detail])
    }

    func textViewDidChange(_ textView: UITextView) {
        self.delegate.didSelect(withInfo: ["type": AppData.desc, "text": textView.text ?? ""])
    }
    
}
