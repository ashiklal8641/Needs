//
//  NOrderStatusTVC.swift
//  Needs
//
//  Created by webcastle on 16/02/22.
//

import UIKit

class NOrderStatusTVC: UITableViewCell {

    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var viewProgress: UIView!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
