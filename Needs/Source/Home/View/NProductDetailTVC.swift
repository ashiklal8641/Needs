//
//  NProductDetailTVC.swift
//  Needs
//
//  Created by webcastle on 13/02/22.
//

import UIKit

class NProductDetailTVC: UITableViewCell {

    @IBOutlet weak var pdtCV: UICollectionView!
    @IBOutlet weak var cvHeightConstraint: NSLayoutConstraint!
    
    var arrPrdts = [ProductM]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.registerCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //---------------------------------------------------------------------------------
    // MARK: - UI UPDATIONS
    //---------------------------------------------------------------------------------
    
    private func registerCell(){
        self.pdtCV.register(NProductCVC.self)
    }

}

extension NProductDetailTVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.arrPrdts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(NProductCVC.self, for: indexPath)
        cell.configureCell(model: arrPrdts[indexPath.item])
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width/2.2
        let height = collectionView.frame.size.height
        return CGSize(width: width, height: height)
    }
    
}

