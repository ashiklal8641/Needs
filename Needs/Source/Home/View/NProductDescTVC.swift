//
//  NProductDescVC.swift
//  Needs
//
//  Created by webcastle on 13/02/22.
//

import UIKit

class NProductDescTVC: UITableViewCell {

    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblFeature: UILabel!
    @IBOutlet weak var btnDesc: UIButton!
    @IBOutlet weak var btnFeature: UIButton!
    @IBOutlet weak var lblContent: UILabel!
    
    weak var delegate: NotifyDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func tappedDesc(_ sender: UIButton) {
        self.delegate?.didSelect(withInfo: ["type": AppData.desc])
        
    }
    
    @IBAction func tappedFeature(_ sender: UIButton) {
        self.delegate?.didSelect(withInfo: ["type": AppData.feature])
        
    }
}
