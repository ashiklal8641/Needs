//
//  NDeliveryAddressTVC.swift
//  Needs
//
//  Created by webcastle on 16/02/22.
//

import UIKit

class NDeliveryAddressTVC: UITableViewCell {

    @IBOutlet weak var lblAddress: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class NDeliveryByTVC: UITableViewCell {

    @IBOutlet weak var lblDelivery: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
