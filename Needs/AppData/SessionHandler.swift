//
//  SessionHandler.swift
//  Noo-gha
//
//  Created by WC-64 on 19/04/21.
//  Copyright © 2021 Noogha. All rights reserved.
//

import Foundation

class SessionHandler {
    
    static let userDefaults = UserDefaults.standard
    
    static var bearer_token:String? {
        get {
            if let value = LocalPersistant.string(for: .token) {
                return value
            } else {
                return nil
            }
        } set {
           // SessionHandler.notificationCount = 0
            LocalPersistant.save(value: newValue, key: .token)
        }
    }
    
    static var preSession:String? {
        get {
            if let value = LocalPersistant.string(for: .session) {
                return value
            } else {
                return nil
            }
        } set {
           // SessionHandler.notificationCount = 0
            LocalPersistant.save(value: newValue, key: .session)
        }
    }
    
//    static var languageSelected : LanguageSelected? {
//        get {
//            if let language = LocalPersistant.string(for: .language) {
//                return LanguageSelected(rawValue: language)
//            } else {
//                return nil
//            }
//        } set {
//            LocalPersistant.save(value: newValue!.rawValue, key: .language)
//        }
//    }
    
    static var uID:Int? {
        get {
            if let value = userDefaults.value(forKey: "uID") as? Int {
                return value
            } else {
                return nil
            }
        } set {
            SessionHandler.notificationCount = 0
            userDefaults.set(newValue, forKey: "uID")
        }
    }
    
    static var preUserID:Int {
        get {
            if let value = userDefaults.value(forKey: "preUserID") as? Int {
                return value
            } else {
                return -1
            }
        } set {
            userDefaults.set(newValue, forKey: "preUserID")
        }
    }
    
    static var isGuest:Bool {
        get {
            if let value = userDefaults.value(forKey: "isGuest") as? Bool {
                return value
            } else {
                return false
            }
        } set {
            userDefaults.set(newValue, forKey: "isGuest")
        }
    }
    
    static var isGuestCheckOut:Bool {
        get {
            if let value = userDefaults.value(forKey: "isGuestCheckOut") as? Bool {
                return value
            } else {
                return false
            }
        } set {
            userDefaults.set(newValue, forKey: "isGuestCheckOut")
        }
    }
    
    
    static var isLoggedIn: Bool {
        get {
            if uID == -1 || uID == nil{
                return false
            } else {
                return true
            }
        }
    }
 
    static var deviceToken: String = "Notokenfound-Notokenfound-Notokenfound-Notokenfound"
    static var deviceType: Int = 2
//    static var preSession:String = ""
    
    static var fullName : String {
        get {
            ""
           // return "\(self.loginData?.firstName ?? "" ) \(loginData?.lastName ?? "")"
        }
    }
    static var userName : String {
        get {
            ""
            //return "\(self.loginData?.userName ?? "")"
        }
    }
    
//    static var loginData : UserModel? {
//        get {
//            if let data = LocalPersistant.data(for: .user) {
//                do {
//                    let decoder = JSONDecoder()
//                    let decodedData = try decoder.decode(UserModel.self, from: data)
//                    return decodedData
//                } catch {
//                    DebugLogger.info("========= Fetching New LoginData failed =========")
//                    return nil
//                }
//            } else {
//                return nil
//            }
//        } set {
//            do {
//                let encoder = JSONEncoder()
//                let data = try encoder.encode(newValue)
//                LocalPersistant.save(value: data, key: .user)
//                DebugLogger.info("========= Saving New LoginData =========")
//            } catch (let err)  {
//                print("error save in user info",err.localizedDescription)
//            }
//            
//        }
//    }
    
    static var notificationCount : Int = 0 {
        didSet {
            DebugLogger.info("Posting notificationCount")
//            NotificationCenter.default.post(name: .notificationCountChange, object: nil)
        }
    }
    
    static var incomingURL : URL?
    static var refCode:String?
    static var incomingNotificationData:[String:Any]?
    
    static var cartCount: Int? {
        didSet {
            print("Posting chnage notification",cartCount?.description ?? "")
            NotificationCenter.default.post(name: .didChangeCart, object: nil)
        }
    }
    
    static var isCompletedOrder: Bool? {
        didSet {
            print("Posting chnage notification",isCompletedOrder?.description ?? "")
            NotificationCenter.default.post(name: .didCompletedOrder, object: nil)
        }
    }
    
}
