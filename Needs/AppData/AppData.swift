//
//  AppData.swift
//  Noo-gha
//
//  Created by WC-64 on 19/04/21.
//  Copyright © 2021 Noogha. All rights reserved.
//

import Foundation


class AppData {
    
    static var environment:Environment = .development
    
    static var baseURL:String {
        get {
            switch AppData.environment {
            case .development:
               return "https://s414.previewbay.com/Needs/api/" 
            case .production:
                return "https://s414.previewbay.com/Needs/api/"
            }
        }
    }

    static var googleAPIKey:String {
        get {
            switch self.environment {
            case .development:
                return "AIzaSyDaPEoxbTCNWttesGnp4HAO8XNcrfrYkMU"
            case .production:
                return "AIzaSyDaPEoxbTCNWttesGnp4HAO8XNcrfrYkMU"
            }
        }
    }
    
    static let faq = "https://noo-gah.com/faq.php"//"https://noo-gah.com/#faq"
    static let tnc = "http://www.joscojewellers.in/terms-and-conditions"
    static let contactUs = "https://noo-gah.com/#contact"
    static let privacyPolicy = "http://www.joscojewellers.in/privacy-policy"
    static let redeem = "https://noo-gah.com/redemption.php"
    static let aboutUs = "https://noo-gah.com/#aboutus"
    
    static let appstore_id = "1583263572"
    static let android_packageName = "app.josco"
    static let dynamiclinkDomainURL = "josco.page.link"
    
    static let imgBaseURL = "https://josco.s3.ap-south-1.amazonaws.com/"
    
    static let category = "Category"
    static let customize = "Customize"
    static let addCart = "AddCart"
    static let addToCart = "AddToCart"
    static let cart = "Cart"
    static let product = "Products"
    static let edit = "Edit"
    static let remove = "Remove"
    static let favourite = "Favourite"
    static let topBanner = "TopBanner"
    static let explore = "Explore"
    static let banner = "Banner"
    static let advanced = "Advanced"
    static let cntnue = "Continue"
    static let yes = "Yes"
    static let returnItem = "ReturnItem"
    static let delete = "Delete"
    static let save = "Save"
    static let changeAddress = "ChangeAddress"
    static let addAddress = "AddAddress"
    static let location = "Location"
    static let add = "Add"
    static let minus = "Minus"
    static let desc = "Description"
    static let feature = "Feature"
    static let login = "Login"
    static let reason = "Reason"
    static let detail = "Detail"
    static let outOfStock = "OutOfStock"

    static let cartCount = "cartCount"
}

enum DynamicLinkDomain : String {
    case dynamiclinkDomainURL = "noogah.page.link"
    case dynamiclinkProfileDomainURL = "noogahprofile.page.link"
}

enum Environment {
    case development
    case production
}

