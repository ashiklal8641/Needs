//
//  ResusableCell.swift
//  Dermacy
//
//  Created by WC-64 on 10/07/21.
//

import UIKit

protocol ReusableView {
    static var reuseIdentifier: String { get }

}


extension ReusableView {

    static var reuseIdentifier: String {
        return String(describing: self)
    }

}

extension UITableViewCell: ReusableView {}
extension UICollectionViewCell: ReusableView {}

extension UITableView {
    
    func register<T: UITableViewCell>(_: T.Type) {
           register(UINib(nibName: T.reuseIdentifier, bundle: nil), forCellReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusableCell<T: UITableViewCell>(_ identifier:T.Type,for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: identifier.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Unable to Dequeue Reusable Table View Cell")
        }

        return cell
    }

}

extension UICollectionView {
    
    func register<T: UICollectionViewCell>(_: T.Type) {
        register(UINib(nibName: T.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusableCell<T: UICollectionViewCell>(_ identifier:T.Type,for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: identifier.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Unable to Dequeue Reusable Table View Cell")
        }
        return cell
    }

}
