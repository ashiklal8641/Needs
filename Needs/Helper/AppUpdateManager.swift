//
//  AppUpdateManager.swift
//  DayToFresh
//
//  Created by Appzoc Technologies on 19/06/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import Foundation
import Firebase
import FirebaseRemoteConfig


class AppUpdateManager {
    
    static let shared = AppUpdateManager()
    var isFetchNeeded = true
    var remoteConfig:RemoteConfig?
    var availableUpdateversion = ""
    var desicription = ""
    var updateKind:Update = .none {
        didSet {
            if self.updateKind != .force {
             self.isFetchNeeded = false
             self.removerObserver()
            }

        }
    }
    
    private var version : Double  {
        let currentAppVersion = Bundle.main.infoDictionary
        print("currentAppVersion :\(String(describing: currentAppVersion?["CFBundleShortVersionString"] as! String))")
        guard let version = currentAppVersion?["CFBundleShortVersionString"] as? String else { return 0.0 }
        return Double(version) ?? 0.0
    }
    
    private init() {
        if remoteConfig == nil {
            remoteConfig = RemoteConfig.remoteConfig()
            NotificationCenter.default.addObserver(self, selector: #selector(didEnterForground), name: UIApplication.willEnterForegroundNotification, object: nil)
        }
        
    }
    
    @objc private func didEnterForground() {
        let topVC = UIApplication.topMostViewController
        if let _ = topVC as? JLoginVC {
            fetchRemoteConfig()
        } else if let _ = topVC as? JHomeVC {
           fetchRemoteConfig()
        }
        
    }
    
    private func showAlert(controller:UIViewController,update:Update){
        switch update {
        case .force:
            let alert = UIAlertController(title: "New Version Available", message: self.desicription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Update", style: .default, handler: { (_) in
                self.openAppStore()
            }))
            controller.present(alert, animated: true, completion: nil)
        case .normal:
            let alert = UIAlertController(title: "New Version Available", message: self.desicription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Update          ", style: .default, handler: { (_) in
                self.openAppStore()
            }))
            alert.addAction(UIAlertAction(title: "Later", style: .default, handler: { (_) in
               // self.openAppStore()
            }))
            controller.present(alert, animated: true, completion: nil)
        case .none:
            break
        }
        
    }
    
    
    func fetchRemoteConfig() {
        guard isFetchNeeded else {return}
        remoteConfig?.fetch(withExpirationDuration: 0, completionHandler: { (status, error) in
            guard error == nil else {   print("error occured while fetching remote config :- \(error?.localizedDescription ?? "No error")") ;return}
            self.remoteConfig?.activate(completion: nil)
            print("call update remote config")
            self.updateValue()
        })
    }
    
    fileprivate func updateValue(){
        
        let min = self.remoteConfigDoubleValue(forKey: .ios_min)
        let max = self.remoteConfigDoubleValue(forKey: .ios_max)
        self.desicription = "Please download new verion for better performance"//self.remoteConfigStringValue(forKey: .ios_description_en)
      //  self.availableUpdateversion = self.remoteConfigStringValue(forKey: .ios_store)
        print("min ;\(min) max:\(max) current version :\(version)")
        if version == max {
            self.updateKind = .none
        }else if version < min {
            self.updateKind = .force
        } else if (version > min && version < max) || max > version {
            self.updateKind = .normal
        }
        print("updateKind",updateKind)
        DispatchQueue.main.async { [unowned self] in
            guard let vc = UIApplication.topMostViewController else {return}
            self.showAlert(controller: vc, update: self.updateKind)
        }
        
        
    }
    
    private func openAppStore() {
         //TO DO:- Change App URL
        //https://itunes.apple.com/app/apple-store/id1472052463
        let storePath = self.remoteConfigStringValue(forKey: .ios_storeURL)
         if let url = URL(string: storePath),
             UIApplication.shared.canOpenURL(url){
             UIApplication.shared.open(url, options: [:]) { (opened) in
                 if (opened) {
                     print("App Store Opened")
                 }
             }
         } else {
             print("Can't Open URL on Simulator")
         }
     }
    
    
    fileprivate  func setupRemoteConfigDefaults() {
        let defaults:[String:NSObject] = ["version":version as NSObject]
        print("================ setupRemoteConfigDefaults ============== \n \(defaults) \n ===========================================")
        remoteConfig?.setDefaults(defaults)
    }
    
    fileprivate func removerObserver(){
        print("removing observer willEnterForegroundNotification")
        self.remoteConfig = nil
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    deinit {
        removerObserver()
    }
    
    fileprivate func remoteConfigDoubleValue(forKey key: ValueKey) -> Double {
        if let numberValue = self.remoteConfig?.configValue(forKey: key.rawValue).numberValue {
            return numberValue.doubleValue
        } else {
            return 0.0
        }
    }
    
    fileprivate func remoteConfigStringValue(forKey key:ValueKey) -> String {
        if let stringValue = self.remoteConfig?.configValue(forKey: key.rawValue).stringValue {
            return stringValue
        } else {
            return ""
        }
    }
    
    enum ValueKey: String {
      case ios_max
      case ios_min
      case ios_description_en
      case ios_description_ar
      case ios_storeURL
    }
    
    enum Update {
        case force
        case normal
        case none
    }
    
}
