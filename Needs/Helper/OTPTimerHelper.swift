//
//  OTPTimerHelper.swift
//  Noo-gha
//
//  Created by WC-64 on 05/03/21.
//  Copyright © 2021 Noogha. All rights reserved.
//

import Foundation


class OTPTimer : NSObject {
    typealias Callback = (String,Bool) -> ()
    
    private var seconds : Int!
    private var startTime = TimeInterval()
    private var timer:Timer = Timer()
    var callback:Callback
    
    init(seconds:Int,callback: @escaping Callback) {
        self.seconds = seconds
        self.callback = callback
        super.init()
    }
    
    @objc private func updateTime() {
        let currentTime = NSDate.timeIntervalSinceReferenceDate
        let elapsedTime = Int(UInt8(currentTime - startTime))
        if Int(elapsedTime) <= seconds {
          //  DebugLogger.debug("ElapsedTime OTP : \(elapsedTime)")
            let remainingSeconds = seconds-elapsedTime
            let seconds = remainingSeconds % 60
            let minutes = (remainingSeconds / 60) % 60
            let time = String(format: "%02d:%02d",minutes,seconds)
            callback(time,false)
        } else {
            self.stop()
            callback("00:00",true)
        }
    }
    
    func start(){
        timer.invalidate()
        self.startTime = NSDate.timeIntervalSinceReferenceDate
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    func stop(){
        timer.invalidate()
    }
    
}
