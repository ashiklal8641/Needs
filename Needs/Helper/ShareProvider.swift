//
//  ShareProvider.swift
//  EmployeeApp
//
//  Created by Appzoc Technologies on 24/12/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import FirebaseDynamicLinks

class ShareProvider: NSObject {
    
    typealias Callback = (URL?,Error?) -> ()
    
    var components:URLComponents?
    var arguments:[String:Any]?
    var dynamicLinkInfo:[String:Any]?
    var domain:String = AppData.dynamiclinkDomainURL
    var path:String?
    var callback:Callback
    
    init(domain:DynamicLinkDomain = .dynamiclinkDomainURL,path:String? = nil ,arguments:[String:Any]?, dynamicLinkInfo:[String:Any],callback: @escaping Callback) {
        print("initialized Share Provider")
        self.path = path
        self.domain = domain.rawValue
        self.arguments = arguments
        self.dynamicLinkInfo = dynamicLinkInfo
        self.callback = callback
        super.init()
        
    }
    
  
    func createLink() {
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = self.domain
        if self.path != nil {
            components.path = self.path ?? ""
        }
        if let arg = self.arguments {
            var queryItems:[URLQueryItem] = []
            arg.forEach { (key,value) in
                let queryItem = URLQueryItem(name: key, value: (value as AnyObject).description ?? "")
                queryItems.append(queryItem)
            }
            components.queryItems = queryItems
        }
        guard let linkParameter = components.url else {return}
        print("sharing Link :\(linkParameter.absoluteString)")
        let dynamicLinkDomainURL = "https://\(self.domain)"
        guard let shareLink = DynamicLinkComponents.init(link: linkParameter, domainURIPrefix: dynamicLinkDomainURL) else {
            return }
        // IOS PARAMETERS
        if let bundleID = Bundle.main.bundleIdentifier {
            shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: bundleID)
        }
        shareLink.iOSParameters?.appStoreID = AppData.appstore_id
        // Android PARAMETERS
        shareLink.androidParameters = DynamicLinkAndroidParameters(packageName: AppData.android_packageName)
        
        // Config MetaData
        shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        if let title = self.dynamicLinkInfo?["title"] as? String {
            shareLink.socialMetaTagParameters?.title = title
        }
        if let description = self.dynamicLinkInfo?["desc"] as? String {
            shareLink.socialMetaTagParameters?.descriptionText = description
        }        
        if let imageString = self.dynamicLinkInfo?["thumb"] as? String, let imageURL = URL(string: imageString) {
            shareLink.socialMetaTagParameters?.imageURL = imageURL
        }
        
        guard let longURL = shareLink.url else { return }
        print("The long dynamcLink is :\(longURL)")
        shareLink.shorten {  (url, warnings, error)  in
         
            
            if let error = error {
                self.callback(nil,error)
                print("Oh no! got an error :\(error.localizedDescription)")
                return
            }
            
            if let warnings = warnings {
                for warning in warnings {
                    print("FDL warning :\(warning)")
                }
            }
            
            guard let url = url else { return }
            print("Short url :\(url.absoluteString)")
            self.callback(url,nil)
            DynamicLinks.performDiagnostics(completion: nil)
        }
        
    }
    
    deinit {
        print("deinitilized Share Provider")
    }
    
    
}

/*
 var components = URLComponents()
 components.scheme = "https"
 components.host = SessionHandler.dynamiclinkDomainURL
 components.path = "/doctor"
 
 guard let productId = self.doctorDetail?.doctorId else {
     return }
 let queryItem = URLQueryItem(name: "doctorId", value: productId.description)
 components.queryItems = [queryItem]
 
 guard let linkParameter = components.url else {return}
 print("sharing Link :\(linkParameter.absoluteString)")
 let dynamicLinkDomainURL = "https://\(SessionHandler.dynamiclinkDomainURL)"
 guard let shareLink = DynamicLinkComponents.init(link: linkParameter, domainURIPrefix: dynamicLinkDomainURL) else { return }
 // IOS PARAMETERS
 if let bundleID = Bundle.main.bundleIdentifier {
     shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: bundleID)
 }
 
 shareLink.iOSParameters?.appStoreID = SessionHandler.appstore_id
 // Android PARAMETERS
 shareLink.androidParameters = DynamicLinkAndroidParameters(packageName: SessionHandler.android_packageName)
 // Config MetaData
 shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
 shareLink.socialMetaTagParameters?.title = self.doctorDetail?.doctorName
 shareLink.socialMetaTagParameters?.descriptionText = self.doctorDetail?.specialization
 if let imageString = doctorDetail?.doctorImageUrl, let imageURL = URL(string: imageString) {
     shareLink.socialMetaTagParameters?.imageURL = imageURL
 }
 
 guard let longURL = shareLink.url else { return }
 print("The long dynamcLink is :\(longURL)")
 
 
 shareLink.shorten { (url, warnings, error) in
     Utility.sharedInstance.removeLoader()
     if let error = error {
         print("Oh no! got an error :\(error.localizedDescription)")
         return
     }
     
     if let warnings = warnings {
         for warning in warnings {
             print("FDL warning :\(warning)")
         }
     }
     
     guard let url = url else { return }
     print("Short url :\(url.absoluteString)")
     let itemToShare = ["Hey, download the best App for Online Counselling and Therapy - Metro Mind App. Sign Up Now and schedule video consultation with \(self.doctorDetail?.doctorName ?? "") .",url] as [Any]
     let activityViewController = UIActivityViewController(activityItems: itemToShare, applicationActivities: nil)
     activityViewController.excludedActivityTypes = [.saveToCameraRoll]
     activityViewController.popoverPresentationController?.sourceView = sender // so that iPads won't crash
     self.present(activityViewController, animated: true, completion: {
     })
     DynamicLinks.performDiagnostics(completion: nil)
 }
 */
