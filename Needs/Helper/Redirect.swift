//
//  Redirect.swift
//  Noo-gha
//
//  Created by WC-64 on 15/06/21.
//  Copyright © 2021 Noogha. All rights reserved.
//


import UIKit
import FirebaseDynamicLinks

class Redirect {
    
    
    class func redirectDeepLink(path:String,withInfo:[String:Any]) {
        DebugLogger.info("redirectDeepLink With path:\(path) with info:\(withInfo)")
        if path == "/product" {
            guard SessionHandler.isLoggedIn else {return}
            if let pdtId = withInfo["PRODUCT_ID"]  as? Int{
                    if let _vc = UIApplication.topMostViewController {
                        let slug = withInfo["PRODUCT_SLUG"]  as? String
//                        let vc: JProductDetailVC = Storyboards.Category.viewController(controller: JProductDetailVC.self)
//                        var pdtModel = JProductModel()
//                        pdtModel.id = pdtId
//                        pdtModel.slug = slug
//                        vc.pdtModel = pdtModel
//                        vc.hidesBottomBarWhenPushed = true
//                        _vc.navigationController?.pushViewController(vc, animated: true)
                    }
            }
        }
    }
    
    class func retriveReferral(_ incomingURL: URL) {
         _ = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) { (dynamicLink, error) in
            guard error == nil else {
                print("Found an error :\(String(describing: error?.localizedDescription))")
                return
            }
            if let dynamicLink = dynamicLink {
                self.handleIncomingDynamicLink(dynamicLink) {(path,withInfo) in
                    if path == "/invite" {
                        if let refCode = withInfo["refCode"] {
                            SessionHandler.refCode = refCode
                        }
                    }
                }
            }
        }
        
    }
    
    class func redirectPushNotification(withInfo:[String:Any]){
        DebugLogger.info("redirectPushNotification :\(withInfo)")
        if let type = withInfo["type"] as? Int {
        }
        
    }
    
    class func handleIncomingURL(_ incomingURL: URL) -> Bool {
           let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) { (dynamicLink, error) in
               guard error == nil else {
                   print("Found an error :\(String(describing: error?.localizedDescription))")
                   return
               }
               if let dynamicLink = dynamicLink {
                self.handleIncomingDynamicLink(dynamicLink) {(path,info) in
                    Redirect.redirectDeepLink(path: path, withInfo: info)
                }
               }
           }
           if linkHandled {
               return true
           } else {
               return false
               // Handle Other cases
           }
       }
        
        
    fileprivate class func handleIncomingDynamicLink(_ dynamicLink:DynamicLink,completetion:((_ path:String,_ info:[String:String])->())?) {
            guard let url = dynamicLink.url else {
                print("link Object have no url")
                return
            }
            print("Incoming link parameter is :\(url.absoluteString)")
            var info: [String: String] = [:]
            let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
            let path = urlComponents?.path ?? ""
            urlComponents?.queryItems?.forEach {
                info[$0.name] = $0.value
            }
            
            guard dynamicLink.matchType == .unique || dynamicLink.matchType == .default else {return}
            completetion?(path,info)
            
        }
    
    class func fromCustomSchemeURL(_ url:URL) -> Bool {
         let isDynamicLink = DynamicLinks.dynamicLinks().shouldHandleDynamicLink(fromCustomSchemeURL: url)
          if isDynamicLink {
            if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
                self.handleIncomingDynamicLink(dynamicLink) {(path,info) in
                    Redirect.redirectDeepLink(path: path, withInfo: info)
                }
            }
          }
       return isDynamicLink
    }

    
}
