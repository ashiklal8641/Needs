//
//  APIRouter.swift
//  Dermacy
//
//  Created by WC-64 on 11/08/21.
//
//
import Foundation
import Alamofire

enum APIRouter {
    
    case token
    case signin
    case skipLogin
    case verifyOtp
    case resendOtp
    case register
    case confirmLocation
    case home
    case getProfileDetails
    case EditProfileDetails
    case getCategories
    case notifications
    case getShopsByCategory
    case getProducts
    case getProductDetail
    case addToCart
    case myCart
    case shortCartInfo
    case removeFromCart
    case updateCart
    case cartDetails
    case placeOrder
    case paymentResponse
    case addAddress
    case getAddress
    case editAddress
    case deleteAddress
    case checkout
    case getOrderList
    case getOrderDetail
    case getDeliveryDetails
    case reorderItem
    case returnItem
    case returnReason
    case cancelItem
    case getReviews
    case deleteReview
    case addReview
    case addToCompare
    case getCompareList
    case getFilters
    case getFilterProducts
    case getSearchProducts
    case getSearchCategory
    case checkAvailability
    case logoutUser
    case getCancelInfo
    case addPanCard
    case getPancard
    case editPanCard
    case getFaq
    case getPromotions
    case getTermsConditions
    case getprivacypolicy
    case getReturnPolicy
    case getContact
    case sendMessage
    case getMyVouchers
    case getDailyVoucher
    case getNotifications
    case scratchVoucher
    case getAdvBookingInfo
    case getMyAdvBooking
    case getBookingAmound
    case processPayment
    case paybookingamount
    case getAboutUs
    case getCoupons
    case applyCoupon
    case getWishlist
    case updateWishList
    case updateMonthlyBucket
    case contact
    case getInstoreList
    case redeemInstoreOffer
    case redeemDeliveryPickupOffer
    case getLoyalityInfo
    case getLoyaltyPoints
    case outOfStock
    
    // MARK: - HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .signin,
             .skipLogin,
             .verifyOtp,
             .resendOtp,
             .register,
             .confirmLocation,
             .getProducts,
             .getProductDetail,
             .getCategories,
             .getShopsByCategory,
             .getProfileDetails,
             .EditProfileDetails,
             .addToCart,
             .removeFromCart,
            .updateCart,
             .cartDetails,
             .placeOrder,
             .addAddress,
             .getAddress,
             .editAddress,
             .deleteAddress,
             .checkout,
             .cancelItem,
             .addReview,
             .deleteReview,
             .reorderItem,
             .addToCompare,
             .getCompareList,
             .getFilterProducts,
             .checkAvailability,
             .logoutUser,
             .addPanCard,
             .editPanCard,
             .sendMessage,
             .scratchVoucher,
             .getAdvBookingInfo,
             .getMyAdvBooking,
             .getBookingAmound,
             .processPayment,
             .paybookingamount,
             .returnItem,
             .getAboutUs,
             .getTermsConditions,
             .getprivacypolicy,
             .getReturnPolicy,
             .getFaq,
             .getCoupons,
             .applyCoupon,
             .getWishlist,
             .updateWishList,
             .updateMonthlyBucket,
             .myCart,
             .getInstoreList,
             .redeemInstoreOffer,
             .redeemDeliveryPickupOffer,
             .getOrderList,
             .getOrderDetail,
             .getLoyalityInfo,
             .getLoyaltyPoints,
             .returnReason,
             .outOfStock,
             .shortCartInfo,
             .paymentResponse,
             .getNotifications,
             .notifications: 
             return .post
        case .home,
             .getDeliveryDetails,
             .getReviews,
             .getFilters,
             .getSearchProducts,
             .getSearchCategory,
             .getCancelInfo,
             .getPancard,
             .getPromotions,
             .getContact,
             .getMyVouchers,
             .getDailyVoucher,
             .contact,
             .token:
            return .get
        }
    }
    
    // MARK: - Path
    var path: String {
        switch self {
        case .token:
            return "userSession"
        case .signin:
            return "login"
        case .skipLogin:
            return "guestLogin"
        case .verifyOtp:
            return "verifyOtp"
        case .resendOtp:
            return "resendOtp"
        case .register:
            return "register"
        case .confirmLocation:
            return "confirmLocation"
        case .home:
            return "home"
        case .getCategories:
            return "sub_categories"
        case .getShopsByCategory:
            return  "getShopsByCategory"
        case .notifications:
            return "getNotification"
        case .getProducts:
            return "productList"
        case .getProductDetail:
            return "product_detail"
        case  .getProfileDetails:
            return "getProfile"
        case .EditProfileDetails:
            return "editProfile"
        case .addToCart:
            return "addToCart"
        case .getTermsConditions:
            return "terms"
        case .removeFromCart:
            return "removeCart"
        case .updateCart:
            return "updateCart"
        case .cartDetails:
            return "cart-details"
        case .placeOrder:
            return "placeOrder"
        case .addAddress:
            return "addAddress"
        case .getAddress:
            return "getAddress"
        case .editAddress:
            return "editAddress"
        case .deleteAddress:
            return "deleteAddress"
        case .getPromotions:
            return "getPromotions"
        case.getprivacypolicy:
            return "privacy"
        case .getFaq:
            return "faq"
        case .sendMessage:
            return "sendMessage"
        case .getContact:
            return "getContact"
        case .checkout:
            return "checkOut"
        case .getOrderList:
            return "myOrders"
        case .getDeliveryDetails:
            return "getDeliveryDetails"
        case .cancelItem:
            return "cancelItem"
        case .getReviews:
            return "review"
        case .addReview:
            return "addReview"
        case .deleteReview:
            return "deleteReview"
        case .reorderItem:
            return "reOrderProduct"
        case .addToCompare:
            return "compare"
        case .getCompareList:
            return "compareProducts"
        case .getFilters:
            return "getAppFilters"
        case .getFilterProducts:
            return "getProducts"
        case .getSearchProducts:
            return "search-product"
        case .getSearchCategory:
            return "search-category"
        case .checkAvailability:
            return "pinCode"
        case .logoutUser:
            return "logout"
        case .getCancelInfo:
            return "getCancelInfo"
        case .getLoyalityInfo:
            return "pointHistory"
        case .getLoyaltyPoints:
            return "getLoyaltyPoints"
        case .addPanCard:
            return "addPanCard"
        case .getPancard:
            return "getPancard"
        case .editPanCard:
            return "editPanCard"
        case .getMyVouchers:
            return "getMyVouchers"
        case .getDailyVoucher:
            return "getDailyVoucher"
        case .getNotifications:
        return "notifications"
        case .scratchVoucher:
            return "scratchVoucher"
        case .getAdvBookingInfo:
            return "getAdvBookingInfo"
        case .getMyAdvBooking:
            return "getMyAdvBooking"
        case .getBookingAmound:
            return "getBookingAmound"
        case .processPayment:
            return "processPayment"
        case .paybookingamount:
           return "paybookingamount"
        case .returnItem:
           return "return"
        case .getAboutUs:
            return "about"
        case .getReturnPolicy:
            return "return_policy"
        case .getCoupons:
            return "getCoupons"
        case .applyCoupon:
            return "applyCoupon"
        case .getWishlist:
            return "getWishlist"
        case .updateWishList:
            return "updateWishlist"
        case .updateMonthlyBucket:
            return "updateMonthlyBucket"
        case .contact:
            return "contact"
        case .myCart:
            return "myCart"
        case .getInstoreList:
            return "getInstoreList"
        case .redeemInstoreOffer:
            return "redeemInstoreOffer"
        case .redeemDeliveryPickupOffer:
            return "redeemDeliveryPickupOffer"
        case .getOrderDetail:
            return "orderDetail"
        case .returnReason:
            return "returnReason"
        case .outOfStock:
           return "checkStock"
        case .shortCartInfo:
            return "shortCartInfo"
        case .paymentResponse:
            return "paymentResponse"
        }
    }
    

}

