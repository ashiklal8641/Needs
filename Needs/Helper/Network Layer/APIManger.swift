//
//  APIManger.swift
//  Dermacy
//
//  Created by WC-64 on 11/08/21.
//

import Foundation
import Alamofire

typealias Completion<T:Codable> = ((Result<ResponseBase<T>, AFError>)->Void)
//
//class APIManager {
    
    

    
//
//    @discardableResult
//    static func performRequest<T:Codable>(route:APIConfiguration, decoder: JSONDecoder = JSONDecoder(), completion:@escaping (Result<ResponseBase<T>, AFError>)->Void) -> DataRequest {
//        return AF.request(route)
//                        .responseDecodable (decoder: decoder){ (response: DataResponse<ResponseBase<T>,AFError>) in
//                            self.handleResponse(response, completion: completion)
//        }
//    }
//
//    fileprivate static func handleResponse<T:Codable>(_ response: DataResponse<ResponseBase<T>, AFError>,completion:@escaping Completion<T>) {
////        switch response.result {
////        case .success(let responseData):
////            break
////        case .failure(let error):
////            break
////        }
//        completion(response.result)
//    }
//
//}

/*
 //
 //  APIClient.swift
 //  Dermacy
 //
 //  Created by WC-64 on 11/08/21.
 //

 import UIKit




 class APIClient  {

     @discardableResult
     static func performRequest<T:Codable>(route:APIConfiguration,loader:Bool = true ,decoder: JSONDecoder = JSONDecoder(), completion:@escaping Completion<T>) -> DataRequest {
         if !NetworkReachabilityManager()!.isReachable {
             if let vc = UIApplication.topMostViewController  {
                 let alert = UIAlertController(title: "Couldn't Load", message: "couldn't connect server, please check your network connectivity.", preferredStyle: .alert)
                 
     //            alert.setTitlet(font: UIFont.systemFont(ofSize: 15, weight: .medium), color: .white)
     //            alert.setMessage(font:  UIFont.systemFont(ofSize: 13, weight: .light), color: .lightGray)
                 let reportAction = UIAlertAction(title: "Try Again", style: .destructive) { (_) in
                     self.performRequest(route: route, completion: com)
                 }
                 let cancelAction = UIAlertAction(title: "Cancel", style: .default){ (_) in
                    
                 }

                 alert.addAction(reportAction)
                 alert.addAction(cancelAction)
                 alert.view.tintColor = UIColor.white
                 vc.present(alert, animated: true, completion: nil)
             }
             
         }
         return AF.request(route)
                         .responseDecodable (decoder: decoder){ (response: DataResponse<ResponseBase<T>,AFError>) in
                             print("================= Debug Print =================")
                             debugPrint(response)
                             print("================= ======== =================")
                             self.handleResponse(response, completion: completion)
         }
     }
     
     
     
     
     fileprivate static func handleResponse<T:Codable>(_ response: DataResponse<ResponseBase<T>, AFError>,completion:@escaping Completion<T>) {
         switch response.result {
         case .success(let responseData):
             if let code = responseData.errorcode , code != 200 {
                 Utility.displayBanner(message: responseData.message ?? Utility.errorMsg, style: .danger)
             }
         case .failure(let err):
             Utility.displayBanner(message: err.errorDescription ?? Utility.errorMsg+"\n Error : \(err.responseCode ?? 0)", style: .danger)
         }
         completion(response.result)
     }
 }



 */
