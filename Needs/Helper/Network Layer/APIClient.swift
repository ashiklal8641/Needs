//
//  APIClient.swift
//  Dermacy
//
//  Created by WC-64 on 11/08/21.
//

import UIKit
import Alamofire

var showBanner = true

class APIClient  {

    
    
    static func performRequest<T:Codable>(route:APIConfiguration,loader:Bool = true ,decoder: JSONDecoder = JSONDecoder(), completion:@escaping Completion<T>) {
        print(route)
        if route.request.router == .updateCart  || route.request.router == .confirmLocation || route.request.router == .notifications{
            showBanner = false
        }else {
            showBanner = true
        }
        if !NetworkReachabilityManager()!.isReachable {
            if let vc = UIApplication.topMostViewController  {
                let alert = UIAlertController(title: "Couldn't Load", message: "couldn't connect server, please check your network connectivity.", preferredStyle: .alert)
                let reportAction = UIAlertAction(title: "Try Again", style: .destructive) { (_) in
                    self.performRequest(route: route, completion: completion)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .default){ (_) in
                   
                }
                alert.addAction(reportAction)
                alert.addAction(cancelAction)
                vc.present(alert, animated: true, completion: nil)
            }
            return
        }
        if loader {
            IndicatorOverlay.showActivityIndicatorWithMessage("")
        }
        AF.request(route)
                        .responseDecodable (decoder: decoder){ (response: DataResponse<ResponseBase<T>,AFError>) in
                            if loader {
                                IndicatorOverlay.hideActivityIndicator()
                            }
                            print("================= Debug Print =================")
                            debugPrint(response)
                            print("================= ======== =================")
                            self.handleResponse(response, completion: completion)
        }
    }
    
    
    
    
    fileprivate static func handleResponse<T:Codable>(_ response: DataResponse<ResponseBase<T>, AFError>,completion:@escaping Completion<T>) {
        switch response.result {
        case .success(let responseData):
            if let code = responseData.errorcode , code != 0 , code != 3 {
                if showBanner {
                    Utility.displayBanner(message: responseData.message ?? Utility.errorMsg, style: .danger)
                }
            }
        case .failure(let err):
            if let data = response.data {
                do  {
                    if let jsonData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] {
                        print("jsonData",jsonData)
                        if let erro = jsonData["code"] as? Int , erro == 1 , let message = jsonData["message"] as? String {                            
                                Utility.displayBanner(message: message ?? Utility.errorMsg+"\n Error : \(err.responseCode ?? 0)", style: .danger)
                            
                            return
                        }
                    }
                } catch (_) {
                    
                }
            }
//            Utility.displayBanner(message: err.errorDescription ?? Utility.errorMsg+"\n Error : \(err.responseCode ?? 0)", style: .danger)
        }
        completion(response.result)
    }
    
}


