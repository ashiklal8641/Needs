//
//  APIConfiguration.swift
//  Dermacy
//
//  Created by WC-64 on 11/08/21.
//

import Foundation
import Alamofire

struct APIConfiguration : URLRequestConvertible {
    
    var request:(params:Parameters?,router:APIRouter)

    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try AppData.baseURL.asURL()
        
//        let appendingPath = URL(string: url.absoluteString+request.router.path)!
        var appendingPath = url.absoluteString+request.router.path
        
        if request.router.method == .get {
            if let parameters = request.params {
                appendingPath = appendingPath+"?"+(self.getQueryString(params: parameters))
            }
        }
        //var urlRequest = URLRequest(url: url.appendingPathComponent(request.router.path))
        var urlRequest = URLRequest(url: URL(string: appendingPath)!)
        
        if request.router.method == .post {
            if let parameters = request.params {
                do {
                    if request.router == .updateCart {
                        urlRequest.httpBody = self.getQueryString(params: parameters).data(using: .utf8)
                    } else {
                        urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                    }
                    
                } catch {
                    throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                }
            }
        }
        
        // HTTP Method
        urlRequest.httpMethod = request.router.method.rawValue
        
        // Common Headers
       // urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        if request.router == .updateCart {
            urlRequest.setValue(ContentType.urlEncoded.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        } else {
            urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        }
        
//        if let token = SessionHandler.bearer_token {
//            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
//        }else {
//            if let token = SessionHandler.preSession {
//                urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
//            }
//        }
        if let token = SessionHandler.bearer_token {
            urlRequest.setValue("\(token)", forHTTPHeaderField: "session_id")
        }else {
            if let token = SessionHandler.preSession {
                urlRequest.setValue("\(token)", forHTTPHeaderField: "session_id")
            }
        }
        urlRequest.setValue("\(SessionHandler.deviceType)", forHTTPHeaderField: "device_type")
        urlRequest.setValue(SessionHandler.deviceToken, forHTTPHeaderField: "device_token")
//        if let language = SessionHandler.languageSelected {
//            urlRequest.addValue(language.rawValue, forHTTPHeaderField:"language")
//        }
        
       
        
        // Parameters
       
        
        return urlRequest
    }
    
    func getQueryString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
//            data.append(key + "=\(self.percentEscapeString(value as! String))")
                data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
    case urlEncoded = "application/x-www-form-urlencoded"
}
