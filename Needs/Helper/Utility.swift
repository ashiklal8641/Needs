//
//  Utility.swift
//  Noo-gha
//
//  Created by WC-64 on 03/03/21.
//  Copyright © 2021 Noogha. All rights reserved.
//

import UIKit
import Alamofire

class Utility {
    
    static var errorMsg = "We're sorry, but something went wrong.\nPlease try again."
    
    class var keyWindow: UIWindow? {
            get {
                if #available(iOS 13.0, *) {
                    return UIApplication.shared.connectedScenes.filter({$0.activationState == .foregroundActive}).map({$0 as? UIWindowScene}).compactMap({$0}).first?.windows.filter({$0.isKeyWindow}).first
                } else {
                    // Fallback on earlier versions
                    return UIApplication.shared.keyWindow
                }
            }
        }
    
    class func setRootViewControllers(viewControllers:[UIViewController],currentController:UIViewController? = nil){
        if let window = Utility.keyWindow,let controller = window.rootViewController as? UINavigationController {
           // controller.viewControllers.removeAll()
            controller.setViewControllers(viewControllers, animated: true)
        } else if let vc = currentController,let controller = vc.navigationController {
            controller.setViewControllers(viewControllers, animated: true)
        }
    }
    
    class func setDashboardViewController() {
        if let window = Utility.keyWindow,let controller = window.rootViewController as? UINavigationController {
            let vc = Storyboards.Main.viewController(controller:NCustomTabbar.self)
            controller.setViewControllers([vc], animated: true)
        }
    }

    static var safeAreaLayout : UIEdgeInsets {
        get {
            return Utility.keyWindow?.safeAreaInsets ?? .zero
        }
    }
    
    static var isConnectionBannerNeed = false
    
    static let reachability =  Reachability()
    static var isReachable = true
    static var isFirstTime = true
    
    class func setupRechability(){
         reachability?.whenReachable = { reachability in
             print("network is available")
            isReachable = true
            if self.isFirstTime {
                self.isFirstTime = false
            } else {
                if  isConnectionBannerNeed {
                    self.isConnectionBannerNeed = false
                   // showBanner()
                }
            }
         }
         
         reachability?.whenUnreachable = { reachability in
            print("network is offline")
            isReachable = false
            if self.isFirstTime {
                self.isFirstTime = false
                isConnectionBannerNeed = true
            } else {
                isConnectionBannerNeed = true
                //showBanner()
            }
             
         }
         do {
             try reachability?.startNotifier()
         } catch {
             print("Unable to start notifier")
         }
     }
    
    class func resetRootViewComtroller(viewController:UIViewController){
        if let window = Utility.keyWindow {
            window.rootViewController = viewController
        }
    }

    class func displayBanner(duration: TimeInterval = 1, title: String = "", message: String, alignment: NSTextAlignment? = nil, colour: UIColor? = nil, style: BannerStyle) {
        DisplayBanner.shared.show(duration: duration, title: title, message: message, alignment: alignment, colour: colour, style: style)
    }
    
    
    class func clearDefualts() {
        SessionHandler.uID = nil
        SessionHandler.preUserID = -1
        SessionHandler.notificationCount = 0
        SessionHandler.isGuest = false
//        SessionHandler.loginData = nil
//        SessionHandler.bearer_token = ""
        LocalPersistant.save(value: "", key: .email)
//        LocalPersistant.save(value: "", key: .session)
        LocalPersistant.save(value: "", key: .user_name)
        LocalPersistant.save(value: "", key: .phone_number)
        LocalPersistant.save(value: "", key: .token)
    }

}

class GeneralAlert : NSObject {
    
    var alert:UIAlertController?
    var controller:UIViewController?
    var message:String?
    var title:String? = "NEEDS"
    var actionTitle:String? = "OK"
    
    init(controller:UIViewController,message:String?,title:String? = "NEEDS",actionTitle:String? = "OK") {
        self.controller = controller
        self.message = message
        self.title = title
        self.actionTitle = actionTitle
    }
    
    func showAlert(hanlder:(()->())?){
        alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert?.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        alert?.addAction(UIAlertAction(title: self.actionTitle, style: .default, handler: { (action) in
            hanlder?()
        }))
        guard let alert = self.alert else {
            return
        }
        controller?.present(alert, animated: true, completion: nil)
    }
    
    func showSimpleAlert(hanlder:(()->())?){
        alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert?.addAction(UIAlertAction(title: self.actionTitle, style: .default, handler: { (action) in
            hanlder?()
        }))
        guard let alert = self.alert else {
            return
        }
        controller?.present(alert, animated: true, completion: nil)
    }
    
    deinit {
        print("deinitilized Alert")
    }

}

