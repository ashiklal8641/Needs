//
//  StringExtension.swift
//  Dermacy
//
//  Created by WC-64 on 22/07/21.
//

import UIKit

extension String {
    
    func updateStrikeThroughFont(_ strikethroughColor:UIColor) -> NSAttributedString {
        let strokeEffect: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue,
            NSAttributedString.Key.strikethroughColor: strikethroughColor
        ]
        let strokeString = NSAttributedString(string:self, attributes: strokeEffect)
        return strokeString
    }
    
    var trimWhiteSpace: String {
        get {
            return self.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    func cleanedCurrencyValue(currency:String = " ₹") -> String {
        let value = Double(self) ?? 0.0
        return value.cleanedCurrencyValue()
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    
}


extension Double {
    
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
    
    func rounded(digits: Int) -> Double {
        let multiplier = pow(10.0, Double(digits))
        return (self * multiplier).rounded() / multiplier
    }
    
    func cleanedCurrencyValue(currency:String = " ₹") -> String {
        return currency + self.rounded(digits: 2).cleanValue
    }
    
}
