//
//  UIFontExtension.swift
//  josco
//
//  Created by WC-64 on 26/08/21.
//  Copyright © 2021 Josco. All rights reserved.
//

import UIKit

extension UIFont {
    
    static func robotoFont(ofSize: CGFloat, weight: Resources.Fonts.Weight) -> UIFont {
        return UIFont(name: weight.rawValue, size: ofSize)!
    }
    
}

extension NSNotification.Name {
    
    static var didChangeCart:NSNotification.Name {
        return NSNotification.Name(rawValue: "didChangeCart")
    }
    
    static var didChangeNotificationCount:NSNotification.Name {
        return NSNotification.Name(rawValue: "didChangeNotificationCount")
    }
    
    static var didCompletedOrder:NSNotification.Name {
        return NSNotification.Name(rawValue: "didCompletedOrder")
    }

}
